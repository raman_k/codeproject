<?php
class BackEnd_Helper_viewHelper
{
	/**
	 * Mail sent ot the user registration and forgot password etc
	 * @param array $recipents
	 * @param string $subject
	 * @param string $body
	 * @param string $fromEmail
	 * @param string $config
	 */
   public static function SendMail($recipents,$subject,$body,$fromEmail = null, $config = null)
	{ 
		$to = array() ; $cc =array() ; $bcc = array() ;

		foreach($recipents as $key => $value)
		{	
			switch($key)
			{
				case "to" : 
					if(gettype($value) == "string")
					{		
							
						$to[] = $value; 
								
					}
					else
					{
						foreach($value as $val)
						{
							if(gettype($val) == "string")
							{
								$to[] = $val; 
							}
						}
					}
					
					break;
					
				case "cc" : 
					if(gettype($value) == "string")
					{
						$cc[] = $value; 
					}
					else
					{
						foreach($value as $val)
						{
							if(gettype($val) == "string")
							{
								$cc[] = $val; 
							}
						}
					}
		
					break;
					
				case "bcc" : 
					if(gettype($value) == "string")
						{
							$bcc[] = $value; 
						}
						else
						{
							foreach($value as $val)
							{
								if(gettype($val) == "string")
								{
									$bcc[] = $val; 
								}
							}
						}
				
					break;	
				default:
					if(gettype($value) == 'string')
					{
						$to[] = $value; 
					}
				
			}
			
		}
		
		
// 		if($config == null)
// 		{
// 			$config = array('auth' => 'Register',
// 			//'server' => '192.178.1.2',
// 			'username' => 'kraj@seasiainfotech.com',
// 			'password' => 'mind@123',
// 			'ssl' => 'ssl',
// 			'port' => '25'
// 			);
			
// 		} 
		

		//$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com',$config);
		
		$mail = new Zend_Mail();
		$mail->setBodyHtml($body);
		$mail->setBodyText(strip_tags($body));
	
		if(count($to) > 0)
		{
			foreach($to as $email)
			{
				$mail->addTo($email);
			}
			foreach($cc as $email)
			{
				$mail->addCc($email);
			}
			foreach($bcc as $email)
			{
				$mail->addBcc($email);
			}
			$mail->setFrom($fromEmail);
			$mail->setSubject($subject);
			$mail->send();
		}
		
	}
	
	/**
	 * resizeImage
	 * 
	 * Generate thumnail
	 * 
	 * @param Array $file
	 * @param string $orFileName
	 * @param integer $toWidth
	 * @param integer $toHeight
	 * @param string $savePath
	 * @author Raman
	 * @version 1.0
	 */
	public static function resizeImage($file, $orFileName, $toWidth, $toHeight, $savePath)
	{
		$image =$file["name"];
		$img  = $file['tmp_name'];
		if ($image)  {
	
			$filename = $file['name'];
			$imgInfo = getimagesize($img);
			switch ($imgInfo[2]) {
				case 1: $im = imagecreatefromgif($img); break;
				case 2: $im = imagecreatefromjpeg($img);  break;
				case 3: $im = imagecreatefrompng($img); break;
				default:  @trigger_error('Unsupported filetype!', E_USER_WARNING);
					
				break;
			}
			if($toWidth==0 && $toHeight!=0) {
				//get width
				$width = self::resizeToHeightImage($toHeight,$img);
				$toWidth = $width;
				$height = $toHeight;
			}
			elseif($toWidth!=0 && $toHeight==0) {
				//get height
				$height 	= self::resizeToWidthImage($toWidth,$img);
				$toHeight = $height;
				$width   	= $toWidth;
	
			}
			elseif($toWidth!=0 && $toHeight!=0) {
				$width   	= $toWidth;
				$height = $toHeight;
			}
			$newImg = imagecreatetruecolor($width, $height);
			/* Check if this image is PNG or GIF, then set if Transparent*/
			if(($imgInfo[2] == 1) OR ($imgInfo[2]==3)){
				imagealphablending($newImg, false);
				imagesavealpha($newImg,true);
				$transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
				imagefilledrectangle($newImg, 0, 0, $width, $height, $transparent);
			}
	
			imagecopyresampled($newImg, $im, 0, 0, 0, 0, $width, $height, $imgInfo[0], $imgInfo[1]);
				
			//Generate the file, and rename it to $newfilename
			switch ($imgInfo[2]) {
				case 1: imagegif($newImg,$savePath,100); break;
				case 2: imagejpeg($newImg,$savePath,100);  break;
				case 3: imagepng($newImg,$savePath); break;
				default:  trigger_error('Failed resize image!', E_USER_WARNING);  break;
			}
			imagedestroy($im);
			//imagedestroy($tmp);
			imagedestroy($newImg);
		}
	
	}
	
	/**
	 * getWidthImage
	 * 
	 * get the width of image
	 * 
	 * @author Raman
	 * @return  $size
	 * @version 1.0
	 */
	public static function getWidthImage($file) {
	
		$size = getimagesize($file);
		return $size[0];
	}
	/**
	 * getHeightImage
	 * 
	 * get the height of image
	 * 
	 * @author Raman
	 * @return  $size
	 * @version 1.0
	 */
	public static function getHeightImage($file) {
	
		$size = getimagesize($file);
		return $size[1];
	}
	/**
	 * resizeToHeightImage
	 * 
	 * resize according to height of image
	 * 
	 * @author Raman
	 * @return $width
	 * @version 1.0
	 */
	public static function resizeToHeightImage($height,$file) {
	
		$ratio = $height / self::getHeightImage($file);
		$width = self::getWidthImage($file) * $ratio;
		return  $width;
	}
	/**
	 * resizeToWidthImage
	 * 
	 * resize according to width of image
	 * 
	 * @author Raman
	 * @return $height
	 */
	public static function resizeToWidthImage($width,$file) {
		$ratio = $width / self::getWidthImage($file);
		$height = self::getheightImage($file) * $ratio;
		return  $height;
	}
	
	/**
	 * getImageExtension
	 * 
	 * get the extension of the image
	 * 
	 * @author Raman
	 * @param string $filename
	 * @return string
	 */
	public static function getImageExtension($filename)
	{
	
		$pos = strrpos($filename,".");
		$ext = substr($filename,$pos+1);
		return $ext;
	
	}
	
	/**
	 * encrypt data to save in database
	 *
	 * @author pkaur4
	 * @param  array $plaintext
	 * @return array $plaintext
	 * @version 1.0
	 */
	public static function encryptData(&$plaintext,$key, $keyValue){
	
		$aes = new phpseclibmaster_phpseclib_Crypt_AES();
	
		$aes->enablePadding();
		$aes->setKey($keyValue);
	
		$dataToEncrypt = htmlentities($plaintext);
		$encrypt = $aes->encrypt($dataToEncrypt);
		$plaintext = base64_encode($encrypt);
		//$plaintext = rtrim($text, '\r\n');
	
	}
	
	/**
	 * recursively traverse each member of an array(which is to be encrypt)
	 *
	 * @author pkaur4
	 * @param  array $originalArray
	 * @return array $originalArray
	 * @version 1.0
	 */
	public static function encryptStrArray($originalArray, $keyValue){
	
		$obj = new self();
		array_walk_recursive($originalArray, array($obj, 'encryptData'), $keyValue);
		return $originalArray;
	
	}
	
	/**
	 * encrypt a string
	 *
	 * @author pkaur4
	 * @param  string $plaintext
	 * @return string $plaintext
	 * @version 1.0
	 */
	public static function encryptString($plaintext, $keyValue){
	
		$aes = new phpseclibmaster_phpseclib_Crypt_AES();
	
		$aes->enablePadding();
		$aes->setKey($keyValue);
	
		$dataToEncrypt = htmlentities($plaintext);
		$encrypt = $aes->encrypt($dataToEncrypt);
		$text = base64_encode($encrypt);
		//$plaintext = rtrim($text, '\r\n');
		return $text;
	
	}
	
	/**
	 * decrypt string
	 *
	 * @author pkaur4
	 * @param  string $plaintext
	 * @return string $decryptedString
	 * @version 1.0
	 */
	public static function decryptString($plaintext,$keyValue){
	
		$aes = new phpseclibmaster_phpseclib_Crypt_AES();
	
		$aes->enablePadding();
		$aes->setKey($keyValue);
	
		$decode = base64_decode($plaintext);
		$decryptedString = $aes->decrypt($decode);
		$text = html_entity_decode($decryptedString);
		return $text;
	}

	
	/**
	 * decrypt encrypted data
	 *
	 * @author pkaur4
	 * @param  array $list
	 * @return array $list
	 * @version 1.0
	 */
	public static function decryptData(&$list,$key, $keyValue){
	
		$aes = new phpseclibmaster_phpseclib_Crypt_AES();
	
		$aes->enablePadding();
		$aes->setKey($keyValue);
		$decode = base64_decode($list);
		$rlist = $aes->decrypt($decode);
		$list = html_entity_decode($rlist);
	
	}
	
	/**
	 * recursively traverse each member of an array(which is to be decrypt)
	 *
	 * @author pkaur4
	 * @param  array $originalArray
	 * @return array $originalArray
	 * @version 1.0
	 */
	public static function decryptStrArray($originalArray, $keyValue){
	
		$obj = new self();
		array_walk_recursive($originalArray, array($obj, 'decryptData'), $keyValue);
		return $originalArray;
	
	}
 

	public static function getFirstLastName($id){
	
		$db = Zend_Db_Table::getDefaultAdapter ();
	   	$calPro = $db->prepare ( "CALL AUDIT_CURRENT_INVESTIGATOR_CREATEDBY('$id')" );
	   	$calPro->execute ();
	   	$record = $calPro->fetchAll ();
	   	$fullName['firstname'] = $record[0]['firstname'];
	   	$fullName['lastname'] = $record[0]['lastname'];
	   	return $fullName;
	}
	
	/**
	
	* Specially made for zend pagination
	
	* url.
	
	*
	
	* @returns pagination buttons url.
	
	* @author skumar5
	
	*/
	
	public static function createUrlForZendPagination($params, $controllerr, $actionn)
	{
		$str = "/clinictrial/".$controllerr."/".$actionn."?";
		foreach ($params as $key=>$t)
		{
			if( is_array($t) )
			{
				foreach ($t as $keyy=>$tt)
				{
					$str .= $key."[]=".$tt."&";
				}
			}
		   else
			{
				$str .= $key."=".$t."&";
			}
		}
		return $str;	
	}
	
	
	/**
	
	* Specially made for zend pagination
	
	* url.
	
	*
	
	* @returns pagination buttons url.
	
	* @author skumar5
	
	*/
	
	public static function createUrlForZendPaginationInvestigator($params, $controllerr, $actionn)
	{
		$str = "/investigator/".$controllerr."/".$actionn."?";
		foreach ($params as $key=>$t)
		{
			if( is_array($t) )
			{
				foreach ($t as $keyy=>$tt)
				{
					$str .= $key."[]=".$tt."&";
				}
			}
			else
			{
				$str .= $key."=".$t."&";
			}
		}
		return $str;
	}
	
	
	/**
	 * get country name
	 *
	 * @author pkaur4
	 * @param  $id
	 * @return string $record
	 * @version 1.0
	 */
	public static function getCountryName($id){
	
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL COMMON_GET_COUNTRY_NAME('$id')" );
		$calPro->execute ();
		$record = $calPro->fetchAll ();
		return $record[0];
	}
	
	/**
	 * get state name
	 *
	 * @author pkaur4
	 * @param  $id
	 * @return string $record
	 * @version 1.0
	 */
	public static function getStateName($id){
	
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL COMMON_GET_STATE_NAME('$id')" );
		$calPro->execute ();
		$record = $calPro->fetchAll ();
		return $record[0];
	}
	
	/**
	 * get city name
	 *
	 * @author pkaur4
	 * @param  $id
	 * @return string $record
	 * @version 1.0
	 */
	public static function getCityName($id){
	
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL COMMON_GET_CITY_NAME('$id')" );
		$calPro->execute ();
		$record = $calPro->fetchAll ();
		return $record[0];
	}
	
	/**
	 * Check & Store configure study Id in Session
	 *
	 * @author vverma updated by pkaur4
	 * @param  $id
	 * @return string $record
	 * @version 1.0
	 */
	
	public static function checkStudyConfigueId($params = null){
			$module = @$params['module'];
			if(@$params['module'] == 'subject'){
				
				$user = new Zend_Session_Namespace ('Subject');
				$studyId = $user->subject_studyId;
				$userId = $user->user_data['id'];	
				$roleId = $user->user_data['role_id'];
			}else{
				
		    	$user = new Zend_Session_Namespace ('InvestigatorInfo');
		    	$studyId = $user->investigator_studyId; 
		    	$userId = $user->investigator_data[0]['u__id'];	    	
		    	$roleId = $user->investigator_data[0]['u__role_id'];
			}
			
	        $db = Zend_Db_Table::getDefaultAdapter ();
	        if($roleId == 2){
	        	$calPro = $db->prepare ( "CALL INVESTIGATOR_GET_STUDY_CONFIG_ID($studyId, $userId)" );
	        	
	        }elseif($roleId == 3){
	        	$calPro = $db->prepare("CALL STUDY_MANAGE_SECINVESTIGATOR_GET_STUDY_CONFIG_ID($studyId,$userId)");
	       	 }
	       	 elseif($roleId == 4){
	       	 	$calPro = $db->prepare("CALL SUBJECT_GET_STUDY_CONFIG_ID($studyId, $userId)" );
	       	 } 
	       	
	        $calPro->execute();
	        $record = $calPro->fetch();
	        if(isset($record['id']) && $record['id'] != ' '){
	    	  
	    	  $studyInfo = new Zend_Session_Namespace ('studyDetails');
	    	  $studyInfo->study_configue_id = $record['id'];	    	
	        }else {
	          $studyInfo = new Zend_Session_Namespace ('studyDetails');
	          $studyInfo->study_configue_id = 0;
	        }
	        
	        return $studyInfo->study_configue_id;
	}
	
	public static function checkStudyConfigueIdForSubject(){
			
		$user = new Zend_Session_Namespace ('Subject');
		$studyId = $user->subject_studyId;
		$userId = $user->user_data['id'];
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL SUBJECT_GET_CONFIG_ID($userId)" );
		$calPro->execute();
		$record = $calPro->fetch();
		return $record['study_conf_id'];
	}
	
	/**
	 * decryptWsData
	 * decrypt encrypted data for webservice
	 *
	 * @author Raman 
	 * @param  array $list
	 * @return array $list
	 * @version 1.0
	 */
	public static function decryptWsData(&$list,$key, $keyValue){
		
		$decrypt = true;
		$toSearch = array('_id', 'deleted_at', 'dateupdated', 'datecreated', 'created_by', 'updated_by', 'action', 'createdby', 'password');
		
		foreach($toSearch as $val){
			$pos = strpos($key, $val);
			
			if (!empty($pos)) {
				$decrypt = false;
			} 
		}
		if($decrypt){
			
			$aes = new phpseclibmaster_phpseclib_Crypt_AES();
			
			$aes->enablePadding();
			$aes->setKey($keyValue);
			$decode = base64_decode($list);
			$rlist = $aes->decrypt($decode);
			$list = html_entity_decode($rlist);
			
		} 		
	
	}
	
	/**
	 * decryptWsArray
	 * recursively traverse each member of an array(which is to be decrypt)
	 *
	 * @author Raman
	 * @param  array $originalArray
	 * @return array $originalArray
	 * @version 1.0
	 */
	public static function decryptWsArray($originalArray, $keyValue){
	
		$obj = new self();
		array_walk_recursive($originalArray, array($obj, 'decryptWsData'), $keyValue);
		return $originalArray;
	
	}
	
	/**
	 * strip tag function
	 *
	 * @author vverma
	 * @param  array $plaintext
	 * @return array $plaintext
	 * @version 1.0
	 */
	public static function stripFunction(&$plaintext){
		$plaintext = strip_tags($plaintext);
	}
	
	/**
	 * recursively traverse each member of an array(which is to be stripped)
	 *
	 * @author vverma
	 * @param  array $originalArray
	 * @return array $originalArray
	 * @version 1.0
	 */
	public static function stripHTMLTags($originalArray){
	
		$obj = new self();
		array_walk_recursive($originalArray, array($obj, 'stripFunction'));
		return $originalArray;
	
	}
	

	/**
	 * Check secondary investigator assigned permission
	 *
	 * @author pkaur4
	 * @param  $id
	 * @return string $record
	 * @version 1.0
	 */
	public static function checkSecInvestigatorPermission(){
	
		$user = new Zend_Session_Namespace ('InvestigatorInfo');
		$studyId = $user->investigator_studyId;
		$userId = $user->investigator_data[0]['u__id'];
		
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL STUDY_MANAGE_GET_SEC_INVESTIGATOR_PERMISSION($studyId, $userId)" );
		$calPro->execute ();
		$record = $calPro->fetch();
		return $record;
	}

	/**
	 * grouping of the array on first level
	 *
	 * @author Raman
	 * @param  array $data
	 * @param string $groupkey
	 * @param string $nestname
	 * @param string $innerkey
	 * @version 1.0
	 */
	public static function groupnest( $data, $groupkey, $nestname, $innerkey ) {
		$outer0 = array();
		$group = array(); $nested = array();
	
		foreach( $data as $row ) {
			$outer = array();
			while( list($k,$v) = each($row) ) {
				if( $k==$innerkey ) break;
				$outer[$k] = $v;
			}
	
			$inner = array( $innerkey => $v );
			while( list($k,$v) = each($row) ) {
				if( $k==$innerkey ) break;
				$inner[$k] = $v;
			}
	
			if( count($outer0) and $outer[$groupkey] != $outer0[$groupkey] ) {
				$outer0[$nestname] = $group;
				$nested[] = $outer0;
				$group = array();
			}
			$outer0 = $outer;
	
			$group[] = $inner;
		}
		$outer[$nestname] = $group;
		$nested[] = $outer;
	
		return $nested;
	}
	
  /**
   * checkStudyCompletion
   *
   * check if study configuration has completed or not
   *
   * @author vverma
   * @version 1.0
   */
  
  public static function checkStudyCompletion($studyConfigId){
  	$db = Zend_Db_Table::getDefaultAdapter ();
  	$calPro = $db->prepare("CALL WS_CHECK_STUDY_COMPLETION($studyConfigId)");
  	$calPro->execute();
  	$data = $calPro->fetch();
  	return $data['blocked'];
  }
  
  /**
   * checkStudyCompletion
   *
   * check if study configuration has completed or not
   *
   * @author vverma
   * @version 1.0
   */
  
  public static function checkStudyCompletionForAnalysis($studyConfigId){
  	$db = Zend_Db_Table::getDefaultAdapter ();
  	$calPro = $db->prepare("CALL CHECK_STUDY_COMPLETION_FOR_ANALYSIS($studyConfigId)");
  	$calPro->execute();
  	$data = $calPro->fetch();
  	return $data['block_for_analysis'];
  }
	
  /**
   * getDateDifference
   *
   * check the difference between changed password date and currnt date;
   *
   * @author skumar5
   * @version 1.0
   */
  
  public static function getDateDifference($username){
  	
  	$db = Zend_Db_Table::getDefaultAdapter ();
  	$calPro = $db->prepare("CALL GET_PASSWORD_DATE_DIFFERENCE('$username')");
  	$calPro->execute();
  	$data = $calPro->fetch();
  	$pwd_updated = $data["password_updated_on"];
  	$curr_date = date("Y-m-d h:i:s");
  	$ts1 = strtotime($pwd_updated);
  	$ts2 = strtotime($curr_date);	
  	$seconds_diff = $ts2 - $ts1; 
	$days = floor($seconds_diff/(24*60*60));
	return $days;
  }
  
  /**
   * getLanguages
   * Get all the languages and their locales 
   *
   * @author Raman
   * @version 1.0
   */
  public static function getLanguages(){
  
  		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL GET_ALL_LANGUAGES()" );
  		$calPro->execute ();
  		$languages = $calPro->fetchAll();
  		return $languages;
  
  }
  
}
