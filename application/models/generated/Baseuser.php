<?php

/**
 * Baseuser
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $password
 * @property timestamp $currentLogIn
 * @property timestamp $lastLogIn
 * @property string $type
 * @property integer $role_id
 * @property integer $image_id
 * @property integer $createdBy
 * @property role $roleOfUsers
 * @property image $userImages
 * @property user $usersOfCreator
 * @property Doctrine_Collection $investigatorOfStudy
 * @property Doctrine_Collection $creatorOfUser
 * @property Doctrine_Collection $UserOfCompany
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7691 2011-02-04 15:43:29Z jwage $
 */
abstract class Baseuser extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('user');
        $this->hasColumn('id', 'integer', 11, array(
             'unique' => true,
             'primary' => true,
             'type' => 'integer',
             'autoincrement' => true,
             'length' => '11',
             ));
        $this->hasColumn('firstName', 'string', 100, array(
             'type' => 'string',
             'length' => '100',
             ));
        $this->hasColumn('lastName', 'string', 100, array(
             'type' => 'string',
             'length' => '100',
             ));
        $this->hasColumn('email', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('password', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
        $this->hasColumn('currentLogIn', 'timestamp', null, array(
             'type' => 'timestamp',
             ));
        $this->hasColumn('lastLogIn', 'timestamp', null, array(
             'type' => 'timestamp',
             ));
        $this->hasColumn('type', 'string', 50, array(
             'type' => 'string',
             'length' => '50',
             ));
        $this->hasColumn('role_id', 'integer', 20, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => '20',
             ));
        $this->hasColumn('image_id', 'integer', 11, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => '11',
             ));
        $this->hasColumn('createdBy', 'integer', 11, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => '11',
             ));
        $this->hasColumn('status', 'boolean', null, array(
        		'default' => 0,
        		'type' => 'boolean',
        ));
        $this->setSubClasses(array(
             'investigator' => 
             array(
              'type' => 'investigator',
             ),
             'ct_admin' => 
             array(
              'type' => 'ct_admin',
             ),
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('role as roleOfUsers', array(
             'local' => 'role_id',
             'foreign' => 'id'));

        $this->hasOne('image as userImages', array(
             'local' => 'image_id',
             'foreign' => 'id'));

        $this->hasOne('user as usersOfCreator', array(
             'local' => 'createdBy',
             'foreign' => 'id'));

        $this->hasMany('studyrecord as investigatorOfStudy', array(
             'refClass' => 'refInvestigatorStudies',
             'local' => 'user_id',
             'foreign' => 'study_id'));

        $this->hasMany('user as creatorOfUser', array(
             'local' => 'id',
             'foreign' => 'createdBy'));

        $this->hasMany('ctcompany as UserOfCompany', array(
             'local' => 'id',
             'foreign' => 'created_by'));
		
        $this->hasMany('refusersecurity as refusersec', array(
        		'local' => 'id',
        		'foreign' => 'user_id'));
        
        $timestampable0 = new Doctrine_Template_Timestampable();
        $softdelete0 = new Doctrine_Template_SoftDelete(array(
             'options' => 
             array(
              'default' => 0,
             ),
             'type' => 'boolean',
             ));
        $this->actAs($timestampable0);
        $this->actAs($softdelete0);
    }
}