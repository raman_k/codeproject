<?php

/**
 * user
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7691 2011-02-04 15:43:29Z jwage $
 */
class user extends Baseuser
{
	/**
	 * set password of the user in SHA1 format
	 * @param string $password
	 * @author Raman 
	 * @version 1.0
	 */
	public function setPassword($password) {
		return $this->_set('password', $password);
	}
	/**
	 * validatePassword
	 * 
	 * check password if exist in database or not
	 * 
	 * @param string $passwordToBeVerified
	 * @author Raman updated by pkaur4
	 * @version 1.0
	 */
	public function validatePassword($passwordToBeVerified) {
	
		if ($this->password == $passwordToBeVerified) {
	
			return true;
		}
		return false;
	}
	/**
	 * validateEmail
	 * 
	 * check email in exist in database or not
	 * 
	 * @param string $emailToBeVerified
	 * @author Raman
	 * @version 1.0
	 */
	public function validateEmail($emailToBeVerified) {
		if ($this->email == ($emailToBeVerified)) {
			return true;
		}
		return false;
	}
	/**
	 * updatePassword
	 * 
	 * update password in exist in database or not for the frontend
	 * 
	 * @param string $passwordToBeModfified
	 * @author Raman
	 * @version 1.0
	 */
public static function updatePassword($user_id,$passwordd) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL USER_UPDATE_PASSWORD($user_id,'$passwordd')" );
		$calPro->execute ();
	}
	/**
	 * getUserDetails
	 * 
	 * get session user detail from databse
	 * 
	 * @param integer $id
	 * @author Raman
	 * @version 1.0
	 * @return array $data;
	 */
	public static function getUserDetails($id){		
		$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL USER_DETAILS_IDBASIS($id)" );
			$calPro->execute ();
			$data = $calPro->fetchAll ();
		return $data[0];
	}
	/**
	 * checkUserByPassword
	 *
	 * check user by password
	 *
	 * @param array $parms
	 * @author Raman
	 * @version 1.0
	 * @return array $data;
	 */
public static function checkUserByPassword($passwordToBeVerified){
		
		$pwd = Auth_ClinictrialAdapter::getIdentity();
		$pwd = $pwd[0]['u__password'];
		if ($pwd == $passwordToBeVerified) {
			return true;
		}else{
			return false;			
		}
	}
	
	public static function checkUserPassword($passwordToBeVerified , $username){
	
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL USER_MATCH_PASSWORD('$username')" );
		$calPro->execute ();
		$data = $calPro->fetch ();
		$pwd = $data['password'];
		if ($pwd == $passwordToBeVerified) {
			return true;
		}else{
			return false;			
		}
	}
	
	/**
	 * checkSubjectByPassword
	 *
	 * check subject by password
	 *
	 * @param array $parms
	 * @author skumar5
	 * @version 1.0
	 * @return array $data;
	 */
	public static function checkSubjectByPassword($passwordToBeVerified){
	
		$pwd = Auth_SubjectAdapter::getIdentity();
		
		$pwd = $pwd->password;
		if ($pwd == $passwordToBeVerified) {
			return true;
		}else{
			return false;
		}
	
	}
	
	/**
	 * findUser
	 *
	 * Check existing user
	 *
	 * @param array $params
	 * @author vishal
	 * @version 1.0
	 * @return array $result;
	 */
	public function findUser($params) {
	
		$email = @$params ['email'];
		$passwrd = @sha1 ( $params ['password'] );
		$role = $params ['roleId'];
	
		$data = Doctrine_Query::create ()->select ( "u.id,u.firstname,u.lastname,u.email" )->from ( 'users u' )->where ( 'u.email=' . "'$email'" )->andWhere ( 'u.password=' . "'$passwrd'" )->andWhere ( 'u.role_id=' . "'$role'" )->fetchArray ();
	    $result = @$data [0];	
		return $result;
	}
	
	/**
	 * get password and salt for user
	 *
	 * @param array $params
	 * @author pkaur4 updated by Raman
	 * @version 1.0
	 * @return array $result;
	 */
	   public static function getPassSal($params,$aes_Key){ 
	   	$username = BackEnd_Helper_viewHelper::encryptString($params['username'], $aes_Key);
	   	
	   	//print_r($username);die;
			$emailOrg = $username;
			//$email = "'".$emailOrg."'";
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL USER_GET_SALT_VALUE('$emailOrg')" );
			$calPro->execute ();
			$data = $calPro->fetchAll ();
			return $data[0];
		}
		
		
		/**
		 * get password and salt for subject
		 *
		 * @param array $params
		 * @author skumar5
		 * @version 1.0
		 * @return array $params;
		 */
		public static function getSubjectPassSal($params,$aes_Key){
			$username = BackEnd_Helper_viewHelper::encryptString($params['username'], $aes_Key);
			 
			//print_r($username);die;
			$emailOrg = $username;
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL SUBJECT_GET_SALT_VALUE('$emailOrg')" );
			$calPro->execute ();
			$data = $calPro->fetchAll ();
			return $data[0];
		}

		
		/**
		 * get password and salt for user
		 *
		 * @param array $params
		 * @author pkaur4 updated by Raman
		 * @version 1.0
		 * @return array $result;
		 */
		public static function getPassSalUsername($username){
			
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL USER_GET_SALT_VALUE_FOR_USERNAME('$username')" );
			$calPro->execute ();
			$data = $calPro->fetchAll ();
			return $data[0];
		}
		
	/**
	 * get password and salt for investigator
	 *
	 * @param array $params
	 * @author pkaur4
	 * @version 1.0
	 * @return array $result;
	 */
	public static function getInvestigatorPassSal($uname){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL USER_INVESTIGATOR_GET_SALT_VALUE('$uname')" );
		$calPro->execute ();
		$data = $calPro->fetchAll();
		return @$data[0];
	
	}
	/**
	 * updatePassword with new salt
	 * @param string $updpwd
	 * @author pkaur4 updated by Raman
	 * @version 1.0
	 */
	public static function updatePwd($id, $updpwd) {
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL USER_UPDATE_PASSWORD($id, '$updpwd')" );
		$calPro->execute();
		return true;
	}
	
	/**
	 * get investigator deviceId
	 * @param array $params
	 * @author pkaur4 updated by Raman
	 * @version 1.0
	 */
	public static function getUserDeviceId($urname){

		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL USER_INVESTIGATOR_AUTHENTHICATE('$urname')" );
		$calPro->execute();
		$data = $calPro->fetchAll();
		return $data[0];
	}
	
	public static function getPasswordSalt($email){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL USER_GET_SALT_VALUE('$email')" );
		$calPro->execute ();
		$data = $calPro->fetchAll ();
		return $data[0];
	}
	
	public static function passwordSalt($username){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL USER_GET_SALT('$username')" );
		$calPro->execute ();
		$data = $calPro->fetchAll ();
		return $data[0];
	
	}
	
	/**
	 * updateDeviceSession 
	 * @param string $comingDeviceIdEnc $session_id
	 * @author Raman
	 * @version 1.0
	 */
	public static function updateDeviceSession($userId, $comingDeviceIdEnc, $session_id) {
		
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL USER_UPDATE_SESSION_DEVICE($userId, '$comingDeviceIdEnc', '$session_id')" );
		$calPro->execute();
		
	}
	
	/**
	 * checkDeviceSession
	 * @param string $comingDeviceIdEnc $session_id
	 * @author Raman
	 * @version 1.0
	 */
	public static function checkDeviceSession($userId, $session_id) {
	
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL USER_CHECK_SESSION_DEVICE($userId, '$session_id')" );
		$calPro->execute();
		$data = $calPro->fetchAll ();
		if(!empty($data)){
			return true;
		} else {
			return false;
		}
		
	}
	
	
	public static function checkEmailAddress($email, $aes_Key){
		
		$encryptPrjStatus = BackEnd_Helper_viewHelper::encryptString($email, $aes_Key);
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL USER_CHECK_EMAIL('$encryptPrjStatus', 1)" );
		$calPro->execute ();
		$data = $calPro->fetchAll ();
		return @$data[0];	
	}
	
	/**
	 * check investigator and deviceId combination
	 * @param $urname, $deviceId
	 * @author Raman
	 * @version 1.0
	 */
	public static function checkUserDeviceId($urname, $deviceIdEnc){
	
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL USER_CHECK_DEVICEID('$urname', '$deviceIdEnc')" );
		$calPro->execute();
		$data = $calPro->fetchAll();
		if(!empty($data)){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * insertDeviceSession
	 * @param string $comingDeviceIdEnc $session_id
	 * @author Raman
	 * @version 1.0
	 */
	public static function insertDeviceSession($userId, $comingDeviceIdEnc, $session_id) {
	
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL  USER_INSERT_SESSION_DEVICE($userId, '$comingDeviceIdEnc', '$session_id')" );
		$calPro->execute();
	
	}
	
	/**
	 * checkSession
	 * 
	 * check Investigator device session
	 * @param string $session_id
	 * @author Raman
	 * @version 1.0
	 */
	public static function checkSession($session_id) {
	
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL WS_CHECK_SESSION ('$session_id')" );
		$calPro->execute();
		$data = $calPro->fetchAll();
		return $data;
	
	}
	

	/**
	 * get all permissions for secondary investigator from database
	 *
	 * @author pkaur4
	 * @version 1.0
	 */
	public static function getAllPermissions(){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL STUDY_MANAGE_GET_PERMISSIONS()" );
		$calPro->execute ();
		$data = $calPro->fetchAll();
		return $data;
	}
	
	/**
	 * get secondary-investigator list from database
	 * @param INT $flag,$configId
	 * @author pkaur4
	 * @version 1.0
	 */
	public static function getSubInvestigatorList($investigator_id, $flag, $configId, $aes_Key){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$isActive = BackEnd_Helper_viewHelper::encryptString(1, $aes_Key);
		$calPro = $db->prepare ( "CALL STUDY_MANAGE_SUBINVESTIGATOR_LIST($investigator_id, $flag, $configId, '$isActive')" );
		$calPro->execute ();
		$data = $calPro->fetchAll();
		return $data;
	}
	
	/**
	 * add new secondary-investigator
	 *
	 * 
	 * @param Array $params
	 * @author pkaur4
	 * @version 1.0
	 */
	public static function addSecInvestigator($params, $aes_Key) {
		$params = BackEnd_Helper_viewHelper::stripHTMLTags($params);
		$permissionId = $params['permission'];
		$userID = @$params['userId'];
		
		$ip_address = $_SERVER['REMOTE_ADDR'];
		$ip_address = BackEnd_Helper_viewHelper::encryptString($ip_address, $aes_Key);
		$createdBy = Auth_ClinictrialAdapter::getIdentity ();
		$uId = $createdBy[0]['u__id'];
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		
		
		$params = BackEnd_Helper_viewHelper::encryptStrArray($params, $aes_Key);
		$fName = $params['fname'];
		$lName = $params['lname'];
		$email = $params['email'];
		$status = $params['user_status'];
		$isActive = BackEnd_Helper_viewHelper::encryptString(0, $aes_Key);
		
		$type = BackEnd_Helper_viewHelper::encryptString('secinvestigator', $aes_Key);
		$roleId = 3;
		
		if(isset($userID) && $userID!=null || $userID!=''){
			
			#save data in subinvestigator table
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL STUDY_MANAGE_REF_SUBINVESTIGATOR_SAVE($configId, $userID, $uId, '$ip_address', 'CREATED')" );
			$calPro->execute ();
			$data = $calPro->fetch ();
			$linsertedId = $data ['lastId'];
			$subInvesId = $linsertedId;
			
			#save sub-investigator permission
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL STUDY_MANAGE_REF_SUBINVESTIGATOR_PRMSN_SAVE($subInvesId, $permissionId, $uId, '$ip_address', 'CREATED')" );
			$calPro->execute ();
		}
		else{
			
			#save sub-investigator in user table
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL INVESTIGATOR_SAVE('$fName', '$lName', '$email', '$type', $roleId, '$status', '$ip_address', $uId, 'CREATED', '$isActive')" );
			$calPro->execute ();
			$data = $calPro->fetch ();
			$lastId = $data ['lastId'];
			$userId = $lastId;
			
			#save data in ref table
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL STUDY_MANAGE_REF_SUBINVESTIGATOR_SAVE($configId, $userId, $uId, '$ip_address', 'CREATED')" );
			$calPro->execute ();
			$data = $calPro->fetch ();
			$linsertedId = $data ['lastId'];
			$subInvesId = $linsertedId;
			
			#save sub-investigator permission
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL STUDY_MANAGE_REF_SUBINVESTIGATOR_PRMSN_SAVE($subInvesId, $permissionId, $uId, '$ip_address', 'CREATED')" );
			$calPro->execute ();
			return $userId;
		}
		
	}
	
	/**
	 * get sec investigator data from database
	 *
	 * @param INT $id
	 * @author pkaur4
	 * @version 1.0
	 */
	public static function getSecInvestigatorData($id, $aes_Key){
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL STUDY_MANAGE_SUBINVESTIGATOR_GET_DATA($id,$configId)" );
		$calPro->execute ();
		$data = $calPro->fetchAll();
		$newArry = array();
		$newArry['id'] = $data[0]['userId'];
		$newArry['FName'] = BackEnd_Helper_viewHelper::decryptString($data[0]['fname'], $aes_Key);
		$newArry['LName'] = BackEnd_Helper_viewHelper::decryptString($data[0]['lname'], $aes_Key);
		$newArry['Email'] = BackEnd_Helper_viewHelper::decryptString($data[0]['email'], $aes_Key);
		$newArry['status'] = BackEnd_Helper_viewHelper::decryptString($data[0]['userstatus'], $aes_Key);
		$newArry['permission'] = $data[0]['permission_id'];
		return $newArry;
	}
	
	/**
	 * update secondary investigator
	 *
	 * @param Array $params
	 * @author pkaur4
	 * @version 1.0
	 */
	public static function updateSecInvestigator($params,$aes_Key){
		$ip_address = $_SERVER['REMOTE_ADDR'];
		$ip_address = BackEnd_Helper_viewHelper::encryptString($ip_address, $aes_Key);
		
		$permissionId = $params['permissions'];
		$uId = $params['subInvesId'];
		$firstName = BackEnd_Helper_viewHelper::encryptString($params['fname'], $aes_Key);
		$lastName = BackEnd_Helper_viewHelper::encryptString($params['lname'], $aes_Key);
		//$email = BackEnd_Helper_viewHelper::encryptString($params['email'], $aes_Key);
		$status = BackEnd_Helper_viewHelper::encryptString($params['user_status'], $aes_Key);
		
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		
		$obj = Auth_InvestigatorAdapter::getIdentity ();
		$invesId = $obj[0]['u__id'];
	
		#update sub-investigator in user table
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL INVESTIGATOR_UPDATE('$firstName', '$lastName', '$status', '$ip_address', $invesId, $uId, 'UPDATED')" );
		$calPro->execute ();
		
		#update data in ref table
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL STUDY_MANAGE_REF_SUBINVESTIGATOR_UPDATE($configId, $uId, $invesId, '$ip_address', 'UPDATED')" );
		$calPro->execute ();
		$data = $calPro->fetch ();
		$lupdatedId = $data ['lastupdatedId'];
		$subInvesId = $lupdatedId;
		
		#update sub-investigator permission
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL STUDY_MANAGE_REF_SUBINVESTIGATOR_PRMSN_UPDATE($subInvesId, $permissionId, $invesId, '$ip_address', 'UPDATED')" );
		$calPro->execute ();
	}
	
	/**
	 * delete secondary-investigator 
	 *
	 * @param int $id
	 * @author pkaur4
	 * @version 1.0
	 */
	public static function deleteSecInvestigatorRecord($params, $aes_Key){
		
		$ip_address = $_SERVER['REMOTE_ADDR'];
		$ip_address = BackEnd_Helper_viewHelper::encryptString($ip_address, $aes_Key);
	
		$obj = Auth_InvestigatorAdapter::getIdentity ();
		$invesId = $obj[0]['u__id'];
		
		$id = $params['id'];
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL STUDY_MANAGE_SECINVESTIGATOR_DELETE($id, $invesId, '$ip_address', 'DELETED')" );
		$calPro->execute ();
		return true;
	}
	
	/**
	 * search secondary investigators
	 *
	 * @param int $flag=0
	 * @author pkaur4
	 * @version 1.0
	 */
	public static function searchSecInvestigators($flag){
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		
		$obj = Auth_InvestigatorAdapter::getIdentity ();
		$invesId = $obj[0]['u__id']; 
		
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL STUDY_MANAGE_SEARCH_GET_SEC_INVESTIGATORS_LIST($flag, $invesId, $configId)" );
		
		$calPro->execute ();
		$data = $calPro->fetchAll();
		//print_r($data); die;
		return $data;
	}
	
	/**
	 * get searched sec investigator data from database
	 *
	 * @param INT $id
	 * @author pkaur4
	 * @version 1.0
	 */
	public static function getSearchSecInvestigator($id, $aes_Key){
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL STUDY_MANAGE_SEARCH_SUBINVESTIGATOR_GET_DATA($id)" );
		$calPro->execute ();
		$data = $calPro->fetchAll();
		$newArry = array();
		$newArry['id'] = $data[0]['userId'];
		$newArry['FName'] = BackEnd_Helper_viewHelper::decryptString($data[0]['fname'], $aes_Key);
		$newArry['LName'] = BackEnd_Helper_viewHelper::decryptString($data[0]['lname'], $aes_Key);
		$newArry['Email'] = BackEnd_Helper_viewHelper::decryptString($data[0]['email'], $aes_Key);
		$newArry['status'] = BackEnd_Helper_viewHelper::decryptString($data[0]['userstatus'], $aes_Key);
		$newArry['permission'] = $data[0]['permission_id'];
		return $newArry;
	}
	
    public static function excelExportData($params, $configId, $aes_Key){
		$siteId = $params['site'];
		$visitId = $params['visit'];
		$groupId = $params['groups'];
		$formId = $params['form'];
                if($params['sub_status'] == 0){
                  $subjectStatusId = 0;
                }else {
		  $subjectStatusId = BackEnd_Helper_viewHelper::encryptString($params['sub_status'], $aes_Key);
                }
		$fieldStatusId = $params['fieldstatus'];
		$sites = implode(',' , $siteId);
		$visits = implode(',' , $visitId);
		$groups = implode(',' , $groupId);
		$forms = implode(',' , $formId);
		
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL STUDY_ANALYZE_EXCEL_EXPORT('$sites', '$visits', '$groups', '$forms', $fieldStatusId, '$subjectStatusId', $configId)" );
		$calPro->execute ();
		$data = $calPro->fetchAll();
		
		return $data;	
	}
	
	/**
	 * display subject report data
	 *
	 * @param $params
	 * @author pkaur4
	 * @version 1.0
	 */
	public static function getSubjectData($params, $configId){
		
		$siteId = $params['site'];
		$visitId = $params['visit'];
		$subjectStatusId = $params['sub_status'];
		$groupId = $params['groups'];
		$formId = $params['subform'];
		$formVariablesId = $params['form_variables'];
		
		$sId = implode(',',$siteId);
		$sStatus= implode(',',$subjectStatusId);
		$fId = implode(',',$formId);
		$gId = implode(',',$groupId);
		$vId = implode(',',$visitId);
		$fvId = implode(',',$formVariablesId);
		
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ("CALL STUDY_MANAGE_GET_REPORT_DATA('$sId', '$vId', '$sStatus', '$gId', '$fId', '$fvId', $configId)" );
		$calPro->execute ();
		$data = $calPro->fetchAll();
		return $data;
	}
	
	/**
	 * display field report data
	 *
	 * @param $params
	 * @author pkaur4
	 * @version 1.0
	 */
	public static function getSubjectDataForFieldReport($params, $configId){
		$siteId = $params['site'];
		$visitId = $params['f_visit'];
		$fieldStatusId = $params['fieldstatus'];
		$groupId = $params['groups'];
		$formId = $params['fldform'];
		$subjectStatusId = $params['sub_status'];
		
		$sId = implode(',',$siteId);
		$vId = implode(',',$visitId);
		$fldId = implode(',',$fieldStatusId);
		$gId = implode(',',$groupId);
		$fId = implode(',',$formId);
		$sStatus= implode(',',$subjectStatusId);
		
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ("CALL STUDY_MANAGE_GET_FIELD_REPORT_DATA('$sId', '$vId', '$fldId', '$gId', '$fId', '$sStatus', $configId)" );
		$calPro->execute ();
		$data = $calPro->fetchAll();
		return $data;
	}
	
	/**
	 * getSubjects
	 * 
	 * @param number $studyConfId
	 * @author Raman
	 * @version 1.0
	 */
	public static function getSubjects($studyConfId){

		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL SUBJECTS_GET_ALL_SUBJECTS_DETAILS($studyConfId)" );
		$calPro->execute ();
		$data = $calPro->fetchAll();
		return $data;
	
	}
	
	/**
	 * getSubjectsData
	 *
	 * @param number $studyConfId
	 * @author Raman
	 * @version 1.0
	 */
	public static function getSubjectsData($id){
	
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL SUBJECT_GET_DETAILS ($id)" );
		$calPro->execute ();
		$data = $calPro->fetchAll();
		return $data;
	
	}

	/**
	 * getEnrolledSubjectData
	 *
	 * @param number $studyConfId
	 * @author vishal
	 * @version 1.0
	 */
	public static function getEnrolledSubjectData($enrollment, $configId){
	
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL INVESTIGATOR_SUBJECT_ENROLLED_CHART ($enrollment, $configId)" );
		$calPro->execute ();
		$data = $calPro->fetchAll();
		
		return $data;
	
	}
	
	/**
	 * getEnrolledSubjectSiteWiseData
	 *
	 * @param number $studyConfId
	 * @author vishal
	 * @version 1.0
	 */
	public static function getEnrolledSubjectSiteWiseData($configId, $aes_Key){
		$flag =  0;
		$db = Zend_Db_Table::getDefaultAdapter ();
		$calPro = $db->prepare ( "CALL STUDY_CONF_SITES_LIST($flag, $configId)" );
		$calPro->execute ();
		$result = $calPro->fetchAll(); 
		unset($calPro);

        $newArray = array();

		foreach($result as $key=>$data){ 
	      $enrollment = BackEnd_Helper_viewHelper::decryptString($data['enrollmentgoal'], $aes_Key);
              $siteName = BackEnd_Helper_viewHelper::decryptString($data['name'], $aes_Key);
	      $sId = $data['id'];

	      $db = Zend_Db_Table::getDefaultAdapter ();
	      $calPro = $db->prepare ( "CALL INVESTIGATOR_SUBJECT_ENROLLEMENT_SITE_WISE_CHART($enrollment, $configId, $sId)" );
	      $calPro->execute ();
	      $result = $calPro->fetchAll();
	      $newArray[] = $result;
              $newArray[$key]['siteName'] = $siteName;
		}
	return $newArray;
		}
		
		/**
		 * getEnrolledSubjectGroupWiseData
		 *
		 * @param number $studyConfId
		 * @author vishal
		 * @version 1.0
		 */
	public static function getEnrolledSubjectGroupWiseData($configId, $aes_Key){
			$flag =  0;
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL STUDY_GROUPS_LIST($flag, $configId)" );
			$calPro->execute ();
			$result = $calPro->fetchAll();
			unset($calPro);

			$newArray = array();
			foreach($result as $key=>$data){
				$enrollment = BackEnd_Helper_viewHelper::decryptString($data['enrollmentgoal'], $aes_Key);
                                $groupName = BackEnd_Helper_viewHelper::decryptString($data['name'], $aes_Key);
				$gId = $data['id'];
				 
				$db = Zend_Db_Table::getDefaultAdapter ();
				$calPro = $db->prepare ( "CALL INVESTIGATOR_SUBJECT_ENROLLEMENT_GROUP_WISE_CHART($enrollment, $configId, $gId)" );
				$calPro->execute ();
				$result = $calPro->fetchAll();		       
				$newArray[] = $result;
                                $newArray[$key]['groupName'] = $groupName;
			}
			return $newArray;
		}
		
		/**
		 * display summary report data
		 *
		 * @param $params
		 * @author Raman
		 * @version 1.0
		 */
		public static function getSubjectDataForSummaryReport($params, $configId){
			
			$siteId = $params['site'];
			$subjectStatusId = $params['sub_status'];
			$formId = $params['sumform'];
			$groupId = $params['groups'];
			$visitId = $params['visit'];
			$subjects = $params['subjects'];
		
			$sId = implode(',', $siteId); 
			$sStatus= implode(',', $subjectStatusId);
			$fId = implode(',', $formId);
			$gId = implode(',', $groupId);
			$vId = implode(',', $visitId);
			$subjects = implode(',', $subjects);
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ("CALL STUDY_MANAGE_GET_SUMMARY_REPORT_DATA('$sId', '$vId', '$sStatus', '$gId', '$fId', '$subjects', $configId)" );
			
			//print_r($calPro) ; die;
			$calPro->execute ();
			$data = $calPro->fetchAll();
			return $data;
		}
		
		/**
		* Check duplicate email
		*
		* checkInvestigatorEmail
		* 
		* @param $params
		* @author Raman
		* @version 1.0
		*/
		public static function checkInvestigatorEmail($email)
		{
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL CHECK_INVESTIGATOR_EMAIL('$email')");
			$calPro->execute ();
			$data = $calPro->fetchAll ();
			if(count($data) > 0){
				return false;
			}else{
				return true;
			}
		}
		
		/**
		 * getSubjectsData
		 *
		 * @param number $studyConfId
		 * @author vishal
		 * @version 1.0
		 */
		public static function saveSubjectBasicInfo($emailId, $subId, $userId, $firstName, $lastName, $dob, $aes_Key){
			
			$status = BackEnd_Helper_viewHelper::encryptString(1, $aes_Key);
			$activeStatus = BackEnd_Helper_viewHelper::encryptString(0, $aes_Key);
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL WS_SAVE_SUBJECT_BASIC_DATA ('$emailId', '$subId', $userId, '$firstName', '$lastName', '$dob', '$status', '$activeStatus')" );
			$calPro->execute ();
			$data = $calPro->fetch();
			$lastId = $data ['lastId'];
			return $lastId;
		
		}
		
		/**
		 * getSubjectsData
		 *
		 * @param number $studyConfId
		 * @author vishal
		 * @version 1.0
		 */
		public static function saveSubjectCommonData($configId, $subId){

			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL WS_SAVE_SUBJECT_COMMON_DATA ($configId, $subId)" );
			$calPro->execute ();
			$data = $calPro->fetch();
			$lastId = $data ['lastId'];
			return $lastId;
		
		}
		
		/*Login as a Subject*/
		
		/**
		 * getsubjectDetails
		 *
		 * @param $uId
		 * @author skumar5
		 * @version 1.0
		 */
		public static function getsubjectDetails($subId){
		
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL SUBJECT_GET_ID_AND_STUDY ($subId)" );
			$calPro->execute ();
			$data = $calPro->fetch();
			return $data;		
		}
		
		/**
		 * getStudiesBySubject
		 *
		 * @param $username, $newpass, $role, $aes_Key
		 * @author skumar5
		 * @version 1.0
		 */
		public static function getStudiesBySubject($username, $newpass, $role, $aes_Key){
			
			$studystatus = BackEnd_Helper_viewHelper::encryptString(1, $aes_Key);
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL SUBJECT_GET_STUDIES ('$username', '$newpass' , '$studystatus')" );
			$calPro->execute ();
			$data = $calPro->fetchAll();
			
			if(!empty($data)){
	   			$newArra = array();
	   			foreach($data as $key => $value){
	   				$newArra[$key]['id'] = $value['id'];
	   				$newArra[$key]['title'] =  BackEnd_Helper_viewHelper::decryptString($value['title'], $aes_Key);
	   			}
	   		}
			return $newArra;
		}
		
		
		public static function saveSubjectSignatureData($investigatorSig, $subjectSig, $siteId, $groupId, $subStatus, $statusDate, $studyConfigId, $saveUserDetails, $host, $dbName, $username, $pass){
			
			$Conn = mysql_connect($host, $username, $pass) or die("Couldn't connect to database server");
			mysql_select_db($dbName, $Conn) or die("Couldn't connect to database $dbName");
			
			$sqlquery = "CALL WS_SAVE_SUBJECT_SIGNATURE_DATA ('$investigatorSig', '$subjectSig', '$siteId', '$groupId', '$subStatus', '$statusDate', $studyConfigId, $saveUserDetails)";
			$data = mysql_fetch_array(mysql_query($sqlquery));
			/* $calPro = $db->prepare ( "CALL WS_SAVE_SUBJECT_SIGNATURE_DATA ('$investigatorSig', '$subjectSig', $siteId, $groupId, $studyConfigId, $saveUserDetails)" );
			$calPro->execute ();
			$data = $calPro->fetchAll(); */
		}
		
		public static function checkExistingSubject($subjectId){
			
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL WS_CHECK_EXISTING_SUBJECT ('$subjectId')" );
			$calPro->execute ();
			$data = $calPro->fetch();
			return $data;
		}			
		

		/**
		 * updateSubjectBasicInfo
		 *
		 * @param number $studyConfId
		 * @author vishal
		 * @version 1.0
		 */
		public static function updateSubjectBasicInfo($emailId, $subId, $firstName, $lastName, $dob){
				
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL WS_UPDATE_SUBJECT_BASIC_DATA ('$emailId', '$subId', '$firstName', '$lastName', '$dob')" );
			$calPro->execute ();		
		}
		
		/**
		 * getSubjectCommonDataId
		 *
		 * @param number $studyConfId
		 * @author vishal
		 * @version 1.0
		 */
		public static function getSubjectCommonDataId($subBasicId){
		
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL WS_GET_SUBJECT_COMMON_DATA_ID ($subBasicId)" );
			$calPro->execute ();
			$data = $calPro->fetch();
			return $data['id'];
		}
		
		public static function updateNewPassword($username , $new_password){
		
			$db = Zend_Db_Table::getDefaultAdapter ();
			$calPro = $db->prepare ( "CALL USER_UPDATE_PWD('$username', '$new_password')" );
			$calPro->execute();
			return true;
		}
		
}