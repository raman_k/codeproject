<?php
/**
 * load all function,view required for zend 
 * @author Raman
 *
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
	
	
	protected $lang = null ;
	protected $moduleNames = array();

	/**
	 * Defined docoment type of view , meta description and head title of view
	 * 
	 */
	protected function _initDocType() {
		
		$this->bootstrap('View');
		$view = $this->getResource('View');
		$view->doctype('XHTML1_STRICT');
		// Set the initial title and separator:
		$view->headTitle('clinitrial')->setSeparator(' :: ');
		$view->headMeta()
				->appendHttpEquiv('Content-type', 'text/html; charset=UTF-8')
				->appendName('description', 'mySite');
		
	}
	/**
	 * set all path of application,form,model etc
	 * @return Zend_Loader_Autoloader
	 */
	protected function _initAutoLoad(){
		$autoLoader = Zend_Loader_Autoloader::getInstance();
		$resourceLoader = new Zend_Loader_Autoloader_Resource(
				array('basePath' => APPLICATION_PATH,
						'namespace' => 'Application',
						'resourceTypes' => array(
								'form' => array('path' => 'forms/',
										'namespace' => 'Form'),
								'model' => array('path' => 'models/',
										'namespace' => 'Model'))));
		return $autoLoader;
	}
	protected function _initNavigation() {
	}
	/**
	 * Set module level layout and doctype of rendered view
	 */
	protected function _initSiteModules() {
		//Don't forget to bootstrap the front controller as the resource may not been created yet...
		$this->bootstrap("frontController");
		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper(
				'viewRenderer');
		$viewRenderer->initView();
		$viewRenderer->view->doctype('XHTML1_STRICT');
		$front = $this->getResource("frontController");
		//Add modules dirs to the controllers for default routes...
		$front->addModuleDirectory(APPLICATION_PATH . '/modules');

	}
	/**
	 * Create connection with database by doctrine and
	 * defined model ,time zone and get dsn(doman name server)
	 * @return Ambigous <Doctrine_Connection, multitype:>
	 */
	protected function _initDoctrine() {

		spl_autoload_register(array('Doctrine', 'modelsAutoload'));

		$manager = Doctrine_Manager::getInstance();
		//$manager->setAttribute(Doctrine_Core::ATTR_TBLNAME_FORMAT, $doctrineOptions["prefix"] . '_%s');

		$manager
				->setAttribute(Doctrine_Core::ATTR_MODEL_LOADING,
						Doctrine_Core::MODEL_LOADING_CONSERVATIVE);
		$manager
				->setAttribute(Doctrine_Core::ATTR_AUTO_ACCESSOR_OVERRIDE, true);
		$manager->setAttribute(Doctrine::ATTR_AUTOLOAD_TABLE_CLASSES, true);

		Doctrine_Core::loadModels(APPLICATION_PATH . '/models');

		$doctrineOptions = $this->getOption('doctrine');

		$conn = Doctrine_Manager::connection($doctrineOptions["dsn"],
				"doctrine");
		date_default_timezone_set('Asia/Calcutta');

		return $conn;
	}
	/**
	 * defined and set module level layout
	 */
	protected function _initLayoutHelper() {
		$this->bootstrap('frontController');
		$layout = Zend_Controller_Action_HelperBroker::addHelper(
				new ModuleLayoutLoader());
	}
	protected function _initConstants()
	{
		
		$registry = Zend_Registry::getInstance();
		$registry->constants = new Zend_Config( $this->getApplication()->getOption('constants') );
		$registry->resources = new Zend_Config( $this->getApplication()->getOption('resources') );
	}
	
	 /**
	 * _initTranslation
	 * 
	 * Set the initial translation
	 * 
	 * @author Raman
	 * @version 1.0
	 */
	function _initTranslation(){
	
	
		//define by default locale of the site
		if(isset($_COOKIE['language'])){
			$locale = $_COOKIE['language'];
		} else {
			$locale = 'en_US';
		}
	
		//die($locale);
		$trans = new Zend_Translate(array(
				'adapter' => 'gettext',
				'disableNotices' => true
				
		));
	
		$jsLanguageFile = '';
		if($locale != 'en_US'){
			
			$locale = new Zend_Locale( $locale );
			Zend_Registry::set('Zend_Locale', $locale);
			$trans->addTranslation(
					array(
							'content' => ROOT_PATH.'languages/clinitrial_'.$locale.'.mo',
							'locale' => $locale,
					)
			);
			
			//js language file
			$jsLanguageFile =  ROOT_PATH . 'languages/js/clinitrial_js_'.$locale.'.po';
			
		} 
		
		
		# define language file constant for js
		defined('JS_LANGUAGE_FILE')
		|| define('JS_LANGUAGE_FILE', $jsLanguageFile  );
			
		Zend_Registry::set('Zend_Translate', $trans);
		$locale = new Zend_Locale( $locale );
		Zend_Registry::set('Zend_Locale', $locale); 

	}
}

/**
 * load module layout by geting value from application file
 * and set layout for module
 * @author Raman
 *
 */
class ModuleLayoutLoader extends Zend_Controller_Action_Helper_Abstract {
	public function preDispatch() {
		$bootstrap = $this->getActionController()->getInvokeArg('bootstrap');
		$config = $bootstrap->getOptions();
		$module = $this->getRequest()->getModuleName();
		if (isset($config[$module]['resources']['layout']['layout'])
				&& isset($config[$module]['resources']['layout']['layoutPath'])) {
			$layoutScript = $config[$module]['resources']['layout']['layoutPath'];
			$layoutName = $config[$module]['resources']['layout']['layout'];

			$this->getActionController()->getHelper('layout')
					->setLayoutPath($layoutScript);
			$this->getActionController()->getHelper('layout')
					->setLayout($layoutName);
		}
	}
}

