<?php

class Clinictrial_ManageinvestigatorController extends Zend_Controller_Action
{
    public function init()
    {
       	$this->view->controllerName = $this->getRequest()->getParam('controller');
    	$this->view->action = $this->getRequest()->getParam('action');
    	
    	$flash = $this->_helper->getHelper('FlashMessenger');
    	$message = $flash->getMessages();
    	$this->view->messageSuccess = isset($message[0]['success']) ? $message[0]['success'] : '';
    	$this->view->messageError = isset($message[0]['error']) ? $message[0]['error'] : '';
    }
    /**
     * preDispatch
     *
     * check the authentication before page load
     *
     * @author Raman
     * @version 1.0
     */
    public function preDispatch() {
    
    	if (!Auth_ClinictrialAdapter::hasIdentity()) {
    		$this->_helper->redirector('index','index','login');
    	}
    }
    
    public function indexAction()
    {   $aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$this->view->headTitle("Manage Investigator");
    	$this->view->Message = 'Are you sure you want to delete this investigator?';
    	$this->view->title = 'Delete Investigator';
    	
    	$flag = 0;
    	$list = investigator::getInvestigatorsList($flag, $aes_Key);
    	$page=$this->_getParam('page',1);
    	$paginator = Zend_Paginator::factory($list);
    	$paginator->setItemCountPerPage(10);
    	$paginator->setCurrentPageNumber($page);
    	 
    	$this->view->paginator=$paginator;
    	$this->view->aes_Key=$aes_Key;
    }
    /**
     * Investigator
     *
     * add Investigator in database
     *
     * @author Raman
     * @version 1.0
     */
    public function addInvestigatorAction(){
    	$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$this->view->headTitle("Add Investigator");
    	#get stuidies from database
    	$data=  studyrecord::getAllStudies($aes_Key);
    	$this->view->studies_list = $data;
    	$createdBy = Auth_ClinictrialAdapter::getIdentity ();
    	$uId = $createdBy[0]['u__id'];
    	$this->view->aes_Key=$aes_Key;
    	
    	if ($this->getRequest ()->isPost ()) {
    		$email = array();
    		$params = $this->getRequest()->getParams();    		
    		$email[] = $params['investigator_email'];    		
    		#save data in database
    		$investigator = new investigator();
    		$data = $investigator->saveInvestigatorRecord($params, $uId, $aes_Key);
    		#set flash message
    		$flash = $this->_helper->getHelper('FlashMessenger');
    		if($data!=null){
    			$id = base64_encode($data);
    			$url = MODULE_LOGIN .'/index/create-investigator-account/id/' . $id;
    			$subject  = $this->view->translate('Clinitrial - Account Registration');   			
    		    //$message = "Please click on this <a href='$url'  >Click here to set your username and password</a> link to set the password";
    		  
    			echo $message = "Hi ".ucfirst($params['investigator_firstname']).",<br/><br/>".$this->view->translate('Please follow the instructions below to activate your account')."</a><br/><br/>".$this->view->translate('Thank You').",<br/>".$this->view->translate('Support Team')."<br/><br/>".$this->view->translate('Account activation link  ')."<a href='$url'  >".$this->view->translate('Click here to set your username and password')."</a> .";
    		die;	BackEnd_Helper_viewHelper::SendMail($email, $subject, $message, 'support@cliniops.com');

    			$message = 'Investigator has been added successfully to the application. An email notification has been sent to Investigator to set username and password.';
    			$flash->addMessage(array('success' => $message ));
    			$this->_helper->redirector('index','manageinvestigator','clinictrial');
    			 
    		}else{
    			 
    			$message = 'Problem in your data please enter your valid data.';
    			$flash->addMessage(array('error' => $message ));		 
    		}
    	}
    }
    /**
     * viewInvestigator
     *
     * view Investigator in detail
     *
     * @author Raman
     * @version 1.0
     */
    public function viewInvestigatorAction(){
    	$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$this->view->headTitle("View Investigator");
    	$id = $this->getRequest()->getParam('id');
    	 
    	$flash = $this->_helper->getHelper('FlashMessenger');
    	if($id){
    		 
    		$investigator = investigator::getInvestigatorRecordById($id,'view');
    		if($investigator == false)
    		{
    			$this->_helper->redirector('index','error');
    		}
    		$this->view->investigator = $investigator;
    		$this->view->aes_Key=$aes_Key;
    		//print_r($investigator);die;
    		 
    	}else{
    		 
    		$message = 'Error in your link.';
    		$flash->addMessage(array('success' => $message ));
    		$this->_helper->redirector('index','manageinvestigator','clinictrial');
    	}    
    }
    
    /**
     * editInvestigator
     *
     * edit Investigator from database by id
     *
     * @author Raman
     * @version 1.0
     */
    public function editInvestigatorAction(){
    	$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$id = $this->getRequest()->getParam('id');
    	$this->view->headTitle("Edit Investigator");
    	#get stuidie from database
    	$data=  studyrecord::getAllStudies($aes_Key);
    	$this->view->studies_list = $data;
    	$createdBy = Auth_ClinictrialAdapter::getIdentity ();
    	$uId = $createdBy[0]['u__id'];
    	
    	
    	$flash = $this->_helper->getHelper('FlashMessenger');
    	if($id){
    
    		$investigator = investigator::getInvestigatorRecordById($id,'edit');
    		
    		if($investigator == false)
    		{
    			$this->_helper->redirector('index','error');
    		}
    	 	if(count($investigator) > 0){
    	 		$this->view->investigator = $investigator;
    	 		$this->view->aes_Key=$aes_Key;
    	 	}else{
    	 		$message = 'Error in your link.';
    	 		$flash->addMessage(array('error' => $message ));
    	 		$this->_helper->redirector('index','manageinvestigator','clinictrial');
    	 	}
    		
    		
    	}else{
    
    		$message = 'Error in your link.';
    		$flash->addMessage(array('success' => $message ));
    		$this->_helper->redirector('index','manageinvestigator','clinictrial');
    	}
    	 
    	if ($this->getRequest ()->isPost ()) {
    		 
    		$params = $this->getRequest()->getParams();
    		#save data in dabase
    		$investigator = new investigator();
    		$data = $investigator->saveInvestigatorRecord($params, $uId, $aes_Key);
    		#set flash message
    		if($data!=null){
    
    			$message = 'Investigator record has been updated successfully.';
    			$flash->addMessage(array('success' => $message ));
    			$this->_helper->redirector('index','manageinvestigator','clinictrial');
    
    		}else{
    
    			$message = 'Problem in your data please enter your valid data.';
    			$flash->addMessage(array('error' => $message ));
    
    		}
    		 
    	}
    }
    

    /**
     * deleteInvestigator
     *
     * delete Investigator record from database
     *
     * @author Raman
     * @version 1.0
     */
    public function deleteInvestigatorAction()
    {   $aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$params = $this->_getAllParams();
    	$data = investigator::deleteInvestigatorRecord($params, $aes_Key);
    	$flash = $this->_helper->getHelper('FlashMessenger');
    	$message = 'Investigator record has been deleted successfully.';
    	$flash->addMessage(array('success' => $message ));
    	echo $data;
    	die ();
    }
    
    /**
     * checkduplicateemailAction
     *
     * Check dupliocate email from the application
     *
     * @author Raman
     * @version 1.0
     */
    public function checkduplicateemailAction(){
    		
    	$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$this->_helper->layout ()->disableLayout ();
    	$this->_helper->viewRenderer->setNoRender ( true );
    	$params = $this->_getAllParams();
    
    	$invEmail = $params['investigator_email'];
    	$email = BackEnd_Helper_viewHelper::encryptString($invEmail, $aes_Key);
    	$data = user::checkInvestigatorEmail($email);
    	echo json_encode($data);
    }

}

