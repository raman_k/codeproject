<?php

class Clinictrial_ManagestudyController extends Zend_Controller_Action
{

    public function init()
    {
        $this->view->controllerName = $this->getRequest()->getParam('controller');
    	$this->view->action = $this->getRequest()->getParam('action');
    	
    	$flash = $this->_helper->getHelper('FlashMessenger');
    	$message = $flash->getMessages();
    	$this->view->messageSuccess = isset($message[0]['success']) ? $message[0]['success'] : '';
    	$this->view->messageError = isset($message[0]['error']) ? $message[0]['error'] : '';
    }
    /**
     * preDispatch
     *
     * check the authentication before page load
     *
     * @author Raman
     * @version 1.0
     */
    public function preDispatch() {
    
    	if (!Auth_ClinictrialAdapter::hasIdentity()) {
    		$this->_helper->redirector('index','index','login');
    	}
    
    
    }
	public function indexAction()
    {
    	$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$this->view->headTitle("Manage Study");
    	$this->view->Message = 'Are you sure you want to delete this study?';
    	$this->view->title = 'Delete Study';
    	
    	$flag = 0;
    	$list = studyrecord::getStudiesList($flag);
    	
    	$page=$this->_getParam('page',1);
    	$paginator = Zend_Paginator::factory($list);
    	$paginator->setItemCountPerPage(10);
    	$paginator->setCurrentPageNumber($page);
    	 
    	$this->view->paginator=$paginator;
    	$this->view->aes_Key=$aes_Key;
    }
    /**
     * addStudy
     *
     * add study in database
     *
     * @author Raman
     * @version 1.0
     */
    public function addStudyAction(){
    	$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$this->view->headTitle("Add Study");
    	#get companies from database
    	$data=  ctcompany::getAllCompanyForDrp(0, $aes_Key);
    	$this->view->company_list = $data;
    	$this->view->aes_Key=$aes_Key;
    	$createdBy = Auth_ClinictrialAdapter::getIdentity ();
    	$uId = $createdBy[0]['u__id'];
    	
    	if ($this->getRequest ()->isPost ()) {
    		
    		$params = $this->getRequest()->getParams();
    		#save data in dabase
    		$studyrecord = new studyrecord();
    		$data = $studyrecord->saveStudyRecord($params, $uId, $aes_Key );
    		#set flash message 
    		$flash = $this->_helper->getHelper('FlashMessenger');
    		if($data!=null){
    			
    			$message = 'Study record has been added successfully.';
    			$flash->addMessage(array('success' => $message ));
    			$this->_helper->redirector('index','managestudy','clinictrial');
    			
    		}else{
    			
    			$message = 'Problem in your data please enter your valid data.';
    			$flash->addMessage(array('error' => $message ));
    			
    		}
    		
    	}
    }
    /**
     * viewStudy
     *
     * view study in database
     *
     * @author Raman
     * @version 1.0
     */
    public function viewStudyAction(){
    	$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$this->view->headTitle("View Study");
    	$id = $this->getRequest()->getParam('id');
    	
    	$flash = $this->_helper->getHelper('FlashMessenger');
    	if($id){
    		$studyrecord = studyrecord::getStudyRecordById($id,'view');
    		if($studyrecord == false)
    		{
    			$this->_helper->redirector('index','error');
    		}
    		$this->view->studyrecod = $studyrecord;
    		$this->view->aes_Key=$aes_Key;
    		//print_r($studyrecord);die;
    	
    	}else{
    	
    		$message = 'Error in your link.';
    		$flash->addMessage(array('success' => $message ));
    		$this->_helper->redirector('index','managestudy','clinictrial');
    	}
    	 
    }
    /**
     * editStudy
     *
     * view study in database
     *
     * @author Raman
     * @version 1.0
     */
    public function editStudyAction(){
    	$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$this->view->headTitle("Edit Study");
    	#get companies from database
    	$data=  ctcompany::getAllCompanyForDrp(0, $aes_Key);
    	$this->view->company_list = $data;
    	$id = $this->getRequest()->getParam('id');
    	$this->view->aes_Key=$aes_Key;
    	$flash = $this->_helper->getHelper('FlashMessenger');
    	$createdBy = Auth_ClinictrialAdapter::getIdentity ();
    	$uId = $createdBy[0]['u__id'];
    	if($id){
    		
    		$studyrecord = studyrecord::getStudyRecordById($id,'edit');
    		if($studyrecord == false)
    		{
    			$this->_helper->redirector('index','error');
    		}
    		if(count($studyrecord) > 0){
    			$this->view->studyrecod = $studyrecord;
    		}else{
    			$message = 'Error in your link.';
    			$flash->addMessage(array('error' => $message ));
    			$this->_helper->redirector('index','managestudy','clinictrial');
    		}
    		
    	}else{
    		
    		$message = 'Error in your link.';
    		$flash->addMessage(array('success' => $message ));
    		$this->_helper->redirector('index','managestudy','clinictrial');
    	}
    	
    	if ($this->getRequest ()->isPost ()) {
    	
    		$params = $this->getRequest()->getParams();
    		#save data in dabase
    		$studyrecord = new studyrecord();
    		$data = $studyrecord->saveStudyRecord($params, $uId, $aes_Key);
    		#set flash message
    		if($data!=null){
    			 
    			$message = 'Study record has been updated successfully.';
    			$flash->addMessage(array('success' => $message ));
    			$this->_helper->redirector('index','managestudy','clinictrial');
    			 
    		}else{
    			 
    			$message = 'Problem in your data please enter your valid data.';
    			$flash->addMessage(array('error' => $message ));
    			 
    		}
    	
    	}
    }
    
    /**
     * getstudies
     * 
     * get all studies record from database
     * 
     * @author Raman	
     * @version 1.0
     */
    public function getStudiesAction()
    {
       $param = $this->getRequest()->getParams();
       #get list of the studies from databse
       $a = studyrecord::getStudiesList($param);
       //echo Zend_Json::encode($data);
       die;
    }
    /**
     * deleteStudy
     *
     * delete company record from database
     *
     * @author Raman
     * @version 1.0
     */
    public function deleteStudyAction()
    {
    	$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$params = $this->_getAllParams();
    	$deleteStudy = studyrecord::deleteStudyRecord($params, $aes_Key);
    	$flash = $this->_helper->getHelper('FlashMessenger');
    	$message = 'Study record has been deleted successfully.';
    	$flash->addMessage(array('success' => $message ));
    	echo $deleteStudy;
    	die ();
    }
    
    public function checkStudyIdAction() {
    	$this->_helper->layout ()->disableLayout ();
    	$this->_helper->viewRenderer->setNoRender ( true );
    	$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$params = $this->_getAllParams ();
    	$studyid = studyrecord::getStudyId ($params, $aes_Key);
    	if(isset($studyid[0]['id'])){
    		echo json_encode(false);
    	}else {
    		echo json_encode(true);
    	}
    	die;
    }
}

