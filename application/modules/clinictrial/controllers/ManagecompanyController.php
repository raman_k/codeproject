<?php

class Clinictrial_ManagecompanyController extends Zend_Controller_Action {
	
	public function init() {
		
		$this->view->controllerName = $this->getRequest ()->getParam ( 'controller' );
		$this->view->action = $this->getRequest ()->getParam ( 'action' );
		
		$flash = $this->_helper->getHelper ( 'FlashMessenger' );
		$message = $flash->getMessages ();
		$this->view->messageSuccess = isset ( $message [0] ['success'] ) ? $message [0] ['success'] : '';
		$this->view->messageError = isset ( $message [0] ['error'] ) ? $message [0] ['error'] : '';
	}
	
	/**
	 * preDispatch
	 *
	 * check the authentication before page load
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function preDispatch() {
		
		if (! Auth_ClinictrialAdapter::hasIdentity ()) {
			$this->_helper->redirector ( 'index', 'index', 'login' );
		}
	
	}
	
	public function indexAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$this->view->headTitle ( "Manage Company" );
		$this->view->Message = 'Are you sure you want to delete this company?';
		$this->view->title = 'Delete Company';
		
		$flag=0;
		$companyList = ctcompany::getCompanyList($flag);
		$page=$this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($companyList);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($page);
		
		$this->view->paginator=$paginator;
		$this->view->aes_Key=$aes_Key;
	}
	/**
	 * addCompany
	 *
	 * add company record to database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function addCompanyAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$this->view->headTitle ( "Add Company" );
		$countriesList = country::getAllCountries ();
		$this->view->countries_list = $countriesList;
		$createdBy = Auth_ClinictrialAdapter::getIdentity ();
		$uId = $createdBy[0]['u__id'];
		$this->view->aes_Key=$aes_Key;
		
		/* $translate = new Zend_Translate(
				array(
						'adapter' => 'gettext',
						'content' => ROOT_PATH.'languages/clinitrial_hi.mo',
						'locale'  => 'en_US'
				)
		);
		print $translate->_("Audit Record") . "\n"; die; */
		//print_r($translate); die;
		if ($this->getRequest ()->isPost ()) {
			
			$params = $this->getRequest ()->getParams ();
			
			//state and city for company
			$compState = state::getStatesById ( $params['comp_state'] );
			$compCity = city::getCitiesById ( $params['comp_city'] );
			
			//state and city for company owner
			$compOwnerState = state::getStatesById ( $params['comp_owner_state'] );
			$compOwnerCity = city::getCitiesById ( $params['comp_owner_city'] ); 
			
			$form = new Zend_Session_Namespace ('companyAddForm');
			$form->formData = $params;
			$form->CompanyCityAndState = array("companystate"=>$compState, "companycity"=>$compCity);
			$form->OwnerCityAndState = array("ownerstate"=>$compOwnerState, "ownercity"=>$compOwnerCity); 
			
			// save data in database
			$companyObj = new ctcompany ();
			$data = $companyObj->saveCompanyRecord ( $params, $uId, $aes_Key );
			// et flash message
			$flash = $this->_helper->getHelper ( 'FlashMessenger' );
			if ($data != null) {
				
				//unset form data after saving it
				Zend_Session::namespaceUnset('companyAddForm');
				Zend_Session::namespaceUnset('CompanyCityAndState');
				Zend_Session::namespaceUnset('OwnerCityAndState');
				
				$message = 'Company record has been added successfully.';
				$flash->addMessage ( array ('success' => $message ) );
				$this->_helper->redirector ( 'index', 'managecompany', 'clinictrial' );
			
			} else {				
				$message = 'Upload only .jpg or .png file, not exceeding 2MB';
				$flash->addMessage ( array ('error' => $message ) );
				$this->_helper->redirector ( 'add-company', 'managecompany', 'clinictrial' );
			
			}
		
		}
	}
	
	/**
	 * viewCompany
	 *
	 * get company record from database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function viewCompanyAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$this->view->headTitle ( "View Company" );
		$params = $this->_getAllParams ();
		$viewCompany = ctcompany::viewCompanyRecord ( $params );
		if($viewCompany == false)
		{
			$this->_helper->redirector('index','error');
		}
		$this->view->companyDetail = $viewCompany;
		$this->view->aes_Key=$aes_Key;
	
	}
	
	/**
	 * editCompany
	 *
	 * get company record from database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function editCompanyAction() {
		$this->view->headTitle ( "Edit Company" );
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$createdBy = Auth_ClinictrialAdapter::getIdentity ();
		$uId = $createdBy[0]['u__id'];
		// et companies from database
		$countriesList = country::getAllCountries ();
		$this->view->countries_list = $countriesList;
		$id = $this->getRequest ()->getParam ( 'id' );
		$flash = $this->_helper->getHelper ( 'FlashMessenger' );
		$this->view->aes_Key=$aes_Key;
		if ($id) {
			
			$companyRecord = ctcompany::getCompanyRecordById ( $id );
			if($companyRecord == false)
			{
				$this->_helper->redirector('index','error');
			}
			$this->view->companyRecord = $companyRecord;
			
			if(count($companyRecord) > 0){
				$this->view->studyrecod = $companyRecord;
			}else{
				$message = 'Error in your link.';
				$flash->addMessage(array('error' => $message ));
				$this->_helper->redirector('index','managecompany','clinictrial');
			}
			
		} else {
			
			$message = 'Error in your link.';
			$flash->addMessage ( array ('success' => $message ) );
			$this->_helper->redirector ( 'index', 'managecompany', 'clinictrial' );
		}
		
		if ($this->getRequest ()->isPost ()) {
			
			$params = $this->getRequest ()->getParams ();
			$checkStatus = ctcompany::checkStatus ($id);
			
			if(isset($checkStatus[0]['projectStatus']) && preg_replace('/[^(\x20-\x7f)]*/s', '', BackEnd_Helper_viewHelper::decryptString($checkStatus[0]['projectStatus'], $aes_Key)) !=1){
				if($params['comp_act_status'] == 1){
					$status = 0;
					$companyRecord = studyrecord::getStudiesByCompany ( $id, $status, $aes_Key );
				}
			}else {
				if($params['comp_act_status'] != 1){
// 					$status = $params['comp_act_status'];
// 					$companyRecord = studyrecord::getStudiesByCompany ( $id,$status );
				}
			}
			// save data in database
			$companyRecord = new ctcompany ();
			$data = $companyRecord->saveCompanyRecord ( $params, $uId, $aes_Key);
			// set flash message
			if ($data != null) {
				
				$message = 'Company record has been updated successfully.';
				$flash->addMessage ( array ('success' => $message ) );
				$this->_helper->redirector ( 'index', 'managecompany', 'clinictrial' );
			
			} else {
				
				$message = 'Upload only .jpg or .png file, not exceeding 2MB';
				$flash->addMessage ( array ('error' => $message ) );
				$this->_helper->redirector ( 'edit-company', 'managecompany', 'clinictrial', 
                                       array('id' => $id) );
			
			}
		
		}
	}
	
	/**
	 * deleteCompany
	 *
	 * delete company record from database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function deleteCompanyAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$params = $this->_getAllParams ();
		$deleteCompany = ctcompany::deleteCompanyRecord ( $params, $aes_Key );
		$flash = $this->_helper->getHelper ( 'FlashMessenger' );
		$message = 'Company record has been deleted successfully.';
		$flash->addMessage ( array ('success' => $message ) );
		echo $deleteCompany;
		die ();
	}
	
	/**
	 * getStates
	 *
	 * get states for specific country
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function getStatesAction() {
		$params = $this->_getAllParams ();
		@$selectedState = $params ['selectedState'];
		$stateList = state::getStates ( $params );
		$stateOptions = array ();
		foreach ( $stateList as $data ) {
			$sId = $data ['state_id'];
			$sName = $data ['state_name'];
			if (isset ( $selectedState ) && $selectedState != "") {
				$val = $sId == $selectedState ? 'selected' : '';
				$stateOptions = "<option $val value='$sId'>$sName</option>";
			} else {
				$stateOptions = "<option value='$sId'>$sName</option>";
			}
			echo $stateOptions;
		}
		die ();
	}
	
	/**
	 * getCities
	 *
	 * get cities for specific state
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function getCitiesAction() {
		$params = $this->_getAllParams ();
		@$selectedCity = $params ['selectedCity'];
		$cityList = city::getCities ( $params );
		$cityOptions = array ();
		foreach ( $cityList as $data ) {
			$cId = $data['city_id'];
			$cName = $data['city_name'];
			if (isset ( $selectedCity ) && $selectedCity != "") {
				$val = $cId == $selectedCity ? 'selected' : '';
				$cityOptions = "<option $val value='$cId'>$cName</option>";
			} else {
				$cityOptions = "<option value='$cId'>$cName</option>";
			}
			echo $cityOptions;
		}
		die ();
	}
	
	/**
	 * checkProjectNo
	 *
	 * check if project no already exist
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function checkProjectNoAction() {
		$this->_helper->layout ()->disableLayout ();
		$this->_helper->viewRenderer->setNoRender ( true );
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$params = $this->_getAllParams ();
		$projectNo = ctcompany::getProjectNo ($params, $aes_Key);		
		if(!empty($projectNo)){
			echo json_encode(false);
		}else {
			echo json_encode(true);
		}
		die;
	}
}
