<?php
class Investigator_AnalyzestudyController extends Zend_Controller_Action {

	public function init() {
	
		$this->view->controllerName = $this->getRequest ()->getParam ( 'controller' );
		$this->view->action = $this->getRequest ()->getParam ( 'action' );
	
		$flash = $this->_helper->getHelper ( 'FlashMessenger' );
		$message = $flash->getMessages ();
		$this->view->messageSuccess = isset ( $message [0] ['success'] ) ? $message [0] ['success'] : '';
		$this->view->messageError = isset ( $message [0] ['error'] ) ? $message [0] ['error'] : '';
	}
	
	/**
	 * preDispatch
	 *
	 * check the authentication before page load
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function preDispatch() {
	
		if (!Auth_InvestigatorAdapter::hasIdentity()) {
			$this->_helper->redirector('index','index','login');
		}
	}

	/**
	 * index
	 * 
	 * Display analyze study form
	 * @author Raman
	 * @version 1.0
	*/
	public function indexAction(){

		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		if($configId == 0){
			$this->view->data = NULL;
		}else {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$analysestudy = $this->view->translate('Analyze Study');
		$this->view->headTitle($analysestudy);
		
		$flag = 0;
		$sites = sites::getSitesList($flag,$configId);
		$groups = groups::getGroupsList($flag, $configId);
		$visits = visits::getVisitsList($flag, $configId);
		$sub_status = studystatus::getStudyStatus($flag, $configId);
		$fieldStatus = studystatus::getFieldStatus();
				
		$this->view->sites = $sites;
		$this->view->groups = $groups;
		$this->view->visits = $visits;
		$this->view->sub_status = $sub_status;
		$this->view->fieldStatus = $fieldStatus;
		$this->view->aes_Key = $aes_Key;
		
		// get zend session variables
		$studyInfo = new Zend_Session_Namespace ('InvestigatorInfo');
		$study_id = $studyInfo->investigator_studyId;
		$investigatorData = $studyInfo->investigator_data;
		
		$role = $investigatorData[0]['u__role_id'];
		$investigator_id = $investigatorData[0]['u__id'];
		
		#check secondary investigator permission and show view accordingly
		$chkpermission = BackEnd_Helper_viewHelper::checkSecInvestigatorPermission();
		$permission_id = $chkpermission['pId'];
		$this->view->perName = $chkpermission['pname'];
		$this->view->permissionId = $permission_id;
		$this->view->roleId = $role;
		
		$flash = $this->_helper->getHelper ( 'FlashMessenger' );
		$message = $flash->getMessages ();
		$this->view->checkData = isset ( $message [0] ['checkData'] ) ? $message [0] ['checkData'] : '';
		}
	}
	
	/**
	 *  getVisitForm
	 *
	 * get form assigned to particuler visit
	 * @author Raman
	 * @version 1.0
	 */
	/*public function getVisitFormAction(){
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$params = $this->_getAllParams();
		if(isset($params['id']) && $params['id'] == '0') {
			$opiton = '';
			$flag = 0;
			$formList= forms::getAllFormName($configId, $flag);
			$opiton .= "<option value='0'>All Forms</option>";
			foreach($formList as $data){
			$id = $data['id'];
			$name = BackEnd_Helper_viewHelper::decryptString($data['name'], $aes_Key) ;			
			$opiton .= "<option value='$id'>$name</option>";			
			}
			print_r($opiton); die;
		}else {
			$opiton = '';
		    $formDetails = forms::getFormName($params);
		    foreach($formDetails as $data){
			    $id = $data['fid'];
			    $name = BackEnd_Helper_viewHelper::decryptString($data['fname'], $aes_Key) ;
			    $opiton .= "<option value='$id'>$name</option>"; 
		    }
		    print_r($opiton); die;
		}
	}*/
	
	public function getVisitFormAction(){
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$params = $this->_getAllParams();
		$option = '';
		if(isset($params['id']) && $params['id'] == '0') {
				
			$flag = 0;
			$formList = forms::getAllFormName($configId, $flag);
			$option .= "<option value='' disabled='disabled'>Select Forms</option><option value='0'>All Forms</option>";
			foreach($formList as $fList){
				$id = $fList['id'];
				$name = BackEnd_Helper_viewHelper::decryptString($fList['name'], $aes_Key);
				$option .= "<option value='$id'>$name</option>";
			}
			print_r($option); die;
		}else { 
			$formDetails = forms::getFormName($params);
			if(!empty($formDetails))
			$option .= "<option value='' disabled='disabled'>Select Forms</option><option value='0'>All Forms</option>";
			foreach($formDetails as $key=>$fval){
				$id = $fval['fid'];
				$name = BackEnd_Helper_viewHelper::decryptString($fval['fname'], $aes_Key) ;
				$option .= "<option value='$id'>$name</option>";
			}
			print_r($option); die;
		}
	
		}
	/**
	 * exportData
	 *
	 * Export data in an excel form
	 * @author Raman
	 * @version 1.0
	 */
public function exportDataAction(){
		$this->_helper->layout()->disableLayout();
		Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		$params = $this->_getAllParams();

		$data = user::excelExportData($params, $configId, $aes_Key);
		
	    if(empty($data)){ 
	    	$flash = $this->_helper->getHelper('FlashMessenger');
			$message = $this->view->translate('No Record Found');
			$flash->addMessage(array('checkData' => $message ));								
			$this->_helper->redirector ( 'index', 'analyzestudy', 'investigator');	    	
	    }else {
		set_time_limit ( 10000 );
		ini_set('max_execution_time',115200);
		ini_set("memory_limit","1024M");

		$cols = 8;
		$row = 1;
		$formVariable = array();
		$variableArray = array();
	/*	foreach($data as $result1){

			$formVariable[] = forms::getFormVariables($result1['fm_id']);
		}
		
		foreach($formVariable as $result2){
			foreach($result2 as $varData){
				$variableArray[]=$varData['id'];
			}
		}
		
		foreach($variableArray as $key=>$var){
			$formVariableFields[$key] = forms::getFormVariableData($var, $aes_Key);
		}
*/
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A1',
				$this->view->translate('Sr. No'));
		$objPHPExcel->getActiveSheet()->setCellValue('B1',
				$this->view->translate('Subject ID'));
		$objPHPExcel->getActiveSheet()->setCellValue('C1',
		        $this->view->translate('Subject Status'));
		$objPHPExcel->getActiveSheet()->setCellValue('D1',
				$this->view->translate('Field Status'));
		$objPHPExcel->getActiveSheet()->setCellValue('E1',
				$this->view->translate('Site'));
		$objPHPExcel->getActiveSheet()->setCellValue('F1',
				$this->view->translate('Group'));
		$objPHPExcel->getActiveSheet()->setCellValue('G1',
				$this->view->translate('Visit'));
		$objPHPExcel->getActiveSheet()->setCellValue('H1',
				$this->view->translate('Form'));
	/*	foreach(@$formVariableFields as $key=>$fields){
			$variablelabel = BackEnd_Helper_viewHelper::decryptString($fields['variablelabel'], $aes_Key);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cols, $row, $this->view->translate($variablelabel));
			$cols++;
		}
	*/	
		
		$column = 2;
		$row = 2;
		$i = 1;
		
		foreach($data as $result){

			$groupData = groups::getUpdateGroupData($result['groupId'], $aes_Key);
			$groupname = $groupData['name'];
			
			$visitData = visits::getVisitData($result['visitID'], $aes_Key);
			$visitname = $visitData['name'];
			
			$siteData = sites::getUpdateSiteData($result['siteId'], $aes_Key);
			$sitename = $siteData['name'];
			
			$formData = forms::getFormData($result['formID'], $aes_Key);
			$formname = $formData['name'];
			
			$subStatusData = BackEnd_Helper_viewHelper::decryptString($result['subjectStatus'], $aes_Key);
			$subId = BackEnd_Helper_viewHelper::decryptString($result['subject_id'], $aes_Key);
			
			if($result['fieldStatusID']==0){
				$fieldstatus = "Missing";
			}else{
				$fieldstatus = "Entered";
			}
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$column,
					$i);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$column,
					$subId);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$column,
					$subStatusData);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$column,
					$fieldstatus);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$column,
					$sitename);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$column,
					$groupname);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$column,
					$visitname);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$column,
					$formname);
			
			$cols = 8;
	/*		foreach($formVariableFields as $key=>$fieldValue){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cols++, $row, $fieldValue['fvvalue']);
			}
	*/		
			$column++;
			$row++;
			$i++;
		}
		
		//FORMATING OF THE EXCELL
		$headerStyle = array(
				'fill' => array(
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb'=>'fff'),
				),
				'font' => array(
						'bold' => true,
				)
		);
		$borderStyle = array('borders' =>
				array('outline' =>
						array('style' => PHPExcel_Style_Border::BORDER_MEDIUM,
								'color' => array('argb' => '000000'),   ),),);
		//HEADER COLOR
		
		
		$objPHPExcel->getActiveSheet()->getStyle('A1:'.'V1')->applyFromArray($headerStyle);
		
		//SET ALIGN OF TEXT
		
		$objPHPExcel->getActiveSheet()->getStyle('A1:V1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$objPHPExcel->getActiveSheet()->getStyle('B2:V'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		
		//BORDER TO CELL
		
		$objPHPExcel->getActiveSheet()->getStyle('A1:'.'V1')->applyFromArray($borderStyle);
		$borderColumn =  (intval($column) -1 );
		
		$objPHPExcel->getActiveSheet()->getStyle('A1:'.'V'.$borderColumn)->applyFromArray($borderStyle);
		
		//SET SIZE OF THE CELL

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);

	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);


	// redirect output to client browser

	$fileName =  $this->view->translate('export.xlsx');
	header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename=' . $fileName);
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	die();
	    }
	}
}
}