<?php
class Investigator_ViewsubjectsController extends Zend_Controller_Action {

	public function init() {

		$this->view->controllerName = $this->getRequest ()->getParam ( 'controller' );
		$this->view->action = $this->getRequest ()->getParam ( 'action' );

		$flash = $this->_helper->getHelper ( 'FlashMessenger' );
		$message = $flash->getMessages ();
		$this->view->messageSuccess = isset ( $message [0] ['success'] ) ? $message [0] ['success'] : '';
		$this->view->messageError = isset ( $message [0] ['error'] ) ? $message [0] ['error'] : '';
	}

	
	/**
	 * preDispatch
	 *
	 * check the authentication before page load
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function preDispatch() {
	
		if (!Auth_InvestigatorAdapter::hasIdentity()) {
			$this->_helper->redirector('index','index','login');
		}
	}
	
	
	/**
	 * View subjects 
	 * 
	 * indexAction
	 * @author Raman
	 * @version 1.0
	 */
	public function indexAction(){
		$sub = $this->view->translate('View Subjects');
		$this->view->headTitle($sub);
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		//echo $configId; die;
		if($configId == 0){
			$this->view->sites= NULL;
		}else {
				
			// get zend session variables
			$studyInfo = new Zend_Session_Namespace ('InvestigatorInfo');
			$study_id = $studyInfo->investigator_studyId;
			$investigatorData = $studyInfo->investigator_data;
			$investigator_id = $investigatorData[0]['u__id'];
			
			$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
			$listdata = user::getSubjects($configId);			
			$list = BackEnd_Helper_viewHelper::decryptWsArray($listdata, $aes_Key);			
			$page=$this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($list);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			if(!empty($list)){
				$subjectExist = true;
			} else {
				$subjectExist = false;
			}
			$this->view->subjects=$paginator;
			$this->view->aes_Key=$aes_Key;
			$this->view->page=$page;
			$this->view->subjectExist=$subjectExist;
			$this->view->firstsubject = @$list[0]['sub_id'];
		
		}
	}
	
	/**
	 * get details of the subject
	 *
	 * getsubjectdetailsAction
	 * @author Raman
	 * @version 1.0
	 */
	public function getsubjectdetailsAction(){
	
		require('signature/signature.class.php');
		
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$params = $this->getRequest()->getParams();
		$subjectId = $params['id'];
		
		$templateDir = UPLOAD_PATH."/backend/stdocument/";
		
		$subData = user::getSubjectsdata($subjectId);
		$data = BackEnd_Helper_viewHelper::decryptWsArray($subData, $aes_Key);
		//echo "<pre>";print_r($data);die;
		
		$template = $data[0]['st_conf_filename'];
		
		$template_pdf = $data[0]['st_conf_filecontent_id'];
		$sub_sign = $data[0]['sub_sig_id'];
		$inv_sign = $data[0]['inv_sig_id'];
		$fileToOutPut = "";
		if($template_pdf != "" && $sub_sign != "" && $inv_sign != ""){
			
			$s = new signature();
			
			//set subject and investigator sign
			$s->make_signature($sub_sign, $inv_sign);
			
			$toOutPut = $s->make_name();
			$fileToOutPut = 'output'.$toOutPut;
			$s->use_template_pdf($fileToOutPut, $template_pdf);
			
			$stdocument = MODULE_INVESTIGATOR."/viewsubjects/download-consent-form/file/".$fileToOutPut.".pdf";
			
			$s->delete_signature();
			
		}
		
		if(file_exists($templateDir.$fileToOutPut.".pdf")){
			
			$sign_doc = "<a href='".$stdocument."' target='_blank'>".$this->view->translate('Click Here')."</a>";
		
		} else {
			
			$sign_doc = $this->view->translate('Not Available');
		} 
		
		$subVisitData = visits::getVisistsOfSubject($subjectId);
		$visitData = BackEnd_Helper_viewHelper::decryptWsArray($subVisitData, $aes_Key);
		$visitDetails = "";
		$i = 0;
		foreach($visitData as $visits){
			$visitDetails = $visitDetails."<tr>
		   					<td>".++$i."</td>
						    <td>".$visits['visitname']."</td>
						    <td>".$visits['c_datecreated']."</td>
						    <td><div class='actions'> <a href='javascript:;' onclick='openPopup(".$subjectId.",".$visits['cdata_form_id'].")'>".$visits['form_name']."</a> </div></td>
		    			</tr>";
		}
		
		
		$visitDetailsMain = "<table cellspacing='0' cellpadding='0'  class='view-sub-data'>
		    		<tbody>
		    			<tr>
		    				<th>".$this->view->translate('Count')." </th>
		    				<th>".$this->view->translate('Study Visits')."</th>
		    				<th>".$this->view->translate('Visit  Date')." </th>
		    				<th>".$this->view->translate('Form')." </th>
		    			</tr>
		    			".$visitDetails."
		    		</tbody>
		    	</table>";
		
		if(!empty($data)){
		    $htmlPart = "<h3>".$this->view->translate('Subject Calendar')."</h3>
		    <div class='subject-cal-detail'>
		    	<div class='subject-cal-left'>
				    <p><strong>".$this->view->translate('Subject ID').":</strong>".$data[0]['subjectid']."</p>
				    <p><strong>".$this->view->translate('Registration Date').": </strong>".$data[0]['u_datecreated']."</p>
				    <p><strong>".$this->view->translate('Group Name').": </strong>".$data[0]['gp_name']."</p>
		    	</div>
		    	<div class='subject-cal-right'>
				    <p><strong>".$this->view->translate('Registered by').":</strong>".$data[0]['cby_firstname']." ".$data[0]['cby_lastname']."</p>
				    <p><strong>".$this->view->translate('Signed ICF').": </strong>".$sign_doc."</p>
				    <p><strong>".$this->view->translate('Site Name').": </strong>".$data[0]['st_name']."</p>
		    	</div>
		    	<div class='clr'></div>
		    </div>
		    <div class='table-data investigator-data'>
		    	".$visitDetailsMain."
		    </div>";
		}else {
			$htmlPart = null;
		}
		echo json_encode($htmlPart); die;
	}
	
	/**
	 * get details of the subject
	 *
	 * getformdetailsAction
	 * @author Raman
	 * @version 1.0
	 */
	public function getformdetailsAction(){
	
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$params = $this->getRequest()->getParams();
		$subjectId = $params['id'];
		$formId = $params['fid'];
	
		$subVisitData = visits::getVisistDetailOfSubject($subjectId, $formId);
		$formData = BackEnd_Helper_viewHelper::decryptWsArray($subVisitData, $aes_Key);
		
		//print_r($formData); die;
		if(!empty($formData)){
			
			$heading = @$formData[0]['form_name'];
			$details = "";
			foreach($formData as $data){
				if($data['datatype'] =='ordinal' || $data['datatype'] =='nominal' || $data['datatype'] =='dichotomus'){
					$value = $data['value'];
					/* $val = $data['value']; 
					$value = visits::getSubjectVariableValue($val); */
				} else {
					$value = $data['value']; 
				}
				$details = $details."<li><span class='left-txt'>".$data['variablelabel'].":</span> <span class='right-txt'>".$value."</span></li>";
			}
			
			$htmlPart = "<h2>".$heading."</h2>
		    <div class='popup-inner'>
		      <ul class='reg-lisiting'>".$details."</ul>
		    </div>";
		}else {
			$htmlPart = null;
		}
		echo json_encode($htmlPart); die;
	}
	
	/**
	 * downloadConsentFormAction
	 * 
	 * downloads the consent form signed
	 * 
	 * @author Raman
	 * @version 1.0
	 */
	public function downloadConsentFormAction(){
		
		$params = $this->getRequest()->getParams();
		$this->view->file = $params['file'];
	
	}
	
	
}