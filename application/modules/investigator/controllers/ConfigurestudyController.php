<?php
class Investigator_ConfigurestudyController extends Zend_Controller_Action {
	
	public function init() {
	
		$this->view->controllerName = $this->getRequest ()->getParam ( 'controller' );
		$this->view->action = $this->getRequest ()->getParam ( 'action' );
	
		$flash = $this->_helper->getHelper ( 'FlashMessenger' );
		$message = $flash->getMessages ();
		$this->view->messageSuccess = isset ( $message [0] ['success'] ) ? $message [0] ['success'] : '';
		$this->view->messageError = isset ( $message [0] ['error'] ) ? $message [0] ['error'] : '';
	}
	
	/**
	 * preDispatch
	 *
	 * check the authentication before page load
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function preDispatch() {
	
		if (!Auth_InvestigatorAdapter::hasIdentity()) {
			$this->_helper->redirector('index','index','login');
		}
	}
	
	
	/**
	 * Index action for studystatus
	 * @author skumar5
	 * @version 1.0
	 */
	public function indexAction() {
	
		// get zend session variables
		$studyInfo = new Zend_Session_Namespace ('InvestigatorInfo');
		$study_id = $studyInfo->investigator_studyId;
		$investigatorData = $studyInfo->investigator_data;
		$role = $investigatorData[0]['u__role_id'];
			
		#check secondary investigator permission and show view accordingly
		$chkpermission = BackEnd_Helper_viewHelper::checkSecInvestigatorPermission();
		$permission_id = $chkpermission['pId'];
		$this->view->perName = $chkpermission['pname'];
		$this->view->permissionId = $permission_id;
		$this->view->roleId = $role;
		#code ends here

		$this->view->Message = $this->view->translate('Are you sure you want to delete this studystatus?');
		$this->view->title = $this->view->translate('Delete Studystatus');

		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$list = studystatus::getStudyStatusData();
		$page=$this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($list);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($page);
			
		$this->view->studystatus=$paginator;
		$this->view->aes_Key=$aes_Key;
		$this->view->page=$page;
	}
	
	/**
	 * generalsettings of study
	 * @author pkaur4
	 * @version 1.0
	 */
	public function generalAction(){
		
		$studyStatuses = studystatus::checkstudystatuses();
		
		if(!empty($studyStatuses['type'])){
			
			//$type = array("1", "2", "3");
			//$typeCount = count($type);
			//$studyStatus = $studyStatuses['type'];
			//$containsSearch = count(array_intersect($type, $studyStatus));
			//if($containsSearch == $typeCount){
				
				$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
				$host = Zend_Registry::getInstance()->resources->db->params->host;
				$dbName= Zend_Registry::getInstance()->resources->db->params->dbname;
				$username = Zend_Registry::getInstance()->resources->db->params->username;
				$pass = Zend_Registry::getInstance()->resources->db->params->password;
				
				$studyGeneral = $this->view->translate('Study General Settings');
				$this->view->headTitle($studyGeneral);
				
				#get zend session variables
				$studyInfo = new Zend_Session_Namespace ('InvestigatorInfo');
				$study_id = $studyInfo->investigator_studyId;
				$investigatorData = $studyInfo->investigator_data;
				
				$role = $investigatorData[0]['u__role_id'];
				$investigator_id = $investigatorData[0]['u__id'];
				
				$data = studyConfiguration::getRefInvestigatorStudyID($investigator_id,$study_id,$role);
				$refInvestigaorStudyID = $data[0]['id'];
				
				#check secondary investigator permission and show view accordingly
				$chkpermission = BackEnd_Helper_viewHelper::checkSecInvestigatorPermission();
				$permission_id = $chkpermission['pId'];
				$this->view->perName = $chkpermission['pname'];
				$this->view->permissionId = $permission_id;
				$this->view->roleId = $role;
				#code ends here
				
				/* $status_type = 1;
				$subStatus = studyConfiguration::getSubStatusOptions($status_type, $refInvestigaorStudyID);
				$this->view->subStatus = $subStatus;
					
				$status_type = 2;
				$fieldStatus = studyConfiguration::getSubStatusOptions($status_type, $refInvestigaorStudyID);
				$this->view->fieldStatus = $fieldStatus; */
				
				$status_type = 3;
				$reasonDataChange = studyConfiguration::getSubStatusOptions($status_type, $refInvestigaorStudyID);
				$this->view->reasonDataChange = $reasonDataChange;
				
				#get study settings from database
				$records = studyConfiguration::getStudyConfigurations($refInvestigaorStudyID);
				
				$this->view->getvalue = $records;
				$this->view->aes_Key = $aes_Key;
				
				$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
				
				$expectedEnrollmentCount = studyrecord::getExpectedEnrollmentCount($study_id);
				$expEnroll = BackEnd_Helper_viewHelper::decryptString($expectedEnrollmentCount['expectedenrollment'] , $aes_Key);
				$this->view->expEnroll = $expEnroll;
				
				$sitesEnrollmentCount = sites::getSitesEnrollmentCount($configId);
				$enrollCountSites = BackEnd_Helper_viewHelper::decryptString(@$sitesEnrollmentCount[0]['enrollmentgoal'] , $aes_Key);
				
				$groupsEnrollmentCount = groups::getGroupsEnrollmentCount($configId);
				$enrollCountGroups = BackEnd_Helper_viewHelper::decryptString(@$groupsEnrollmentCount[0]['enrollmentgoal'] , $aes_Key);
				
				if(!empty($sitesEnrollmentCount) || !empty($groupsEnrollmentCount)){
					if($enrollCountSites < $enrollCountGroups){
						$this->view->enrollment = $enrollCountGroups;
					}else{
						$this->view->enrollment = $enrollCountSites;
					}
				}else{
					$this->view->enrollment = 0;
				}
				if ($this->getRequest()->isPost()){
				$params = $this->getRequest()->getParams();
				
				#save/update data in database
				$res = studyConfiguration::saveStudyConfigurations($params, $refInvestigaorStudyID, $investigator_id, $aes_Key, $host, $dbName, $username, $pass);
				$flash = $this->_helper->getHelper ( 'FlashMessenger' );
				
				if(!empty($params['studyConfigId'])){
					$message = $this->view->translate ( 'General Settings have been updated successfully.' );
				}else{
					$message = $this->view->translate ( 'General Settings have been saved successfully.' );
				}
				$flash->addMessage ( array ('success' => $message ) );
				$this->_helper->redirector('sites','configurestudy','investigator');
					
				}
			}
			else {
					$this->view->getvalue = NULL;
			}
		//}
	} 
 
	
	/**
	 * download uploaded file 
	 * @author pkaur4
	 * @version 1.0
	 */
	public function downloadFileAction(){
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$studyInfo = new Zend_Session_Namespace ('InvestigatorInfo');
		$study_id = $studyInfo->investigator_studyId;
		$investigatorData = $studyInfo->investigator_data;
		
		$investigator_id = $investigatorData[0]['u__id'];
		$roleId = $investigatorData[0]['u__role_id'];
		
		$data = studyConfiguration::getRefInvestigatorStudyID($investigator_id,$study_id,$roleId);
		
		$refInvestigaorStudyID = $data[0]['id'];
		
		$records = studyConfiguration::getStudyDocument($refInvestigaorStudyID);
		
		$template_pdf = base64_decode($records[0]['filecontent']);
		//$template_pdf = stripslashes($template);
		$tmp_name = "doc".md5(uniqid()).".pdf";
		
		//directory for uploading the pdf file
		$templateDir = UPLOAD_PATH."backend/consentform/";
		
		$document = $templateDir.$tmp_name;
		
		if(is_writable($templateDir)){
			file_put_contents($document, $template_pdf);
		}else{
		
			die('<p>The working directory is not writable, abort.</p>');
		}
		
		$this->view->document = $document;
		$this->view->aes_Key = $aes_Key;
		
		//if(file_exists($document)) @unlink($document);
		
	}	
	
	/**
	 * LISTSITES
	 *
	 * Display sites Listing
	 *
	 * @author skumar5 updated by pkaur4
	 * @version 1.0
	 */
	public function sitesAction() {
		
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		
		$records = studyConfiguration::getStudyConfigurationsCount($configId);		
		$totalStudyEnrollment = BackEnd_Helper_viewHelper::decryptString($records['targetenrollment'] , $aes_Key);
		
		$sitesEnrollmentCount = sites::getSitesEnrollmentCount($configId);
		
		if(!empty($sitesEnrollmentCount)){
			$totalSitesEnrollment = 0;	
			foreach($sitesEnrollmentCount as $count){	
				$enrollCount = BackEnd_Helper_viewHelper::decryptString($count['enrollmentgoal'] , $aes_Key);
				$totalSitesEnrollment = $enrollCount + $totalSitesEnrollment;
			}	
		}else{
			
			$totalSitesEnrollment = 0;
		}
		$count = $totalStudyEnrollment - $totalSitesEnrollment;
		$this->view->count = $count;
		
		if($configId == 0){
			$this->view->sites= NULL;
		}else {
			
			// get zend session variables
			$studyInfo = new Zend_Session_Namespace ('InvestigatorInfo');
			$study_id = $studyInfo->investigator_studyId;
			$investigatorData = $studyInfo->investigator_data;
			$role = $investigatorData[0]['u__role_id'];
				
			#check secondary investigator permission and show view accordingly
			$chkpermission = BackEnd_Helper_viewHelper::checkSecInvestigatorPermission();
			$permission_id = $chkpermission['pId'];
			$this->view->perName = $chkpermission['pname'];
			$this->view->permissionId = $permission_id;
			$this->view->roleId = $role;
			#code ends here
			
			$this->view->Message = $this->view->translate('Are you sure you want to delete this site?');
			$this->view->title = $this->view->translate('Delete Site');
			
			$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
			$flag = 0;
			$list = sites::getSitesList($flag,$configId);
			$page=$this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($list);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			
			$this->view->sites=$paginator;
			$this->view->aes_Key=$aes_Key;
			$this->view->page=$page;
		}
	}
	
	/**
	 * ADDSITES
	 *
	 * Add new sites to database
	 *
	 * @author skumar5 updated by pkaur4
	 * @version 1.0
	 */
	public function addNewSiteAction() {
		
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$newSite = $this->view->translate('Add New Site');
    	$this->view->headTitle($newSite);
    	$flash = $this->_helper->getHelper('FlashMessenger');
    	if ($this->getRequest ()->isPost ()) {
    		$params = $this->getRequest()->getParams();
    		$data = sites::addNewSite($params, $aes_Key);
    		$message = $this->view->translate('New site has been added successfully');
    		$flash->addMessage(array('success' => $message ));
    		$this->_helper->redirector('sites','configurestudy','investigator');	 
    	}
	}
	
	/**
	 * EDITSITES
	 *
	 * Edit sites
	 *
	 * @author skumar5 updated by pkaur4
	 * @version 1.0
	 */
	public function editSiteAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$editSite = $this->view->translate('Edit Site');
		$this->view->headTitle($editSite);
		$params = $this->_getAllParams();
		$id = $params['id'];
		$data = sites::getUpdateSiteData($id, $aes_Key);
		echo json_encode($data); die;		
	}
	
	/**
	 * UPDATESITE
	 *
	 * update site
	 *
	 * @author skumar5 updated by pkaur4
	 * @version 1.0
	 */
	public function updateSiteAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$flash = $this->_helper->getHelper('FlashMessenger');
		if ($this->getRequest ()->isPost ()) {
			$params = $this->_getAllParams();
			$data = sites::updateSiteData($params , $aes_Key);
			$message = $this->view->translate('Site has been updated successfully');
			$flash->addMessage(array('success' => $message ));
			$this->_helper->redirector('sites','configurestudy','investigator');
		}
	}
	
	/**
	 * DELETESITE
	 *
	 * update site
	 *
	 * @author skumar5 updated by pkaur4
	 * @version 1.0
	 */
	public function deleteSiteAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$params = $this->_getAllParams();
		$id = $params['id'];
		$data = sites::deleteSiteData($id,$aes_Key);
		$flash = $this->_helper->getHelper('FlashMessenger');
    	$message = $this->view->translate('Site record has been deleted successfully');
    	$flash->addMessage(array('success' => $message ));
    	echo $data;
    	die ();
    }
	
    /**
     * GETDELETEDDATA
     *
     * get deleted sites from database
     *
     * @author skumar5 updated by pkaur4
     * @version 1.0
     */
	public function recoverDeletedSitesAction() {
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$flag = 1;
		$data = sites::getSitesList($flag,$configId);
		$page = $this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($data);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($page);
		
		$this->view->recoveredSites = $paginator;
		$this->view->aes_Key = $aes_Key;
		$this->view->page = $page; 
	}
	
	/**
	 * RECOVERDELETEDSITES
	 *
	 * recover deleted sites
	 *
	 * @author skumar5 updated by pkaur4
	 * @version 1.0
	 */
	public function recoverSiteAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$params = $this->_getAllParams();
		$id = $params['id'];
		$data = sites::recoverSites($id,$aes_Key);
		$flash = $this->_helper->getHelper('FlashMessenger');
		$message = $this->view->translate('Site record has been recovered successfully');
		$flash->addMessage(array('success' => $message ));
		$this->_helper->redirector('recover-deleted-sites','configurestudy','investigator');	
	}
	
	/**
	 * groups
	 *
	 * Display groups Listing
	 *
	 * @author Raman updated by pkaur4
	 * @version 1.0
	 */
	public function groupsAction() {
		
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		
		$records = studyConfiguration::getStudyConfigurationsCount($configId);
		$totalStudyEnrollment = BackEnd_Helper_viewHelper::decryptString($records['targetenrollment'] , $aes_Key);
		
		$groupsEnrollmentCount = groups::getGroupsEnrollmentCount($configId);
		
		if(!empty($groupsEnrollmentCount)){
			$totalGroupsEnrollment = 0;
			foreach($groupsEnrollmentCount as $count){
				$enrollCount = BackEnd_Helper_viewHelper::decryptString($count['enrollmentgoal'] , $aes_Key);
				$totalGroupsEnrollment = $enrollCount + $totalGroupsEnrollment;
			}
		}else{
			$totalGroupsEnrollment = 0;
		}
		$count = $totalStudyEnrollment - $totalGroupsEnrollment;

		$this->view->count = $count;
		
		if($configId == 0){
			$this->view->groups= NULL;
		}else {
			
			// get zend session variables
			$studyInfo = new Zend_Session_Namespace ('InvestigatorInfo');
			$study_id = $studyInfo->investigator_studyId;
			$investigatorData = $studyInfo->investigator_data;
			$role = $investigatorData[0]['u__role_id'];
			
			#check secondary investigator permission and show view accordingly
			$chkpermission = BackEnd_Helper_viewHelper::checkSecInvestigatorPermission();
			$permission_id = $chkpermission['pId'];
			$this->view->perName = $chkpermission['pname'];
			$this->view->permissionId = $permission_id;
			$this->view->roleId = $role;
			#code ends here
			
			
			$this->view->Message = $this->view->translate('Are you sure you want to delete this group?');
			$this->view->title = $this->view->translate('Delete Group');
			
			$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
			$flag = 0;
			$list = groups::getGroupsList($flag, $configId);
		    $page=$this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($list);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
		
			$this->view->groups=$paginator;
			$this->view->aes_Key=$aes_Key;
			$this->view->page=$page;
		}	
	}
	
	/**
	 * addGroup
	 *
	 * Add group to database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function addGroupAction(){
		
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$group = $this->view->translate('Add Group');
		$this->view->headTitle($group);
		$flash = $this->_helper->getHelper('FlashMessenger');
		if ($this->getRequest ()->isPost ()) {
			$params = $this->getRequest()->getParams();
			$data = groups::addGroup($params, $aes_Key);
			$message = $this->view->translate('New group has been added successfully');
			$flash->addMessage(array('success' => $message ));
			$this->_helper->redirector('groups','configurestudy','investigator');
		}
	}
	
	/**
	 * edit a Group
	 *
	 * Fetch group data from database 
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function editGroupAction() {
		
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$editGroup = $this->view->translate('Edit Group');
		$this->view->headTitle($editGroup);	
		$params = $this->_getAllParams();
		$id = $params['id'];
		$data = groups::getUpdateGroupData($id, $aes_Key);
		echo json_encode($data); die;
	}
	
	/**
	 * updateGroup
	 *
	 * Update group data in database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function updateGroupAction(){
		
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$flash = $this->_helper->getHelper('FlashMessenger');
		if ($this->getRequest ()->isPost ()) {
			$params = $this->getRequest()->getParams();
			$data = groups::updateGroup($params, $aes_Key);
			$message = $this->view->translate('Group has been updated successfully');
			$flash->addMessage(array('success' => $message ));
			$this->_helper->redirector('groups','configurestudy','investigator');
		}
	}
	
	/**
	 * deleteGroup
	 *
	 * Delete group data from database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function deleteGroupAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$params = $this->_getAllParams();
		$id = $params['id'];
		$data = groups::deleteGroup($id, $aes_Key);
		$flash = $this->_helper->getHelper('FlashMessenger');
		$message = $this->view->translate('Group record has been deleted successfully');
		$flash->addMessage(array('success' => $message ));
		echo $data;
		die ();
	}
	
	/**
	 * deletedGroupsList
	 *
	 * Fetch deleted group list from database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function deletedGroupsListAction() {
		
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		if($configId == 0){
			$this->view->recoveredGroups= NULL;
		}else {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$flag = 1;
		$data = groups::getDeletedGroups($flag, $configId);
		$page=$this->_getParam('page',1);
		$paginator = Zend_Paginator::factory($data);
		$paginator->setItemCountPerPage(10);
		$paginator->setCurrentPageNumber($page);
	
		$this->view->recoveredGroups=$paginator;
		$this->view->aes_Key=$aes_Key;
		$this->view->page=$page;
		}
	}
	
	/**
	 * recoverGroup
	 *
	 * Recover deleted group record
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function recoverGroupAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$params = $this->_getAllParams();
		$id = $params['id'];
		$data = groups::recoverGroup($id, $aes_Key);
		$flash = $this->_helper->getHelper('FlashMessenger');
		$message = $this->view->translate('Group record has been recovered successfully');
		$flash->addMessage(array('success' => $message ));
		$this->_helper->redirector('deleted-groups-list','configurestudy','investigator');
	}
	
	/**
	 * visits
	 *
	 * Display visit listing
	 *
	 * @author Raman updated by pkaur4
	 * @version 1.0
	 */
	public function visitsAction() {
	
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		if($configId == 0){
			$this->view->visits= NULL;
		}else {
			// get zend session variables
			$studyInfo = new Zend_Session_Namespace ('InvestigatorInfo');
			$study_id = $studyInfo->investigator_studyId;
			$investigatorData = $studyInfo->investigator_data;
			$role = $investigatorData[0]['u__role_id'];
			
			$formsExist = forms::checkIfStaticFormsCreated($configId);
			$forms = 0;
			
			if($formsExist){
				
				$setupPreScreen = forms::checkToSetupPrescreen($configId);
				$setupPreScreen = BackEnd_Helper_viewHelper::decryptString($setupPreScreen['setupsubjectprescreen'], $aes_Key);
				if($setupPreScreen == 'no'){
					$preScreen = 2;
				}else{
					$preScreen = 0;
				}
			
				//add visits if not added automatically
				$get = visits::getVisit($configId, $preScreen);
				if($get){
					$set = visits::setVisit($get, $aes_Key, $configId);
				
					$visitIds = visits::getRegistrationConsentVisitId($configId);// get the registration and consent visit Ids
				
					$formIds = visits::getRegistrationConsentFormId($configId);// get the registration and consent visit Ids
				
					//assign registration and consent form ids to respective visits
					if(!empty($visitIds)){
						visits::assignFormsToVisits($visitIds, $formIds);
					}						
				}

				if($preScreen == 2){
				    $data = visits::checkPrescreenVisit($configId);
					
					if(!empty($data)){
						$visitId = $data[0]['id'];
						$deletePrescreenVisit = visits::deletePrescreenVisit($configId , $visitId, $aes_Key);
					}
				}else if($preScreen == 0){
									
					$data = forms::checkPrescreen($configId);
					if(empty($data)){
						$getPrescreen = forms::getPrescreenForm($configId);
						$set = forms::addPrescreenForm($getPrescreen, $aes_Key, $configId);
					}
					
					$data = visits::checkPrescreenVisit($configId);
						
					if(empty($data)){
						$getPrescreenVisit = visits::getPrescreenVisit($configId);
						$set = visits::addPrescreenVisit($getPrescreenVisit, $aes_Key, $configId);
					}
				}
				
				$forms = 1;
			} else {
				
				$forms = 0;
			}
			 
			$this->view->createFormMsg = $this->view->translate('Please Create the forms first!!');
			#check secondary investigator permission and show view accordingly
			$chkpermission = BackEnd_Helper_viewHelper::checkSecInvestigatorPermission();
			$permission_id = $chkpermission['pId'];
			$this->view->perName = $chkpermission['pname'];
			$this->view->permissionId = $permission_id;
			$this->view->roleId = $role;
			#code ends here
			
			
			$this->view->Message = $this->view->translate('Are you sure you want to delete this visit?');
			$this->view->title = $this->view->translate('Delete Visit');
				
			
			$flag = 0;
			$list = visits::getVisitsList($flag, $configId);
			
			$page=$this->_getParam('page',1);
			$paginator = Zend_Paginator::factory($list);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			$assignedFormsList = forms::getAssignedFormsToStudy($configId);
			$assignFormsArr = array();
			foreach($assignedFormsList as $list){
				$assignFormsArr[] = $list['form_id'];
			}
			
			$formsStr = implode(',', $assignFormsArr);
			$formsString = "'".$formsStr."'";
 			$formList = forms::getFormListExceptAssigned($flag, $configId, $formsString);
	
			$this->view->visits=$paginator;
			$this->view->aes_Key=$aes_Key;
			$this->view->page=$page;
			$this->view->formList=$formList;
			$this->view->forms=$forms;
		}
	}
	
	/**
	 * deleteVisit
	 *
	 * Delete visit data from database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function deleteVisitAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		 $params = $this->_getAllParams();
		$id = $params['id'];
		$data = visits::deleteVisit($id, $aes_Key);
		$flash = $this->_helper->getHelper('FlashMessenger');
		$message = $this->view->translate('Visit record has been deleted successfully');
		$flash->addMessage(array('success' => $message ));
		echo $data;
		die ();
	}
	
	/**
	 * addVisit
	 *
	 * Add visit to database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function addVisitAction(){
		
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$addVisit = $this->view->translate('Add Visit');
		$this->view->headTitle($addVisit);
		$flash = $this->_helper->getHelper('FlashMessenger');
		if ($this->getRequest ()->isPost ()) {
			$params = $this->getRequest()->getParams();
	
			$updateid = sha1(round(microtime(true) * 1000));
			
			$data = visits::addVisit($params, $aes_Key,$updateid);
			$message = $this->view->translate('New visit has been added successfully');
			$flash->addMessage(array('success' => $message ));
			$this->_helper->redirector('visits','configurestudy','investigator');
		}
	}
	
	/**
	 * editVisit
	 *
	 * Fetch visit data from database
	 *
	 * @author Raman updated By Raman
	 * @version 1.0
	 */
	public function editVisitAction() {
	
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$editVisit = $this->view->translate('Edit Visit');
		$this->view->headTitle($editVisit);
		$params = $this->_getAllParams();
		$id = $params['id'];
		$data['visit'] = visits::getVisitDataForEdit($id, $aes_Key);
		$data['id'] = $id;
		$data['name'] = visits::getVisitNameIdBasis($id, $aes_Key);
		echo json_encode($data); die;
	}
	
	/**
	 * updateVisit
	 *
	 * Update visit data in database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function updateVisitAction(){
	
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$flash = $this->_helper->getHelper('FlashMessenger');
		if ($this->getRequest ()->isPost ()) {
			$params = $this->getRequest()->getParams();

			$updateid = sha1(round(microtime(true) * 1000));
			
			$data = visits::updateVisit($params, $aes_Key ,$updateid);
			$message = $this->view->translate('Visit has been updated successfully');
			$flash->addMessage(array('success' => $message ));
			$this->_helper->redirector('visits','configurestudy','investigator');
		}
	}
	
	/**
	 * forms
	 *
	 * Display forms Listing
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function formsAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		if($configId == 0){
			$this->view->forms= NULL;
		}else {

			$setupPreScreen = forms::checkToSetupPrescreen($configId);
			$setupPreScreen = BackEnd_Helper_viewHelper::decryptString($setupPreScreen['setupsubjectprescreen'], $aes_Key);
			if($setupPreScreen == 'no'){
				$preScreen = 2;
			
			}else{
			    $preScreen = 0;
			}
			// This sets the static forms automatically
			$get = forms::getForm($configId, $preScreen);
		    if($get){
		        $set = forms::setForm($get, $aes_Key, $configId);
			
		    }
			
			if($preScreen == 2){
				
				$data = forms::checkPrescreen($configId);
				if(!empty($data)){
					$deletePrescreen = forms::deletePrescreen($configId,$aes_Key);
				}

			}else if($preScreen == 0){

				$data = forms::checkPrescreen($configId);
				
				if(empty($data)){
					$getPrescreen = forms::getPrescreenForm($configId);
					$set = forms::addPrescreenForm($getPrescreen, $aes_Key, $configId);
				}
			}				

		}
			
			$this->view->formMessage = $this->view->translate('Are you sure you want to delete this form?');
			$this->view->formTitle = $this->view->translate('Delete Form');	
						
			$this->view->Message = $this->view->translate('Are you sure you want to delete form-variable and its value?');
			$this->view->title = $this->view->translate('Delete Form Variable');				

			$flag = 0;
			$list = forms::getFormList($flag, $configId);
	
			$page=$this->_getParam('page', 1);
			$paginator = Zend_Paginator::factory($list);
			$paginator->setItemCountPerPage(10);
			$paginator->setCurrentPageNumber($page);
			
			// get zend session variables
			$studyInfo = new Zend_Session_Namespace ('InvestigatorInfo');
			$study_id = $studyInfo->investigator_studyId;
			$investigatorData = $studyInfo->investigator_data;
			$role = $investigatorData[0]['u__role_id'];
			
			#check secondary investigator permission and show view accordingly
			$chkpermission = BackEnd_Helper_viewHelper::checkSecInvestigatorPermission();
			if(empty($chkpermission)){
				$permission_id = "all";
				$PerName = "all";
			} else {
				$permission_id = $chkpermission['pId'];
				$PerName = $chkpermission['pname'];
			}
			
			$this->view->perName = $PerName;
			$this->view->permissionId = $permission_id;
			$this->view->roleId = $role;
			#code ends here
			
			$this->view->forms=$paginator;
			$this->view->aes_Key=$aes_Key;
			$this->view->page=$page;
		}
	
	/**
	 * addForm
	 *
	 * Add form to database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function addFormAction(){
			
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$addForm = $this->view->translate('Add Form');
		$this->view->headTitle($addForm);
		$flash = $this->_helper->getHelper('FlashMessenger');
		if ($this->getRequest ()->isPost ()) {
			$params = $this->getRequest()->getParams();
			$data = forms::addForm($params, $aes_Key);
			$message = $this->view->translate('New Form has been added successfully');
			$flash->addMessage(array('success' => $message ));
			$this->_helper->redirector('forms','configurestudy','investigator');
		}
	}
	
	/**
	 * addFormVariable
	 *
	 * Add form to database
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function addFormVariableAction(){
		
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$addVars = $this->view->translate('Add Variables');
		$this->view->headTitle($addVars);
		$flash = $this->_helper->getHelper('FlashMessenger');
		if ($this->getRequest ()->isPost ()) {
			$params = $this->getRequest()->getParams();		
			$data = forms::addFormVariables($params, $aes_Key);
			$message = $this->view->translate('Form Variables has been added successfully');
			$flash->addMessage(array('success' => $message ));
			$this->_helper->redirector('forms','configurestudy','investigator');
		}
	}
	
	/**
	 * previewForm
	 *
	 * display preview of form
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function previewFormAction(){
		
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$preview = $this->view->translate('Preview');
		$this->view->headTitle($preview);
		$params = $this->getRequest()->getParams();
		$formData = forms::getFormType($params['id']);		
		$formTypeName = BackEnd_Helper_viewHelper::decryptString($formData['name'], $aes_Key);
		
		$data = forms::previewForm($params['id']);
		$resultHTML = '';
		$content = '';
		if(!empty($data)){ 
		foreach($data as $result){
		$VarLebel = BackEnd_Helper_viewHelper::decryptString($result['label'], $aes_Key);
		$VarName = BackEnd_Helper_viewHelper::decryptString($result['variablename'], $aes_Key);
		$VarDataType = BackEnd_Helper_viewHelper::decryptString($result['datatype'], $aes_Key);
		$VarUpperLimit = BackEnd_Helper_viewHelper::decryptString($result['rangeto'], $aes_Key);
		$VarLowerLimit = BackEnd_Helper_viewHelper::decryptString($result['rangefrom'], $aes_Key);
		
		$formType = "<h2 id=form_type style='text-align:center; text-decoration: underline;margin:-10px 0 34px 0px;'>Form Type: $formTypeName</h2>";
				
		$rowFirst = "<fieldset><span class='label-name'>$VarLebel</span><div class='input-box'>";

		
		if($VarDataType == 'number' || $VarDataType == 'date' || $VarDataType == 'text' || $VarDataType == 'time' || $VarDataType == 'string' || $VarDataType == 'email'){
			if(isset($VarUpperLimit) && $VarUpperLimit != ' '){
	      	$max = 'max = '.$VarUpperLimit;
	      }else {
	      	$max = '';
	      }
	      
	      if(isset($VarLowerLimit) && $VarLowerLimit != ' '){
	      	$min = 'min = '.$VarLowerLimit;
	      }else {
	      	$min = ' ';
	      }
            $content = "<input type='text' title='Enter Variable Label' $max value='' name='$VarName' $min>";
		}else if($VarDataType == 'nominal'){
            
			$content = "<select name='$VarName'><option value=''>Select</option>";
			
			foreach($result['value'] as $Valdata){
				$optionName = BackEnd_Helper_viewHelper::decryptString($Valdata['varValue'], $aes_Key);
				$optionVal =  $Valdata['id'];
			    $content .= "<option value='$optionVal'>$optionName</option>";
			}
			
			$content .= "</select>";		
		
		}else if($VarDataType == 'ordinal'){ 
			$content = '';
			foreach($result['value'] as $Valdata){
				$optionName = BackEnd_Helper_viewHelper::decryptString($Valdata['varValue'], $aes_Key);
				$optionVal =  $Valdata['id'];
				$content .= "<input type='checkbox' name = '$VarName' value='$optionVal' style='margin:12px 5px 0px 10px;' />". $optionName;
			}
		}else if($VarDataType == 'dichotomus'){
			$content = "<select name='$VarName' multiple class='multiple'>";
			
			foreach($result['value'] as $Valdata){
				$optionName = BackEnd_Helper_viewHelper::decryptString($Valdata['varValue'], $aes_Key);
				$optionVal =  $Valdata['id'];
			    $content .= "<option value='$optionVal'>$optionName</option>";
			}
			
			$content .= "</select>";	
		}
		
        $rowLast = "<div class='clr'></div></div></fieldset>";
        $resultHTML .= $rowFirst.$content.$rowLast;
		}
			$resultHTML = $formType.$resultHTML;
		}else {
			$resultHTML = null;
		}
		
		echo json_encode($resultHTML);
		die;
	}
	
	/**
	 * arrangeVariable
	 *
	 * arrange variable in an order
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function arrangeVariableAction(){
		
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$var = $this->view->translate('Arrange Variables');
		$this->view->headTitle($var);
		$params = $this->getRequest()->getParams();
		$data = forms::getFormVariables($params['id']);

		$resultHTML = '';
		
		if(!empty($data)){
	    $i = 1;
	    $rowFirst = "<table width='75%' cellspacing='0' cellpadding='0' border='1'>";
	    $content = '';
		foreach($data as $result){
		$VarLebel = BackEnd_Helper_viewHelper::decryptString($result['variablelabel'], $aes_Key);
		$VarName = BackEnd_Helper_viewHelper::decryptString($result['variablename'], $aes_Key);	
		$id = $result['id'];
		
		$content .= "<tr id=$id><td width='15%'>$i</td><td width='50%'>$VarLebel</td><td width='35%'>$VarName</td></tr>";        
        $i++;
		}
		$rowLast = "</table>";
		$resultHTML .= $rowFirst.$content.$rowLast;

		}else {
		$resultHTML = null;
		}
		
		echo json_encode($resultHTML); die;
	}
	
	/**
	 * deleteVariableAction
	 *
	 * delete variable in an order
	 *
	 * @author skumar5
	 * @version 1.0
	 */
	
	public function deleteVariableAction(){
	
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$delvar = $this->view->translate('Delete Variables');
		$this->view->headTitle($delvar);
		$params = $this->getRequest()->getParams();
		$data = forms::getFormVariables($params['id']);
	
		$resultHTML = '';
	
		if(!empty($data)){
		
			$i = 1;
			$rowFirst = "<table width='75%' cellspacing='0' cellpadding='0' border='1'>";
			$content = '';
			foreach($data as $result){
				$VarLebel = BackEnd_Helper_viewHelper::decryptString($result['variablelabel'], $aes_Key);
				$VarName = BackEnd_Helper_viewHelper::decryptString($result['variablename'], $aes_Key);
				$id = $result['id'];
				
				//Start
				$VarDataType = BackEnd_Helper_viewHelper::decryptString($result['datatype'],$aes_Key);
				//End				
				
				if($VarName=='email' || $VarName=='a9bcfb6ec7359af8b92a90c7a21_firstname' || $VarName=='a9bcfb6ec7359af8b92a90c7a21_lastname' || $VarName=='a9bcfb6ec7359af8b92a90c7a21_dob' ) {					
				$content .= "<tr id=$id><td width='15%'>$i</td><td width='50%'>$VarLebel</td><td width='35%'>$VarName</td><td>Static Variable</td></tr>";
		}else{
				$content .= "<tr id=$id><td width='15%'>$i</td><td width='50%'>$VarLebel</td><td width='35%'>$VarName</td><td><a onclick='deleteRecord($id)' href='javascript:;'>Delete</a></td></tr>";				
		 }
								
				$i++;
			}
			$rowLast = "</table>";
			$resultHTML .= $rowFirst.$content.$rowLast;
		}else {
			$resultHTML = null;
		}
	
		echo json_encode($resultHTML); die;
	}
	

	/**
	 * deleteVariableAction
	 *
	 * delete variable in an order
	 *
	 * @author skumar5
	 * @version 1.0
	 */
	
	public function deleteFormVariableAction(){
		
		$params = $this->getRequest()->getParams();
		$data = formvariable::deleteVariableAndValue($params);
		$message = $this->view->translate('Variables has been deleted successfully');
		$flash = $this->_helper->getHelper('FlashMessenger');
		$flash->addMessage(array('success' => $message ));
		$this->_helper->redirector('forms','configurestudy','investigator');
		echo $data; die;
	}
	
	/**
	 * deleteForms
	 *
	 * delete forms
	 *
	 * @author skumar5
	 * @version 1.0
	 */
	 
	public function deleteFormsAction(){
		
		$params = $this->getRequest()->getParams();
		$data = forms::deleteForms($params);
		if($data == 1){
			$message = $this->view->translate('Form has been deleted successfully');
			$flash = $this->_helper->getHelper('FlashMessenger');
			$flash->addMessage(array('success' => $message ));
		}
		
		echo $data; die;
	}
		
	public function updateOrderAction(){
		
		$params = $this->getRequest()->getParams();
		$data = forms::updateVariableOrder($params);
		$message = $this->view->translate('Variables order has been updated successfully');
		$flash = $this->_helper->getHelper('FlashMessenger');
		$flash->addMessage(array('success' => $message ));
		$this->_helper->redirector('forms','configurestudy','investigator');
	}
	
	/**
	 * getformtypesAction
	 *
	 * get all the form types
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function getformtypesAction(){
	
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$enData = forms::getFormTypes();
		$decData = BackEnd_Helper_viewHelper::decryptWsArray($enData, $aes_Key);
		
		$startbox = "<select name='form_type' id='form_type'>";
		
		$option = '';
		foreach($decData as $data){
			$option = $option."<option value=".$data['type_id'].">".$data['name']."</option>";
		}
		
		
		$endbox = "</select>";
		$dynamicBox = $startbox.$option.$endbox;
		//print_r($decData); die;
		echo json_encode($dynamicBox); die;
	}
	
	/**
	 * blockStudyAction
	 *
	 * @author skumar5
	 * @version 1.0
	 */
	public function blockStudyAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$this->view->Message = $this->view->translate('Are you sure you want to Lock this Study?');
		$this->view->title = 'Lock Study';
		$this->view->aes_Key = $aes_Key;
	}
	
	/**
	 * studyBlockAction
	 *
	 *lock the study after data collection
	 *
	 * @author skumar5
	 * @version 1.0
	 */
	public function studyBlockAction() {
		
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		$data = studyConfiguration::blockStudy($configId);
		$flash = $this->_helper->getHelper('FlashMessenger');
		$message = $this->view->translate('Study has been locked successfully');
		$flash->addMessage(array('success' => $message ));
		echo $data;
		die ();
	}
	
	/**
	 * studyLockForAnalysis
	 *
	 * @author skumar5
	 * @version 1.0
	 */
	public function studyLockForAnalysisAction() {
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$this->view->Message = $this->view->translate('Are you sure you want to Lock the Study for data analysis?');
		$this->view->title = 'Lock Study For Data Analysis';
		$this->view->aes_Key = $aes_Key;
	}
	
	/**
	 * blockStudyForAnalysis
	 *
	 *lock the study after data analysis
	 *
	 * @author skumar5
	 * @version 1.0
	 */
	public function blockStudyForAnalysisAction() {
	
		$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
		$data = studyConfiguration::blockAnalysisStudy($configId);
		$flash = $this->_helper->getHelper('FlashMessenger');
		$message = $this->view->translate('Study has been locked successfully for data analysis');
		$flash->addMessage(array('success' => $message ));
		echo $data;
		die ();
	}
}
