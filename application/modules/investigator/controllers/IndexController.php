<?php

class Investigator_IndexController extends Zend_Controller_Action
{

    public function init()
    {
    	/* Initialize action controller here */
    	$this->view->controllerName = $this->getRequest()->getParam('controller');
    	$this->view->action = $this->getRequest()->getParam('action');
    	 
    	$flash = $this->_helper->getHelper('FlashMessenger');
    	$message = $flash->getMessages();
    	$this->view->messageSuccess = isset($message[0]['success']) ? $message[0]['success'] : '';
    	$this->view->messageError = isset($message[0]['error']) ? $message[0]['error'] : '';
    }

    /**
     * preDispatch
     *
     * check the authentication before page load
     *
     * @author Raman
     * @version 1.0
     */
    public function preDispatch() {
    
    	if (!Auth_InvestigatorAdapter::hasIdentity()) {
    		$this->_helper->redirector('index','index','login');
    	}
    }
    
    public function indexAction()
    {
    	$configId = BackEnd_Helper_viewHelper::checkStudyConfigueId();
    	$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
    	$params = $this->getRequest()->getParams();
    	// get zend session variables
    	$studyInfo = new Zend_Session_Namespace ('InvestigatorInfo');	
    	$study_id = $studyInfo->investigator_studyId;
    	$investigatorData = $studyInfo->investigator_data;
    	
    	$role = $investigatorData[0]['u__role_id'];
    	$investigator_id = $investigatorData[0]['u__id'];
    	
    	$data = studyConfiguration::getRefInvestigatorStudyID($investigator_id,$study_id,$role);
    	
    	$refInvestigaorStudyID = $data[0]['id'];
    	
    	$flag = 0;
    	$groupList = groups::getGroupsList($flag, $configId);
    	$visitList = visits::getVisitsList($flag, $configId);
    	$siteList = sites::getSitesList($flag,$configId);
    	$records = studyConfiguration::getStudyConfigurations($refInvestigaorStudyID);
    	$this->view->aes_Key = $aes_Key;
    	$this->view->group = count($groupList);
    	$this->view->visit = count($visitList);
    	$this->view->site = count($siteList);
    	$targetEnrollment = BackEnd_Helper_viewHelper::decryptString(@$records[0]['targetenrollment'], $aes_Key);
        $this->view->enrollment = $targetEnrollment;
        if(isset($targetEnrollment) && $targetEnrollment != ''){
           $tarEnroll = $targetEnrollment;
        }else {
           $tarEnroll = 0;
        }
    	
    	if(isset($params['req']) && $params['req'] == 'chart1'){ 
    	$enrolledSubjectData = user::getEnrolledSubjectData($tarEnroll, $configId);
    	$tempArray =  array();   
        if(empty($enrolledSubjectData)){
        	$tempArray = "";
        }else {
    	$newArrayOne = array();    	
    	$j = 0;
    	for($i=0; $i<=11; $i++){
    		if($enrolledSubjectData[$j]['monthCount'] == $i+1){
    			$cumulative += $enrolledSubjectData[$j]['percentage'];
    			
    			$newArrayOne[$i]['percentage'] .= $enrolledSubjectData[$j]['percentage'];
    			$newArrayOne[$i]['cumulative'] .= $cumulative;
    			$j++;
    		}else {
    			$newArrayOne[$i]['percentage'] .= ' ';
    			$newArrayOne[$i]['cumulative'] .= ' ';
    		}
    	}

    	foreach($newArrayOne as $data){


	    		    $tempArray['percentage'][] = $data['percentage'];
	    		    $tempArray['cumulative'][] = $data['cumulative'];

    	    
    	} 
        }

    	echo json_encode($tempArray, JSON_NUMERIC_CHECK); die;
    	}
    	
//     	if(isset($params['req']) && $params['req'] == 'chart2'){
//     		$subjectStatusData = studystatus::getSubjectStatusData($refInvestigaorStudyID);
//     		$subjectStatusList = studystatus::getSubjectStatusList($refInvestigaorStudyID);
//     		$tempArray =  array();
//             if(empty($subjectStatusData)){
//             	$tempArray = "";
//             }else {
//     		$newArrayTwo = array();
    		
//     		$j = 0;
//     		for($i=0; $i<count($subjectStatusList); $i++){
//     			if($subjectStatusData[$j]['id'] == $subjectStatusList[$i]['id']){
//     				$newArrayTwo[$i]['statusVal'] .= $subjectStatusData[$j]['countSubStatus'];
//     				$j++;
//     			} else {
//     				$newArrayTwo[$i]['statusVal'] .= ' ';
//     			}
//     			$newArrayTwo[$i]['subName'] .= BackEnd_Helper_viewHelper::decryptString($subjectStatusList[$i]['name'], $aes_Key);			 
    		    
//     		} 
    		
//     		foreach($newArrayTwo as $data){
//     		  $tempArray['subName'][] = $data['subName'];
//     		  $tempArray['statusVal'][] = $data['statusVal'];
//     		}
//             }
//     		echo json_encode($tempArray, JSON_NUMERIC_CHECK); die;
//     	}
    	
        if(isset($params['req']) && $params['req'] == 'chart2'){
      		$flag = 0;
      		$getSubjectVisitData = visits::getSubjectVisitDataChart($flag, $configId);
      		
      		$tempArray =  array();
      		if(empty($getSubjectVisitData)){
      		  $tempArray = "";
      		}else {
      		  $newArrayTwo = array();
      		
      		for($i=0; $i<count($getSubjectVisitData); $i++){      	
      		  $newArrayTwo[$i]['statusVal'] .= $getSubjectVisitData[$i]['subjectStatus_count'][0]['count(id)'];
      		  $newArrayTwo[$i]['subName'] .= BackEnd_Helper_viewHelper::decryptString($getSubjectVisitData[$i]['visitname'], $aes_Key);      		        }
      		
      		foreach($newArrayTwo as $data){
      		  $tempArray['subName'][] = $data['subName'];
      		  $tempArray['statusVal'][] = $data['statusVal'];
      		}

      		}
      		echo json_encode($tempArray, JSON_NUMERIC_CHECK); die; 		
      	}
      	
      	
    	if(isset($params['req']) && $params['req'] == 'chart3'){
    		$enrolledSubjectSiteWiseData = user::getEnrolledSubjectSiteWiseData($configId, $aes_Key);
    		//print_r($enrolledSubjectSiteWiseData);die;
    		$mainArrayOne = array();
    		$emptyData = true;
    		foreach($enrolledSubjectSiteWiseData as $data){
    			if(!empty($data)){
    				$emptyData = false;
    			}
    		}
    		if($emptyData == true){
    			$new = "";
    		}else {
    		foreach($enrolledSubjectSiteWiseData as $k => $dataset){
    			$mainArrayOne['Site' + $k]['name'] =  $dataset['siteName'].' (% Enrolled)';
    			$arr1 = array();
    			$newA = array();
    			foreach($dataset as $key => $data){
    				for($i=1; $i<=12; $i++){
    					if($data['monthCount'] != $i && !in_array($i,$newA)){
    						$arr1[$i] = '';
    					}
    					else { 
    						$arr1[$data['monthCount']] = $data['percentage'];
    						$newA[] = $data['monthCount'];
    					}
    				}				
    			}
    			
    			$mainArrayOne['Site' + $k]['data'] = array_values($arr1);
    		}
    		
    		$mainArrayTwo = array();
    		foreach($enrolledSubjectSiteWiseData as $k => $dataset){
    			$mainArrayTwo['Site' + $k]['name'] =  $dataset['siteName'].' Cumulative (% Enrolled)';
    			$arr1 = array();
    			$newA = array();
    			foreach($dataset as $key => $data){
    				for($i=1; $i<=12; $i++){
    					if($data['monthCount'] != $i && !in_array($i,$newA)){
    						$arr1[$i] = '';
    					}
    					else {
    						$arr1[$data['monthCount']] += $data['percentage'];
    						$newA[] = $data['monthCount'];
    					}
    				}
    			}
    			 
    			$mainArrayTwo['Site' + $k]['data'] = array_values($arr1);
    		}
    		
    		$new = array();
    		for ($i=0; $i<count($mainArrayOne); $i++) {
    			$new[] = $mainArrayOne[$i];
    			$new[] = $mainArrayTwo[$i];
    		}
    		}
    		echo json_encode($new, JSON_NUMERIC_CHECK); die;

    	}
    	
    	
    	if(isset($params['req']) && $params['req'] == 'chart4'){
    		$enrolledSubjectGroupWiseData = user::getEnrolledSubjectGroupWiseData($configId, $aes_Key);
    		//print_r($enrolledSubjectGroupWiseData); die;
    		$mainArrayOne = array();
    		$emptyData = true;
    		foreach($enrolledSubjectGroupWiseData as $data){
    			if(!empty($data)){
    				$emptyData = false;
    			}
    		}
    		if($emptyData == true){
    			$new = "";
    		}else {
    		foreach($enrolledSubjectGroupWiseData as $k => $dataset){
    			$mainArrayOne['Group' + $k]['name'] =  $dataset['groupName'].' (% Enrolled)';
    			$arr1 = array();
    			$newA = array();
    			foreach($dataset as $key => $data){
    				for($i=1; $i<=12; $i++){
    					if($data['monthCount'] != $i && !in_array($i,$newA)){
    						$arr1[$i] = '';
    					}
    					else {
    						$arr1[$data['monthCount']] = $data['percentage'];
    						$newA[] = $data['monthCount'];
    					}
    				}
    			}
    			 
    			$mainArrayOne['Group' + $k]['data'] = array_values($arr1);
    		}
    	
    		$mainArrayTwo = array();
    		foreach($enrolledSubjectGroupWiseData as $k => $dataset){
    			$mainArrayTwo['Group' + $k]['name'] =  $dataset['groupName'].' Cumulative (% Enrolled)';
    			$arr1 = array();
    			$newA = array();
    			foreach($dataset as $key => $data){
    				for($i=1; $i<=12; $i++){
    					if($data['monthCount'] != $i && !in_array($i,$newA)){
    						$arr1[$i] = '';
    					}
    					else {
    						$arr1[$data['monthCount']] += $data['percentage'];
    						$newA[] = $data['monthCount'];
    					}
    				}
    			}
    	
    			$mainArrayTwo['Group' + $k]['data'] = array_values($arr1);
    		}
    	
    		$new = array();
    		for ($i=0; $i<count($mainArrayOne); $i++) {
    			$new[] = $mainArrayOne[$i];
    			$new[] = $mainArrayTwo[$i];
    		}
    		}
    		echo json_encode($new, JSON_NUMERIC_CHECK); die;
    	
    	}
    }
}
