<?php

class Login_IndexController extends Zend_Controller_Action {
	
	public function init() {
		/*$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
	echo BackEnd_Helper_viewHelper::encryptString("ae8b3hiMIqJL1iRH+ALLrA==", $aes_Key);die;*/
		$flash = $this->_helper->getHelper('FlashMessenger');
		$message = $flash->getMessages();
		$this->view->messageSuccess = isset($message[0]['success']) ? $message[0]['success'] : '';
		$this->view->messageError = isset($message[0]['error']) ? $message[0]['error'] : '';
		
	}
	
	/**
	 * handle request
	 * 
	 * @see Zend_Controller_Action::preDispatch()
	 * @author Raman
	 * @version 1.0
	 */
	public function preDispatch() {
		
		// get action name from zend parametes
		$action = $this->getRequest ()->getActionName ();
		
		// check action
		switch ($action) {
			
			case "logout" :
				
			break;
			
			case "logoutinvestigator" :
			
			break;
			
			case "logoutsubject" :
					
			break;
			
			default :
				// if user is authenticated
				 if (Auth_ClinictrialAdapter::hasIdentity () || Auth_InvestigatorAdapter::hasIdentity () || Auth_SubjectAdapter::hasIdentity ()) {
					
					$u = Zend_Auth::getInstance ()->getIdentity (); 
					if($action != 'create-investigator-account' && $action != 'create-subject-account'){
						
						if($u[0]["u__role_id"] == 1){
						    $this->_redirect('/clinictrial');
	 					}else if($u[0]["u__role_id"] == 2 || $u[0]["u__role_id"] == 3){
							$this->_redirect('/investigator');
						}else if($u[0]["u__role_id"] == 4){
							$this->_redirect('/subject');
						}
					} 
				} 
			break;
		}
	}
	
	/**
	 * To authenticate the user for the application
	 * 
	 * @author Raman 
	 * @version 1.0
	 */
	public function indexAction() { 

		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$passwordExpirationTime = Zend_Registry::getInstance()->constants->PASSWORD_EXPIRATION_TIME;
		
		$params = $this->_request->getParams ();
		$login = $this->view->translate('Login');
		$this->view->headTitle($login);
		$this->view->aes_Key = $aes_Key;
		// check post form or not
		if ($this->getRequest ()->isPost ()) { 

			// then unset the msg session
			
			Zend_Session::namespaceUnset ( 'msg' );
		    $username = $params ['username'];
		    $orgUsername  = $params['username'];
			$password = $params ['password'];
			$userType = $params ['user_type'];
			@$studyId = $params ['investigator_study'];
			$timeSeconds = 28800;
			
			switch ($userType) {
				
				case '2' : # Primary Investigator
				case '3' : # Secondary Investigator
				if($studyId != 0){
				    // set authentication if Investigator valid					
					$username = BackEnd_Helper_viewHelper::encryptString($params['username'], $aes_Key);
					
					$userpasssalt = user::getPassSalUsername($username);
					$salt = $userpasssalt['r__password_salt'];
					$newpass = SHA1($params['password'].$salt);
					
					$days = BackEnd_Helper_viewHelper::getDateDifference($username);
					
					if($days < $passwordExpirationTime){
						
						$data_adapter = new Auth_InvestigatorAdapter ( $username, $newpass, $aes_Key);
						$auth = Zend_Auth::getInstance ();
						$result = $auth->authenticate ( $data_adapter );
					
					if (Auth_InvestigatorAdapter::hasIdentity ()) {
							
							$obj = Auth_InvestigatorAdapter::getIdentity ();		
							$id = $obj[0]['u__id'];
							//salt updation using session id
							$new_salt = md5(round(microtime(true) * 1000));
							
							//updating salt
							$updatedsal = refusersecurity::updateSal($id,$new_salt);
							
							$updpwd = SHA1($password.$new_salt);
							User::updatePwd($id, $updpwd);
							
							//for session logs
							$token = SHA1(round(microtime(true) * 1000));
							$ip_address = $_SERVER['REMOTE_ADDR'];
							$sessionObj = new sessionLogs();
							$saveSession = $sessionObj->saveSessionDetails($id , $token , $ip_address);
							$user = new Zend_Session_Namespace ('InvestigatorInfo');
							$user->investigator_data = $obj;
							$user->investigator_token = $token;
							$user->investigator_studyId = $studyId;
							
								$investigatorStudyInfo = studyrecord:: getInvestigatorStudyInfo($studyId, $aes_Key);
							    $investigatorCompLogo = studyrecord:: getInvestigatorCompanyLogo($investigatorStudyInfo['comId'], $aes_Key);
							    //print_r($investigatorCompLogo);die;
							    $user->investigatorStudyInfo = $investigatorStudyInfo;
							    $user->investigatorCompLogo = $investigatorCompLogo;
								//automatic expiration
								$user->setExpirationSeconds ( $timeSeconds );
								$sessionNamespace = new Zend_Session_Namespace ();
								$referer = new Zend_Session_Namespace ( 'referer' );
								$namespace = new Zend_Session_Namespace ( 'Zend_Auth' );
								$namespace->setExpirationSeconds ( $timeSeconds );

							
								if (isset ( $referer->refer ) && $referer->refer != '') {
										
									$url = $referer->refer;
										
								} else {
										
									$url = MODULE_INVESTIGATOR;
								}
								#flash message settings
								$flash = $this->_helper->getHelper('FlashMessenger');
								$message = $this->view->translate('Welcome to Investigator panel');
								$flash->addMessage(array('success' => $message ));
															// redirect to other page(user list)
								$this->_redirect ( rtrim ( $url, '?' ) );
								
						} else {	
												
							$flash = $this->_helper->getHelper('FlashMessenger');
							$message = $this->view->translate('Please enter valid username/password');
							$flash->addMessage(array('error' => $message ));
							// redirect to same page but with msg parameter
							$this->_helper->redirector ( 'index', 'index', 'login');
						}	
					}else{
						
						$url = MODULE_LOGIN."/index/password-expire/uname/".urlencode($orgUsername);
						$this->_redirect ( rtrim ( $url, '?' ) );
					}
				}else{
					$flash = $this->_helper->getHelper('FlashMessenger');
					$message = $this->view->translate('Please select a valid Study');
					$flash->addMessage(array('error' => $message ));
					// redirect to same page but with msg parameter
					$this->_helper->redirector ( 'index', 'index', 'login');
				}
					
					break;
				
				case '1' : # clinical trial
				    
				    // set authentication if Clinical trial valid
				   
				    //$s = self::unique_string();
					$username = BackEnd_Helper_viewHelper::encryptString($username, $aes_Key);
				    $userpasssalt = user::getPassSal($params,$aes_Key);
				    
		 		if(!empty($userpasssalt)){
		 			
				    $salt = $userpasssalt['r__password_salt'];
					$newpass = SHA1($password.$salt);
					
					$days = BackEnd_Helper_viewHelper::getDateDifference($username);
					if($days < $passwordExpirationTime){
					
					$data_adapter = new Auth_ClinictrialAdapter ( $username, $newpass );
					$auth = Zend_Auth::getInstance ();
					$result = $auth->authenticate ( $data_adapter );
					if (Auth_ClinictrialAdapter::hasIdentity ()) {
						
						$obj = Auth_ClinictrialAdapter::getIdentity ();
									
						$id = $obj[0]['u__id'];
						
						//salt updation using session id
							$new_salt = md5(round(microtime(true) * 1000));
							
							//updating salt
							$updatedsal = refusersecurity::updateSal($id,$new_salt);
							
							$updpwd = SHA1($password.$new_salt);
							User::updatePwd($id, $updpwd); 
	
							//for session logs
							$token = SHA1(round(microtime(true) * 1000));
							$ip_address = $_SERVER['REMOTE_ADDR'];
							$sessionObj = new sessionLogs();	
							$saveSession = $sessionObj->saveSessionDetails($id , $token , $ip_address); 
							$user = new Zend_Session_Namespace ('ClinicalTrial'); 
							$user->user_data = $obj;
							$user->token = $token;
							
							//automatic expiration
							$user->setExpirationSeconds ( $timeSeconds );
							$sessionNamespace = new Zend_Session_Namespace ();
							$referer = new Zend_Session_Namespace ( 'referer' );
							$namespace = new Zend_Session_Namespace ( 'Zend_Auth' );
							$namespace->setExpirationSeconds ( $timeSeconds );
							
							if (isset ( $referer->refer ) && $referer->refer != '') {
								
								$url = $referer->refer;
								
							} 
							else {
								
								$url = MODULE_CLINICTRIAL;
							}
							#flash message settings
							$flash = $this->_helper->getHelper('FlashMessenger');
							$message = $this->view->translate('Welcome to admin panel');
							$flash->addMessage(array('success' => $message ));
							// redirect to other page(user list)
							$this->_redirect ( rtrim ( $url, '?' ) );
						
						}	
						 else {
								
							$flash = $this->_helper->getHelper('FlashMessenger');
							$message = $this->view->translate('Please enter valid username/password');
							$flash->addMessage(array('error' => $message ));
							// redirect to same page but with msg parameter
							$this->_helper->redirector ( 'index', 'index', 'login');
						}
					}else{
							
							$url = MODULE_LOGIN."/index/password-expire/uname/".urlencode($orgUsername);
							$this->_redirect ( rtrim ( $url, '?' ) );
						}
			 	}else{
			 		$flash = $this->_helper->getHelper('FlashMessenger');
					$message = $this->view->translate('Please enter valid username/password');
					$flash->addMessage(array('error' => $message ));
					// redirect to same page but with msg parameter
					$this->_helper->redirector ( 'index', 'index', 'login');
			 	}
						
				 	break;
				
				case '4' : # subject
					
						if($studyId != 0){
							
							$params = $this->_request->getParams ();
							@$studyId = $params ['investigator_study'];
							$username = BackEnd_Helper_viewHelper::encryptString($username, $aes_Key);
							$userpasssalt = user::getSubjectPassSal($params,$aes_Key);
							$salt = $userpasssalt['r__password_salt'];
							$newpass = SHA1($password.$salt);
							
							$days = BackEnd_Helper_viewHelper::getDateDifference($id);
							if($days < $passwordExpirationTime){
							
							$data_adapter = new Auth_SubjectAdapter ( $username, $newpass );
	
							$auth = Zend_Auth::getInstance ();
							$result = $auth->authenticate ( $data_adapter );
							if (Auth_SubjectAdapter::hasIdentity ()) {
								$obj = Auth_SubjectAdapter::getIdentity ();
								$id = $obj['id'];
								
								//salt updation using session id
								$new_salt = md5(round(microtime(true) * 1000));
						
								//updating salt
								$updatedsal = refusersecurity::updateSal($id,$new_salt);
						
								$updpwd = SHA1($password.$new_salt);
								User::updatePwd($id, $updpwd);
						
								//for session logs
								$token = SHA1(round(microtime(true) * 1000));
								$ip_address = $_SERVER['REMOTE_ADDR'];
								$sessionObj = new sessionLogs();
								$saveSession = $sessionObj->saveSessionDetails($id , $token , $ip_address);
								$user = new Zend_Session_Namespace ('Subject');
								$user->user_data = $obj;
								$user->subject_token = $token;
								$user->subject_studyId = $studyId;
								$investigatorStudyInfo = studyrecord:: getInvestigatorStudyInfo($studyId, $aes_Key);
								$user->subjectStudyInfo = $investigatorStudyInfo;
								
								/*automatic expiration*/
									
								$user->setExpirationSeconds ( $timeSeconds );
								$sessionNamespace = new Zend_Session_Namespace ();
								$referer = new Zend_Session_Namespace ( 'referer' );
								$namespace = new Zend_Session_Namespace ( 'Zend_Auth' );
								$namespace->setExpirationSeconds ( $timeSeconds );
						
								if (isset ( $referer->refer ) && $referer->refer != '') {
										
									$url = $referer->refer;
										
								} else {
										
									$url = MODULE_SUBJECT;
								}
								#flash message settings
							$flash = $this->_helper->getHelper('FlashMessenger');
							$message = $this->view->translate('Welcome to subject dashboard');
							$flash->addMessage(array('success' => $message ));
												// redirect to other page(user list)
								$this->_redirect ( rtrim ( $url, '?' ) );
									
							}else {
								$flash = $this->_helper->getHelper('FlashMessenger');
								$message = $this->view->translate('Please enter valid username/password');
								$flash->addMessage(array('error' => $message ));
													// redirect to same page but with msg parameter
								$this->_helper->redirector ( 'index', 'index', 'login');
							}
						}else{
								$url = MODULE_LOGIN."/index/password-expire";
								$this->_redirect ( rtrim ( $url, '?' ) );
						}
					}else{
						
						$flash = $this->_helper->getHelper('FlashMessenger');
						$message = $this->view->translate('Please select a valid Study');
						$flash->addMessage(array('error' => $message ));
						// redirect to same page but with msg parameter
						$this->_helper->redirector ( 'index', 'index', 'login');
					}
						break;
										
				default :
					break;
				}
			}
		}
		
		
		
	/**
	 * To generate random string for salt for the password
	 *
	 * @author pkaur4
	 * @version 1.0
	 */
	function unique_string() {
		mt_srand(microtime(true)*100000 + memory_get_usage(true));
		return md5(uniqid(mt_rand(), true));
	}
	
	/**
	 * To get the list of studies of an Investigator
	 * 
	 * @author Raman
	 * @version 1.0
	 */
	public function authenticateAction() {
		
		// get all the parameters from get/post
		$params = $this->_request->getParams ();
		// check post form or not
		if ($this->getRequest ()->isPost ()) {
			// then unset the msg session
			$username = $params ['username'];
			$password = $params ['password'];
			// set authentication if Investigator valid
			$data_adapter = new Auth_InvestigatorAdapter ( $username, $password );
			$auth = Zend_Auth::getInstance ();
			$result = $auth->authenticate ( $data_adapter );
			
			if (Auth_InvestigatorAdapter::hasIdentity ()) {
				// create object of Investigator class
				$investigatorId = Auth_InvestigatorAdapter::getIdentity ()->id;
				$studies = new Investigator ();
				$Obj = Doctrine_Core::getTable ( 'Investigator' )->findOneBy ( 'id', Auth_InvestigatorAdapter::getIdentity ()->id );
			
			} else {
				
				echo false;
			}
		}
	}
	
	/**
	 * getsubjectAction
	 * 
	 * get subject of the investigator
	 *
	 * @author Raman
	 * @version 1.0
	 */
	function getstudyAction(){
		$this->_helper->layout()->disableLayout();
		Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		
		$params = $this->_request->getParams();
		if(isset($params['username']) && isset($params['password'])){
			$username = BackEnd_Helper_viewHelper::encryptString($params['username'], $aes_Key);
			$userpasssalt = user::getPassSalUsername($username);
			$salt = $userpasssalt['r__password_salt'];
			$newpass = SHA1($params['password'].$salt);
			$role = $userpasssalt['u__roleId'];
			$data = investigator::getStudiesByInvestigator($username, $newpass, $role, $aes_Key);
		}else{
			$data = $this->view->translate('Record not found');
		}
		echo Zend_Json::encode($data);
		die;
	}
	
	/**
	 * getsubjectAction
	 *
	 * get subject of the investigator
	 *
	 * @author Raman
	 * @version 1.0
	 */
	function getsubjectstudyAction(){
		$this->_helper->layout()->disableLayout();
		Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);
		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
	
		$params = $this->_request->getParams();
		if(isset($params['username']) && isset($params['password'])){
			$username = BackEnd_Helper_viewHelper::encryptString($params['username'], $aes_Key);
			$userpasssalt = user::getPassSalUsername($username);
			$salt = $userpasssalt['r__password_salt'];
			$newpass = SHA1($params['password'].$salt);
			$role = $userpasssalt['u__roleId'];
			$data = user::getStudiesBySubject($username, $newpass, $role, $aes_Key);

		}else{
			$data = $this->view->translate('Record not found');
		}
		echo Zend_Json::encode($data);
		die;
	}
	
	/**
	 * logout
	 * 
	 * unset all cookies and session of the logged user
	 *
	 * @author Raman
	 * @version 1.0
	 */
    public function logoutAction() {
		//unset the session		
		$user = new Zend_Session_Namespace ('ClinicalTrial');
		$sessionObj = new sessionLogs();
		$sessionObj->updateSessionDetails($user->token);
		
		Auth_ClinictrialAdapter::clearIdentity();
		
		$this->_helper->redirector('index','index','login');
	}
	
	/**
	 * logoutinvestigator
	 * 
	 * unset all cookies and session of the logged user
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function logoutinvestigatorAction() {
		//unset the session		
		$user = new Zend_Session_Namespace ('InvestigatorInfo');
		$sessionObj = new sessionLogs();
		$sessionObj->updateSessionDetails($user->investigator_token);
		
		Auth_InvestigatorAdapter::clearIdentity();
		
		$this->_helper->redirector('index','index','login');
	
	}
	
	/**
	 * logoutsubject
	 *
	 * unset all cookies and session of the logged user
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function logoutsubjectAction() {
		//unset the session
		$user = new Zend_Session_Namespace ('Subject');
		$sessionObj = new sessionLogs();
		$sessionObj->updateSessionDetails($user->subject_token);
	
		Auth_SubjectAdapter::clearIdentity();
	
		$this->_helper->redirector('index','index','login');
	
	}
		/**
		 * forgotpassword
		 * 
		 * forget password by email
		 *
		 * @author Raman
		 * @version 1.0
		 */
		public function forgotpasswordAction() {
			
			$forgotPassword = $this->view->translate('Forgot Password');
			$this->view->headTitle($forgotPassword);
		}
		
		/**
		* forgotpwd
		* 
		* send mail to user for reset password
		*
		* @author Raman updated by pkaur4
		* @version 1.0
		*/
		public function forgotpwdAction() {
			
			$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
			//echo $aes_Key; die;
			$this->_helper->layout()->disableLayout();
			Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);
			$params = $this->_request->getParams();
			
			if ($this->getRequest()->isPost()) {
				$email = BackEnd_Helper_viewHelper::encryptString($params["email"] , $aes_Key);
				$result = Auth_ClinictrialAdapter::forgotPassword($email);
				$email = BackEnd_Helper_viewHelper::decryptString($result["email"] , $aes_Key);
				$flash = $this->_helper->getHelper ( 'FlashMessenger' );
				if ($result == true) {
					$userName = BackEnd_Helper_viewHelper::decryptString($result['firstname'], $aes_Key);
					$resultuserid = base64_encode($result["id"]);
					$url = MODULE_LOGIN .'/index/resetpassword/forgotid/' . $resultuserid;
					$subject  = $this->view->translate('Forgot Password');
					
					//echo $message = $this->view->translate('Dear '). $this->view->translate($userName).','. "<br/>".$this->view->translate('Please click on this')." <a href='$url'  >".$this->view->translate('Set Password')."</a>".$this->view->translate(' link to set the password.');
					
					$message = $this->view->translate('Dear '). $this->view->translate(ucfirst($userName)).','. "<br/><br/>".$this->view->translate('Please click on this')." <a href='$url'  >".$this->view->translate('Set Password')."</a>".$this->view->translate(' link to set the password.')."<br/><br/>".$this->view->translate('Thank You').",<br/>".$this->view->translate('Support Team');
					
					BackEnd_Helper_viewHelper::SendMail($email, $subject, $message, 'support@clinicalTrial.com');
					
					$message =  $this->view->translate('Email has been sent to your email address. Please check to create your new password.') ;
					$flash->addMessage ( array ('success' => $message ) );
					$this->_helper->redirector('index','index','login');
					
				} else {
					$message =  $this->view->translate('Email-id does not exist in database.');
					$flash->addMessage ( array ('error' => $message ) );
					$this->_helper->redirector ( 'forgotpassword', 'index', 'login' );
				}
			  }
		}
		
		/**
		 * resetpassword
		 * 
		 * reset password by email link
		 *
		 * @author Raman
		 * @version 1.0
		 */
		 public function resetpasswordAction() {
			$resetpass = $this->view->translate('Reset Password');
			$this->view->headTitle($resetpass);
			$params = $this->_request->getParams();
			$user_id = @(base64_decode($params["forgotid"]));
			$this->view->forgotid = $user_id;
		}
		/**
		 * resetpwd
		 *
		 * set new password in database
		 *
		 * @author Raman
		 * @version 1.0
		 */
		public function resetpwdAction() {
			$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
			$reset_pass = $this->view->translate('Reset Password');
			$this->view->headTitle($reset_pass);
			$params = $this->_request->getParams();
			$user_id = $params["forgotid"];
			
			if ($this->getRequest()->isPost()) {
				$newpwd = $params["password"];
				$salt = md5(round(microtime(true) * 1000));
				$newpassword = SHA1($newpwd.$salt);
				
				$updatedsal = refusersecurity::updateSal($user_id,$salt);
				$res = user::updatePassword($user_id, $newpassword);
				if ($res) {
					$this->_helper->redirector('index','index','login');
				} else {
					$this->_helper->redirector('index','index','login');
				}
			}
		}
		
		public function createInvestigatorAccountAction(){
			$new_inves = $this->view->translate('New Investigator');
			$this->view->headTitle($new_inves);
			$params = $this->_request->getParams();
			$user_id = @(base64_decode($params["id"]));
			$this->view->id = $user_id;
		}
		
		public function createSubjectAccountAction(){
			$new_sub = $this->view->translate('New Subject');
			$this->view->headTitle($new_sub);
			$params = $this->_request->getParams();
			$user_id = @(base64_decode($params["id"]));
			$this->view->id = $user_id;
		}
		 
		public function uniqueUserAction(){
			
			$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
			$this->_helper->layout ()->disableLayout ();
			$this->_helper->viewRenderer->setNoRender ( true );
			$params = $this->_getAllParams();

			$uname = $params['username'];
			$uname = BackEnd_Helper_viewHelper::encryptString($uname, $aes_Key);
			$data = investigator::checkInvestigatorUserName($uname);
			echo json_encode($data);
		}
		 
		public function createNewInvestigatorAction(){
			$this->_helper->layout ()->disableLayout ();
			$this->_helper->viewRenderer->setNoRender ( true );
			$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
			if ($this->getRequest ()->isPost ()) {
				$params = $this->_getAllParams();
				$username = BackEnd_Helper_viewHelper::encryptString($params['username'], $aes_Key);
				$active = BackEnd_Helper_viewHelper::encryptString(1, $aes_Key);
				$password = $params['password'];
				$id = $params['id'];
			}
			//salt updation using session id
			$new_salt = md5(round(microtime(true) * 1000));
			//updating salt
			$updatedsal = refusersecurity::setSaltValue($id,$new_salt,$aes_Key);

			$newpassword = SHA1($password.$new_salt);
			$data = investigator::setUserNameAndPassword($id , $username , $newpassword, $active);
			if($data){
				$flash = $this->_helper->getHelper('FlashMessenger');
				$message = $this->view->translate('Your username and password has been updated successfully.Please login to continue');
				$flash->addMessage(array('success' => $message ));
				$this->_helper->redirector('index','index','login');
			}
		}
		
	public function passwordExpireAction(){
		$param = $this->_getAllParams();
		$username = @$param['uname'];
		$this->view->username = $username;
	}
	
	public function updatePasswordAction(){

		$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
		$this->_helper->layout()->disableLayout();
		Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);
			
		/*$data = Auth_ClinictrialAdapter::getIdentity();
		$email = $data[0]['u__email'];*/
		
		$param = $this->_getAllParams();
		$orgUsername = $param['uname_hidden'];
		$username = BackEnd_Helper_viewHelper::encryptString($param['uname_hidden'], $aes_Key);

		$userpasssalt = user::passwordSalt($username);
		$salt = $userpasssalt['r__password_salt'];

		$oldpass = $param['oldpassword'];
		$oldpassword = SHA1($oldpass.$salt);
		$checkUser = user::checkUserPassword($oldpassword , $username);
		$error = 0;
		
		$newpass = $param['newpassword'];
	
		$new_salt = md5(round(microtime(true) * 1000));
		$new_password = SHA1($newpass.$new_salt);
		if($checkUser){
			$updatesalt = refusersecurity::updateSalValue($username , $new_salt);
			$error = user::updateNewPassword($username , $new_password);
		}
		
		if($error == 0){
			$flash = $this->_helper->getHelper('FlashMessenger');
			$message = $this->view->translate('Your old password does not matches with our record');
			$flash->addMessage(array('error' => $message ));
			$this->_helper->redirector('password-expire','index','login',array('uname'=>$orgUsername));
		
		}else{
		
			$flash = $this->_helper->getHelper('FlashMessenger');
			$message = $this->view->translate('Password has been changed successfully. Login to continue');
			$flash->addMessage(array('success' => $message ));
			$this->_helper->redirector('index','index','login');
		}
	}
}