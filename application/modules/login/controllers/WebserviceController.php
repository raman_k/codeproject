<?php

class Login_WebserviceController extends Zend_Controller_Action {

	public function init() {

		/*
		 * Initialize action controller here
		 * 
		 */
		$this->_helper->layout()->disableLayout();
		Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);
		
		
	}
	/*
	 * Initialize action controller here
	*
	*/
	public function preDispatch(){
		
		
	}
	
	public function indexAction() {
		
		$params=$this->_request->getParams();
		
	}
	
	/**
	 * checkloginAction
	 * 
	 * checks the authentication of a user
	 * 
	 * @author Raman
	 * @version 1.0
	 */
	
	public function checkloginAction()
	{
		$handle = fopen('php://input','r');
		$jsonInput = fgets($handle);
		//$jsonInput = '{"device_id": "mT27HOeG/zP4L+/zJ7kbWw==","password": "HKu9ings1HpJJe1TcBXXMA==","username": "M6xr7q0LtUC3/nPxkJ8LZg=="}';
		$params = Zend_Json::decode($jsonInput);
		$time_to_expire = Zend_Registry::getInstance()->constants->WS_TIME_TO_EXPIRE;

		if(isset($params['username']) && @$params['username'] !="" && isset($params['password'])&& @$params['password'] != "" && isset($params['device_id'])&& @$params['device_id'] != ""){
		
				
			//generate session id for investigator 
			$sessionId = SHA1(round(microtime(true) * 1000));
			$keyValue = Zend_Registry::getInstance()->constants->PRIVATE_KEY_WS;
			$params = BackEnd_Helper_viewHelper::decryptStrArray($params, $keyValue);
			$deviceId = $params ['device_id'];
			
			//Encrypt data to compare with database
			$keyValueForStorage = Zend_Registry::getInstance()->constants->AES_KEY;
			$username = BackEnd_Helper_viewHelper::encryptString($params ['username'], $keyValueForStorage); 
			
			$password = $params ['password'];
			$device_id  = $params ['device_id'];
			
			$comingDeviceId = $deviceId;
			$comingDeviceIdEnc = BackEnd_Helper_viewHelper::encryptString($comingDeviceId, $keyValueForStorage);
			
			
			//get salt on username basis
			$userpasssalt = user::getInvestigatorPassSal($username);
			
			$salt = $userpasssalt['r__password_salt'];
			$newpass = SHA1($password.$salt);
			$data_adapter = new Auth_InvestigatorAdapter($username, $newpass, $keyValueForStorage);
							
			$auth = Zend_Auth::getInstance ();
			$result = $auth->authenticate ( $data_adapter );
			
			//Authenticate user
			if (Auth_InvestigatorAdapter::hasIdentity ()) {
				
				//create object of Investigator class
				$user = Auth_InvestigatorAdapter::getIdentity();
				$userId = $user[0]['u__id'];
				
				//get device details
				$isDeviceInvRegistered = user::checkUserDeviceId($username, $comingDeviceIdEnc);
				
				//generate session specific key at run time to decrypt data
				$firstPart = substr($deviceId, 0, 6);
				$secondPart = substr($keyValue, 0, 6);
				$thirdPart = substr($sessionId, 0, 4);
				$keyValueToDecrypt = $firstPart.$secondPart.$thirdPart;
				
				//update session logs for device
				$sessionObj = new sessionLogs();
				$saveSession = $sessionObj->saveDeviceSessionDetails($userId , $sessionId , $comingDeviceIdEnc);
				
				
				if($isDeviceInvRegistered){
					
					//update investigator and device session 
					User::updateDeviceSession($userId, $comingDeviceIdEnc, $sessionId);
					
					//update salt and password
					$result = self::updateSaltAndPassword($password, $userId, $user, $sessionId);
					if($result['Status']== 200){
						
						$result = BackEnd_Helper_viewHelper::encryptStrArray($result, $keyValueToDecrypt);
						$result['sessionId'] = $sessionId;
					}
				
				} else {
					
					//register device session
					User::insertDeviceSession($userId, $comingDeviceIdEnc, $sessionId);
					//check device session
					//$checkIfDeviceExist = User::checkDeviceSession($userId, $sessionId);
						
					//salt and password updation
					$result = self::updateSaltAndPassword($password, $userId, $user, $sessionId);
					
					if($result['Status']== 200){
						$result = BackEnd_Helper_viewHelper::encryptStrArray($result, $keyValueToDecrypt);
						$result['sessionId'] = $sessionId;
					}
						
				}
				
							
				
						
			} else {
				#flash error
				$result = array("Status"=>302,"Error"=>"Unauthorized user!");
			}

	
		}else{
	
			$result = array("Status"=>301,"Error"=>"Missing username or password or device id");
		}
		
		
		$data = Zend_Json::encode($result);
		echo $data;
		exit();
	
	}
	
	/**
	 * getconfigurestudydetailsAction
	 * 
	 * get the details of the configured study
	 * 
	 * @author Raman
	 * @version 1.0
	 */
	
	public function getconfigurestudydetailsAction()
	{
	
		$handle = fopen('php://input','r');
		$jsonInput = fgets($handle);
		$params = Zend_Json::decode($jsonInput);
		$time_to_expire = Zend_Registry::getInstance()->constants->WS_TIME_TO_EXPIRE;
		$keyValue = Zend_Registry::getInstance()->constants->PRIVATE_KEY_WS;
		$keyValueForStorage = Zend_Registry::getInstance()->constants->AES_KEY;
		$statusOne = BackEnd_Helper_viewHelper::encryptString(1, $keyValueForStorage);
		
		if(isset($params['sessionId']) && @$params['sessionId'] !="" && isset($params['studyId'])&& @$params['studyId'] != ""){
		
				
			//generate session id for investigator 
			$sessionId = $params['sessionId'];
			$study_id = $params['studyId'];
			$sessionDetails = user::checkSession($sessionId);
			
			if(!empty($sessionDetails)){
				
				$userId = $sessionDetails[0]['user_id'];
				$userDetails = user::getUserDetails($userId);
				$role = $userDetails['role_id'];
				
				$device_id = $sessionDetails[0]['deviceID'];
				$deviceId = BackEnd_Helper_viewHelper::decryptString($device_id, $keyValueForStorage);
				
				$firstPart = substr($deviceId, 0, 6);
				$secondPart = substr($keyValue, 0, 6);
				$thirdPart = substr($sessionId, 0, 4);
				
				$keyValueToDecrypt = $firstPart.$secondPart.$thirdPart;
							
				$studyId = BackEnd_Helper_viewHelper::decryptString($study_id, $keyValueToDecrypt);
				if($role == 2){
				  $getConfigId = studyConfiguration :: getStudyConfigId($userId, $studyId);
				}else {
				  $getConfigId = studyConfiguration :: getStudyConfigIdSubInvestigator($userId);
				}
				
				if(isset($getConfigId) && $getConfigId != ''){
				$checkStudyCompletion = BackEnd_Helper_viewHelper :: checkStudyCompletion($getConfigId);
				if($checkStudyCompletion == 1){

				//Get configured study details from the database on userId and studyId basis
				$encStudyData = studyrecord::getConfiguredStudyData($userId, $studyId, $role, $statusOne, $keyValueForStorage);
				
				//decrypt data from the database
				$studyData = BackEnd_Helper_viewHelper::decryptWsArray($encStudyData, $keyValueForStorage);
				
				$rawData = array("Status"=>200,"message"=>"success","userId"=>$userId, "Role"=>$role, "ConfiguredStudyDetails"=>$studyData);
				
				//Encrypt data session basis
				
				$result = BackEnd_Helper_viewHelper::encryptStrArray($rawData, $keyValueToDecrypt);
				$result['sessionId'] = $sessionId;
				}else {
					$result = array("Status"=>403,"Error"=>"Record not found");
				}
				}else {
					$result = array("Status"=>502,"Error"=>"Study is not configured");
				}
			} else {
				
				#flash error
				$result = array("Status"=>302,"Error"=>"Unauthorized Session!");
			}
			
		}else{
	
			$result = array("Status"=>301,"Error"=>"Missing sessionId or studyId");
		}
		
		
		$data = Zend_Json::encode($result);
		echo $data;
		exit();
	
	}
	/**
	 * setSubjectData
	 *
	 * store subject information in database
	 *
	 * @author vishal
	 * @version 1.0
	 */
	
	public function setSubjectDataAction()
	{
		$handle = fopen('php://input','r');
		$jsonInput = fgets($handle);		
		$params = Zend_Json::decode($jsonInput);
		$time_to_expire = Zend_Registry::getInstance()->constants->WS_TIME_TO_EXPIRE;
		$keyValue = Zend_Registry::getInstance()->constants->PRIVATE_KEY_WS;
		$keyValueForStorage = Zend_Registry::getInstance()->constants->AES_KEY;

		//database credentials for saving signatures
		$host = Zend_Registry::getInstance()->resources->db->params->host;
		$dbName= Zend_Registry::getInstance()->resources->db->params->dbname;
		$username = Zend_Registry::getInstance()->resources->db->params->username;
		$pass = Zend_Registry::getInstance()->resources->db->params->password;
		
		if(isset($params['sessionId']) && $params['sessionId'] !="" && isset($params['subjectId'])&& $params['subjectId'] != "" && isset($params['study_configId'])&& $params['study_configId'] != ""){
	    
			//generate session id for investigator
			$sessionId = $params['sessionId'];			
			$subjectId = $params['subjectId'];
			$studyConfigId = $params['study_configId'];
			$firstName = $params['firstname'];
			$lastName = $params['lastname'];
			$dob = $params['dob'];
			
			$sessionDetails = user::checkSession($sessionId);
				
			if(!empty($sessionDetails)){
	
				$userId = $sessionDetails[0]['user_id'];
				$userDetails = user::getUserDetails($userId);
				$role = $userDetails['role_id'];
	
				$device_id = $sessionDetails[0]['deviceID'];
				$deviceId = BackEnd_Helper_viewHelper::decryptString($device_id, $keyValueForStorage);
	
				$firstPart = substr($deviceId, 0, 6);
				$secondPart = substr($keyValue, 0, 6);
				$thirdPart = substr($sessionId, 0, 4);
	
				$keyValueToDecrypt = $firstPart.$secondPart.$thirdPart;
				
				if(isset($params['email']) && @$params['email'] !=""){
					
					$email = $params['email'];
					$email1 = BackEnd_Helper_viewHelper::decryptString($email, $keyValueToDecrypt);
					$email = BackEnd_Helper_viewHelper::encryptString($email1, $keyValueForStorage);
					
				} else {
					$email = '';
				}
			    
				// Dycript data with global key
				$subjectId = BackEnd_Helper_viewHelper::decryptString($subjectId, $keyValueToDecrypt);
				$studyConfigId = BackEnd_Helper_viewHelper::decryptString($studyConfigId, $keyValueToDecrypt);
				$firstName1 = BackEnd_Helper_viewHelper::decryptString($firstName, $keyValueToDecrypt);
				$lastName= BackEnd_Helper_viewHelper::decryptString($lastName, $keyValueToDecrypt);
				$dob= BackEnd_Helper_viewHelper::decryptString($dob, $keyValueToDecrypt);
				
				$subjectIdIPAD = $subjectId;
				
				// Encrypt data with AES key
				$subjectId = BackEnd_Helper_viewHelper::encryptString($subjectId, $keyValueForStorage);
				$firstName = BackEnd_Helper_viewHelper::encryptString($firstName1, $keyValueForStorage);
				$lastName = BackEnd_Helper_viewHelper::encryptString($lastName, $keyValueForStorage);
				$dob = BackEnd_Helper_viewHelper::encryptString($dob, $keyValueForStorage);
				
				    //check if subject already exist or not
                	$checkExistingSubject = user::checkExistingSubject($subjectId);
                	
                	
                	
                	if(isset($checkExistingSubject['id']) &&  $checkExistingSubject['id'] != ''){
                		//get subject common data id
                		$SubjectCommonDataId = user::getSubjectCommonDataId($checkExistingSubject['id']);
                		
                		//update user basic information
                		$firstName = (isset($params['firstname'])) ? $firstName : '';
                		$lastName = (isset($params['lastname'])) ? $lastName : '';
                		$dob = (isset($params['dob'])) ? $dob : '';
                		$updateUserDetails = user::updateSubjectBasicInfo($email, $subjectId, $firstName, $lastName, $dob);
                
                		// update subject signature Data
                		$investigatorSig = (isset($params['investigator_sig'])) ? $params['investigator_sig'] : '';
                		$subjectSig = (isset($params['subject_sig'])) ? $params['subject_sig'] : '';                		
                		$siteId = (isset($params['sitId'])) ? BackEnd_Helper_viewHelper::decryptString($params['sitId'], $keyValueToDecrypt) : '';
                		$groupId = (isset($params['groupId'])) ? BackEnd_Helper_viewHelper::decryptString($params['groupId'], $keyValueToDecrypt) : '';
                		$subStatus = (isset($params['subStatus'])) ? BackEnd_Helper_viewHelper::decryptString($params['subStatus'], $keyValueToDecrypt) : '';
                		$subStatus = (isset($params['subStatus'])) ? BackEnd_Helper_viewHelper::encryptString($subStatus, $keyValueForStorage) : '';
                		$statusDate = (isset($params['statusDate'])) ? $params['statusDate'] : '';
                		
                		$updateUserSecureDetails = user::saveSubjectSignatureData($investigatorSig, $subjectSig, $siteId, $groupId, $subStatus, $statusDate, $studyConfigId, $checkExistingSubject['id'], $host, $dbName, $username, $pass);
                		
                		
                		// store subject form data
                		foreach($params['form'] as $data){
                			$formId = BackEnd_Helper_viewHelper::decryptString($data['formId'], $keyValueToDecrypt);
                			$visitId = BackEnd_Helper_viewHelper::decryptString($data['visitId'], $keyValueToDecrypt);
                			foreach($data['form_variable'] as $innerData ){
                				 
                				$varId = BackEnd_Helper_viewHelper::decryptString($innerData['varId'], $keyValueToDecrypt);
                				$varVal = BackEnd_Helper_viewHelper::decryptString($innerData['varVal'], $keyValueToDecrypt);
                				$varVal = BackEnd_Helper_viewHelper::encryptString($varVal, $keyValueForStorage);
                				$fieldStatusId = BackEnd_Helper_viewHelper::decryptString($innerData['fieldStatus'], $keyValueToDecrypt);
                				
                				$checkVariableExistence = forms:: checkIfVariableExist($varId, $studyConfigId, $formId, $SubjectCommonDataId);
                				if($checkVariableExistence){
                					$saveSubjectFormData[] = forms::updateSubjectFormData($studyConfigId, $formId, $SubjectCommonDataId, $varId, $varVal, $fieldStatusId, $userId, $visitId);
                				}else{
                					$saveSubjectFormData[] = forms::saveSubjectFormData($studyConfigId, $formId, $SubjectCommonDataId, $varId, $varVal, $fieldStatusId, $userId, $visitId);
                				}
                			}
                			$newArr['variables'] = $saveSubjectFormData;
                			$newArr['formId'] = $formId;
                		}
                		$rawData = array("Status"=>200,"message"=>"success","studyConfigId"=>$studyConfigId, "subjectId"=>$subjectIdIPAD, "formVariableRecords" => $newArr);
                		
                		// Store subject Audit form data
                		$newArrAudit = array();
                		foreach($params['audit'] as $data){
                			$formId = BackEnd_Helper_viewHelper::decryptString($data['formId'], $keyValueToDecrypt);
                			$visitId = BackEnd_Helper_viewHelper::decryptString($data['visitId'], $keyValueToDecrypt);
                			foreach($data['form_variable_audit'] as $innerData ){
                		
                				$varId = BackEnd_Helper_viewHelper::decryptString($innerData['varId'], $keyValueToDecrypt);
                				$oldVal = BackEnd_Helper_viewHelper::decryptString($innerData['oldVal'], $keyValueToDecrypt);
                				$newVal = BackEnd_Helper_viewHelper::decryptString($innerData['newVal'], $keyValueToDecrypt);
                				$reasonId = BackEnd_Helper_viewHelper::decryptString($innerData['reasonId'], $keyValueToDecrypt);
                				$comment = BackEnd_Helper_viewHelper::decryptString($innerData['comment'], $keyValueToDecrypt);
                				$deviceId = BackEnd_Helper_viewHelper::decryptString($innerData['deviceId'], $keyValueToDecrypt);
                				 
                				 
                				$oldVal = BackEnd_Helper_viewHelper::encryptString($oldVal, $keyValueForStorage);
                				$newVal = BackEnd_Helper_viewHelper::encryptString($newVal, $keyValueForStorage);
                				$comment = BackEnd_Helper_viewHelper::encryptString($comment, $keyValueForStorage);
                				$deviceId = BackEnd_Helper_viewHelper::encryptString($deviceId, $keyValueForStorage);
                				 
                				$checkVariableExistence = forms:: checkIfVariableExistAudit($varId, $studyConfigId, $formId, $SubjectCommonDataId);
                				if($checkVariableExistence){
                					$saveSubjectFormDataAudit[] = forms::updateSubjectFormDataAudit($studyConfigId, $formId, $SubjectCommonDataId, $varId, $oldVal, $newVal, $reasonId, $comment, $deviceId, $userId, $visitId);
                				}else{
                					$saveSubjectFormDataAudit[] = forms::saveSubjectFormDataAudit($studyConfigId, $formId, $SubjectCommonDataId, $varId, $oldVal, $newVal, $reasonId, $comment, $deviceId, $userId, $visitId);
                				}
                			}
                			$newArrAudit['variablesAudit'] = $saveSubjectFormDataAudit;
                		}
                		$rawDataAudit = array("Status"=>200,"message"=>"success","studyConfigId"=>$studyConfigId, "subjectId"=>$subjectIdIPAD, "AuditformVariableRecords" => $newArrAudit);
                		 
                		
                	}else {
                	
					//store subject basis information
					$saveUserDetails = user::saveSubjectBasicInfo($email, $subjectId, $userId, $firstName, $lastName, $dob, $keyValueForStorage);
		            if($saveUserDetails >0){
		            	$newArr = array();
		            	
		            	// store subject commmon information
		            	$saveCommonData = user::saveSubjectCommonData($studyConfigId, $saveUserDetails);
		            	
		            	// store subject signature Data
		            	$investigatorSig = (isset($params['investigator_sig'])) ? $params['investigator_sig'] : '';
                		$subjectSig = (isset($params['subject_sig'])) ? $params['subject_sig'] : '';		           		$siteId = (isset($params['sitId'])) ? BackEnd_Helper_viewHelper::decryptString($params['sitId'], $keyValueToDecrypt) : '';
                		$groupId = (isset($params['groupId'])) ? BackEnd_Helper_viewHelper::decryptString($params['groupId'], $keyValueToDecrypt) : '';
                		$subStatus = (isset($params['subStatus'])) ? BackEnd_Helper_viewHelper::decryptString($params['subStatus'], $keyValueToDecrypt) : '';
                		$subStatus = (isset($params['subStatus'])) ? BackEnd_Helper_viewHelper::encryptString($subStatus, $keyValueForStorage) : '';
                		$statusDate = (isset($params['statusDate'])) ? $params['statusDate'] : '';
                		
                        $saveUserSecureDetails = user::saveSubjectSignatureData($investigatorSig, $subjectSig, $siteId, $groupId, $subStatus, $statusDate, $studyConfigId, $saveUserDetails, $host, $dbName, $username, $pass);
		            	
		            	// store subject form data
		            	foreach($params['form'] as $data){
		            		$formId = BackEnd_Helper_viewHelper::decryptString($data['formId'], $keyValueToDecrypt);
		            		$visitId = BackEnd_Helper_viewHelper::decryptString($data['visitId'], $keyValueToDecrypt);
		            		foreach($data['form_variable'] as $innerData ){
		            	      
		            		  $varId = BackEnd_Helper_viewHelper::decryptString($innerData['varId'], $keyValueToDecrypt);
		            		  $varVal = BackEnd_Helper_viewHelper::decryptString($innerData['varVal'], $keyValueToDecrypt);
		            		  $varVal = BackEnd_Helper_viewHelper::encryptString($varVal, $keyValueForStorage);
		            		  $fieldStatusId = BackEnd_Helper_viewHelper::decryptString($innerData['fieldStatus'], $keyValueToDecrypt);
		            		  
		            		  $checkVariableExistence = forms:: checkIfVariableExist($varId, $studyConfigId, $formId, $saveCommonData);
		            		  if($checkVariableExistence){
		            		  	$saveSubjectFormData[] = forms::updateSubjectFormData($studyConfigId, $formId, $saveCommonData, $varId, $varVal, $fieldStatusId, $userId, $visitId);
		            		  }else{
		            		    $saveSubjectFormData[] = forms::saveSubjectFormData($studyConfigId, $formId, $saveCommonData, $varId, $varVal, $fieldStatusId, $userId, $visitId);  
		            		  }
		            		}
		            		$newArr['variables'] = $saveSubjectFormData;
		            		$newArr['formId'] = $formId;
		                }
		                $rawData = array("Status"=>200,"message"=>"success","studyConfigId"=>$studyConfigId, "subjectId"=>$subjectIdIPAD, "formVariableRecords" => $newArr);
		                
		                // Mail Code Start
		                $id = base64_encode($saveUserDetails);
		                $url = MODULE_LOGIN .'/index/create-subject-account/id/' . $id;
		                $subject  = $this->view->translate('New Investigator');
		                $emailId['email'] = $email1;
		                                
		                $message = "Hi ".ucfirst($firstName1).",<br/><br/>".$this->view->translate('Please follow the instructions below to activate your account')."</a><br/><br/>".$this->view->translate('Thank You').",<br/>".$this->view->translate('Support Team')."<br/><br/>".$this->view->translate('Account activation link  ')."<a href='$url'  >".$this->view->translate('Click here to set your username and password')."</a> .";
		                
		                BackEnd_Helper_viewHelper::SendMail($emailId, $subject, $message, 'support@cliniops.com');
		                // Mail Code End
		                
		                // Store subject Audit form data
		                $newArrAudit = array();
		                foreach($params['audit'] as $data){
		                	$formId = BackEnd_Helper_viewHelper::decryptString($data['formId'], $keyValueToDecrypt);
		                	$visitId = BackEnd_Helper_viewHelper::decryptString($data['visitId'], $keyValueToDecrypt);
		                	foreach($data['form_variable_audit'] as $innerData ){
		                
		                		$varId = BackEnd_Helper_viewHelper::decryptString($innerData['varId'], $keyValueToDecrypt);
		                		$oldVal = BackEnd_Helper_viewHelper::decryptString($innerData['oldVal'], $keyValueToDecrypt);
		                		$newVal = BackEnd_Helper_viewHelper::decryptString($innerData['newVal'], $keyValueToDecrypt);
		                		$reasonId = BackEnd_Helper_viewHelper::decryptString($innerData['reasonId'], $keyValueToDecrypt);
		                		$comment = BackEnd_Helper_viewHelper::decryptString($innerData['comment'], $keyValueToDecrypt);
		                		$deviceId = BackEnd_Helper_viewHelper::decryptString($innerData['deviceId'], $keyValueToDecrypt);
		                		 
		                		 
		                		$oldVal = BackEnd_Helper_viewHelper::encryptString($oldVal, $keyValueForStorage);
		                		$newVal = BackEnd_Helper_viewHelper::encryptString($newVal, $keyValueForStorage);
		                		$comment = BackEnd_Helper_viewHelper::encryptString($comment, $keyValueForStorage);
		                		$deviceId = BackEnd_Helper_viewHelper::encryptString($deviceId, $keyValueForStorage);
		                		 
		                		$checkVariableExistence = forms:: checkIfVariableExistAudit($varId, $studyConfigId, $formId, $saveCommonData);
		                		if($checkVariableExistence){
		                			$saveSubjectFormDataAudit[] = forms::updateSubjectFormDataAudit($studyConfigId, $formId, $saveCommonData, $varId, $oldVal, $newVal, $reasonId, $comment, $deviceId, $userId, $visitId);
		                		}else{
		                			$saveSubjectFormDataAudit[] = forms::saveSubjectFormDataAudit($studyConfigId, $formId, $saveCommonData, $varId, $oldVal, $newVal, $reasonId, $comment, $deviceId, $userId, $visitId);
		                		}
		                	}
		                	$newArrAudit['variablesAudit'] = $saveSubjectFormDataAudit;
		                }
		                $rawDataAudit = array("Status"=>200,"message"=>"success","studyConfigId"=>$studyConfigId, "subjectId"=>$subjectIdIPAD, "AuditformVariableRecords" => $newArrAudit);
		                 
		            }else {
		            	$rawData = array("Status"=>303,"message"=>"failure");
		            }
		            				
                	}
                	
				//Encrypt data session basis
	
				$result = BackEnd_Helper_viewHelper::encryptStrArray($rawData, $keyValueToDecrypt);
				$result['audit_record'] = BackEnd_Helper_viewHelper::encryptStrArray(@$rawDataAudit, $keyValueToDecrypt);
				$result['sessionId'] = $sessionId;
	
			} else {
	
				#flash error
				$result = array("Status"=>302,"Error"=>"Unauthorized Session!");
			}
					
			}else{
	
			$result = array("Status"=>301,"Error"=>"Missing sessionId or email or subjectId ");
		}
	
					$data = Zend_Json::encode($result);
					echo $data;
					exit();
	
	}
	
	/**
	 * forgotpasswordAction
	 *
	 * send mail to user for reseting password
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function forgotpasswordAction() {
		
		$handle = fopen('php://input','r') ;		
		$jsonInput = fgets($handle);
		$params = Zend_Json::decode($jsonInput);
		
		if(isset($params['email']) && $params['email'] != ""){
			$aes_Key = Zend_Registry::getInstance()->constants->AES_KEY;
			$privateKey = Zend_Registry::getInstance()->constants->PRIVATE_KEY_WS;
			$decryptedEmail = BackEnd_Helper_viewHelper::decryptString($params["email"] , $privateKey);
			$encryptedEmail = BackEnd_Helper_viewHelper::encryptString($decryptedEmail , $aes_Key);
			$result = Auth_ClinictrialAdapter::forgotPassword($encryptedEmail);
			$name = BackEnd_Helper_viewHelper::decryptString($result['firstname'] , $aes_Key);
			$email['email'] = $decryptedEmail;
			if (!empty($result)) {
				$resultuserid = base64_encode($result["id"]);
				$url = MODULE_LOGIN .'/index/resetpassword/forgotid/' . $resultuserid;
				$fromEmail = "info@clinicops.com";
				$subject  = $this->view->translate('Forgot Password');
				$message = $this->view->translate('Dear ') . $this->view->translate($name).','. "<br/>".$this->view->translate('Please click on this')." <a href='$url'  >".$this->view->translate('Set Password')."</a>".$this->view->translate(' link to set the password'); 
				BackEnd_Helper_viewHelper::SendMail($email, $subject, $message, $fromEmail);
				
				$message = $this->view->translate('An email has been sent to your email address. Please reset your password');
				$result = array("Status"=>200,"message"=>$message);
					
			} else {
				$message = $this->view->translate('Email id does not exist in database!!');
				$result = array("Status"=>302,"Error:"=>$message);
			}
		}else{
	
			$result = array("Status"=>301,"Error:"=>"Missing email !!");
		}
		
		$data = Zend_Json::encode($result);
		echo $data;
		exit();
	}
	
	
	/**
	 * updateSaltAndPassword
	 *
	 * update user and password with study list
	 *
	 * @author Raman
	 * @version 1.0
	 */
	
	public function updateSaltAndPassword($password, $userId, $user, $sessionId){
		
		//salt updation
		$new_salt = md5(round(microtime(true) * 1000));
		
		$updatedsal = refusersecurity::updateSal($userId, $new_salt);
		$updpwd = SHA1($password.$new_salt);
			
		User::updatePwd($userId, $updpwd);
		
		$keyValueForStorage = Zend_Registry::getInstance()->constants->AES_KEY;
		
		$role = $user[0]['u__role_id'];
		
		//Get studylist from the database on userId basis
		$encStudyData = studyrecord::getInvestigatorAllData($userId, $role);
		
		//decrypt data from the database
		$studyData = BackEnd_Helper_viewHelper::decryptWsArray($encStudyData, $keyValueForStorage);
		//echo "<pre>"; print_r($studyData); die;
		if(!empty($studyData)){
			
			$fname = BackEnd_Helper_viewHelper::decryptString($user[0]['u__firstname'] , $keyValueForStorage);
			$lname = BackEnd_Helper_viewHelper::decryptString($user[0]['u__lastname'], $keyValueForStorage);
			
			$languages = BackEnd_Helper_viewHelper::getLanguages();
			
			$newArray = array("Status"=>200,"message"=>"success","userId"=>$userId,"First Name"=>$fname, "Last Name"=> $lname, "Role"=>$role,"language"=>$languages, "studyList"=>$studyData);
			$result = $newArray;
		} else {
			
			#flash error as no study exist
			$result = array("Status"=>302,"Error"=>"No study exist for this user!");
		}
		return $result;
	}
	
	/**
	 * encryptStringAction
	 *
	 * encrypt string
	 *
	 * @author Raman
	 * @version 1.0
	 */
	public function encryptstringAction() {
	
		$arrayToDec = array(
				"type1" => "11",
				"type2" => "Mindfull@123",
		);
		//$keyValue = "abcdefLakk282a09";
		//$keyValue = Zend_Registry::getInstance()->constants->PRIVATE_KEY_WS;

		$keyValue = Zend_Registry::getInstance()->constants->AES_KEY;
		$result = BackEnd_Helper_viewHelper::encryptString('clinitrialadmin@clinicops.com',$keyValue);
		//$result = BackEnd_Helper_viewHelper::decryptString($arrayToDec, $keyValue);

		echo "<pre>";
		print_r($result);
		die; 
	}
	
	public function decryptstringAction() {
	
		$arrayToDec = array(
				"type1" => "7",
				"type2" => "Mindfull@123",
				"type3" => "Consent Visit",
				"type4" => "Screening Visit",
				"type5" => "Study Visit 1"
		);
		$keyValue = "abcdefLakk285a18";
		//$keyValue = Zend_Registry::getInstance()->constants->PRIVATE_KEY_WS;
		//$keyValue = Zend_Registry::getInstance()->constants->AES_KEY;
		$result = BackEnd_Helper_viewHelper::decryptStrArray($arrayToDec, $keyValue);
		echo "<pre>";
		print_r($result);
		die;
	}
	
	
	function fetch_recursive($src_arr, $currentid = 0, $parentfound = false, $cats = array())
	{
	    foreach($src_arr as $row)
	    {
	        if((!$parentfound && $row['id'] == $currentid) || $row['study_id'])
	        {
	            $rowdata = array();
	            foreach($row as $k => $v)
	                $rowdata[$k] = $v;
	            $cats[] = $rowdata;
	            if($row['study_id'] == $currentid)
	                $cats = array_merge($cats, self::fetch_recursive($src_arr, $row['id'], true));
	        }
	    }
	    return $cats;
	}
	
	
	
	public function groupnest( $data, $groupkey, $nestname, $innerkey ) {
		$outer0 = array();
		$group = array(); $nested = array();
	
		foreach( $data as $row ) {
			$outer = array();
			while( list($k,$v) = each($row) ) {
				if( $k==$innerkey ) break;
				$outer[$k] = $v;
			}
	
			$inner = array( $innerkey => $v );
			while( list($k,$v) = each($row) ) {
				if( $k==$innerkey ) break;
				$inner[$k] = $v;
			}
	
			if( count($outer0) and $outer[$groupkey] != $outer0[$groupkey] ) {
				$outer0[$nestname] = $group;
				$nested[] = $outer0;
				$group = array();
			}
			$outer0 = $outer;
	
			$group[] = $inner;
		}
		$outer[$nestname] = $group;
		$nested[] = $outer;
	
		return $nested;
	}
	
	/**
	 * reloginAction
	 *
	 * checks the authentication of a user without returning the studies data
	 *
	 * @author Raman
	 * @version 1.0
	 */
	
	public function reloginAction()
	{
		$handle = fopen('php://input','r');
		$jsonInput = fgets($handle);
		//$jsonInput = '{"device_id": "mT27HOeG/zP4L+/zJ7kbWw==","password": "HKu9ings1HpJJe1TcBXXMA==","username": "M6xr7q0LtUC3/nPxkJ8LZg=="}';
		$params = Zend_Json::decode($jsonInput);
		$time_to_expire = Zend_Registry::getInstance()->constants->WS_TIME_TO_EXPIRE;
	
		if(isset($params['username']) && @$params['username'] !="" && isset($params['password'])&& @$params['password'] != "" && isset($params['device_id'])&& @$params['device_id'] != ""){
	
	
			//generate session id for investigator
			$sessionId = SHA1(round(microtime(true) * 1000));
			$keyValue = Zend_Registry::getInstance()->constants->PRIVATE_KEY_WS;
			$params = BackEnd_Helper_viewHelper::decryptStrArray($params, $keyValue);
			$deviceId = $params ['device_id'];
				
			//Encrypt data to compare with database
			$keyValueForStorage = Zend_Registry::getInstance()->constants->AES_KEY;
			$username = BackEnd_Helper_viewHelper::encryptString($params ['username'], $keyValueForStorage);
				
			$password = $params ['password'];
			$device_id  = $params ['device_id'];
				
			$comingDeviceId = $deviceId;
			$comingDeviceIdEnc = BackEnd_Helper_viewHelper::encryptString($comingDeviceId, $keyValueForStorage);
				
				
			//get salt on username basis
			$userpasssalt = user::getInvestigatorPassSal($username);
				
			$salt = $userpasssalt['r__password_salt'];
			$newpass = SHA1($password.$salt);
			$data_adapter = new Auth_InvestigatorAdapter($username, $newpass, $keyValueForStorage);
				
			$auth = Zend_Auth::getInstance ();
			$result = $auth->authenticate ( $data_adapter );
				
			//Authenticate user
			if (Auth_InvestigatorAdapter::hasIdentity ()) {
	
				//create object of Investigator class
				$user = Auth_InvestigatorAdapter::getIdentity();
				$userId = $user[0]['u__id'];
	
				//get device details
				$isDeviceInvRegistered = user::checkUserDeviceId($username, $comingDeviceIdEnc);
	
				//generate session specific key at run time to decrypt data
				$firstPart = substr($deviceId, 0, 6);
				$secondPart = substr($keyValue, 0, 6);
				$thirdPart = substr($sessionId, 0, 4);
				$keyValueToDecrypt = $firstPart.$secondPart.$thirdPart;
	
				//update session logs for device
				$sessionObj = new sessionLogs();
				$saveSession = $sessionObj->saveDeviceSessionDetails($userId , $sessionId , $comingDeviceIdEnc);
	
	
				if($isDeviceInvRegistered){
						
					//update investigator and device session
					User::updateDeviceSession($userId, $comingDeviceIdEnc, $sessionId);
						
					//update salt and password
					$result = self::updateSaltAndPasswordForRelogin($password, $userId, $user, $sessionId);
					if($result['Status']== 200){
	
						$result = BackEnd_Helper_viewHelper::encryptStrArray($result, $keyValueToDecrypt);
						$result['sessionId'] = $sessionId;
					}
	
				} else {
						
					//register device session
					User::insertDeviceSession($userId, $comingDeviceIdEnc, $sessionId);
					//check device session
	
					//salt and password updation
					$result = self::updateSaltAndPasswordForRelogin($password, $userId, $user, $sessionId);
					if($result['Status']== 200){
						$result = BackEnd_Helper_viewHelper::encryptStrArray($result, $keyValueToDecrypt);
						$result['sessionId'] = $sessionId;
					}
	
				}
	
					
	
	
			} else {
				#flash error
				$result = array("Status"=>302,"Error"=>"Unauthorized user!");
			}
	
	
		}else{
	
			$result = array("Status"=>301,"Error"=>"Missing username or password or device id");
		}
	
	
		$data = Zend_Json::encode($result);
		echo $data;
		exit();
	
	}
	
	/**
	 * updateSaltAndPasswordForRelogin
	 *
	 * update user and password
	 *
	 * @author Raman
	 * @version 1.0
	 */
	
	public function updateSaltAndPasswordForRelogin($password, $userId, $user, $sessionId){
	
		//salt updation
		$new_salt = md5(round(microtime(true) * 1000));
	
		$updatedsal = refusersecurity::updateSal($userId, $new_salt);
		$updpwd = SHA1($password.$new_salt);
			
		User::updatePwd($userId, $updpwd);
	
		$keyValueForStorage = Zend_Registry::getInstance()->constants->AES_KEY;
	
		$role = $user[0]['u__role_id'];
			
		$fname = BackEnd_Helper_viewHelper::decryptString($user[0]['u__firstname'] , $keyValueForStorage);
		$lname = BackEnd_Helper_viewHelper::decryptString($user[0]['u__lastname'], $keyValueForStorage);
	
		$newArray = array("Status"=>200,"message"=>"success","userId"=>$userId,"First Name"=>$fname, "Last Name"=> $lname, "Role"=>$role);
		$result = $newArray;
		
		return $result;
	}
	
	
	
// End class	
}