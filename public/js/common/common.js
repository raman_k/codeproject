
/**
 * JS translator
 * @author Raman
 */

var gt = new Gettext();
function __ (msgid) { return gt.gettext(msgid); }

/**
 * Add overlay for front-end
 * @author kraj
 */
function ___addOverLay()
{
	

	if( jQuery("div#overlay").length == 0)

	{
		
		var overlay = jQuery("<div id='overlay'><img id='img-load' src='" +  HOST_PATH  + "/public/images/backend/ajax-loader.gif'/></div>");
		overlay.appendTo(document.body);
		
	} 
		
}
/**
 * remove overlay
 * @author kraj
 */
function ___removeOverLay()
{
	jQuery('div#overlay').remove();
	return true ;
}
/**
 * Add overlay for back-end
 * @author kraj
 */
function addOverLay()
{
	

	/*if( jQuery("div#overlay").length == 0)

	{
		
		var overlay = jQuery("<div id='overlay'><img id='img-load' src='" +  HOST_PATH_PUBLIC + "/images/back_end/ajax-loader.gif'/></div>");
		overlay.appendTo(document.body);
		jQueryt = jQuery(document.body); // CHANGE it to the table's id you have
		jQuery("#img-load").css({
		  top  : (jQueryt.height() / 2),
		  left : (jQueryt.width() / 2)
		});
	} */
		
}
/**
 * remove overlay
 * @author kraj
 */
function removeOverLay()
{
	jQuery('div#overlay').remove();
	return true ;
}
function ucfirst(str) {
	
	str = str == null ?  "" : str ;
	var firstLetter = str.substr(0, 1);
	return firstLetter.toUpperCase() + str.substr(1);
}

function changeDateFormat(date){
	date = date.split('-');
	return date['2']+'-'+date['1']+'-'+date['0'];
}
function redriectoUrl(a){
	
	var url = $(this).attr('rel');
	window.location.href = url;
}

function navigateLanguage(){
	
	var siteLang = $("#siteLanguage").val();
	createCookie("language", siteLang, 1);
	window.location.reload(true);
}

//Cookies
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";

    var fixedName = '';
    name = fixedName + name;

    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}


 /**
 * 
 * Code for automatic logout after 8 idle minutes
 * 
 * @author skumar5
 * 
 */

var timer = 0;
function set_interval() {
  timer = setInterval("auto_logout()", 28800000);
}

function reset_interval() {
  if (timer != 0) {
    clearInterval(timer);
    timer = 0;
    timer = setInterval("auto_logout()", 28800000);
  }
}

function auto_logout() {
	$.ajax({
		url : MODULE_LOGIN + "/index/logout",
		success : function(data) {
			window.location.href = MODULE_LOGIN;
		}
	});
}
