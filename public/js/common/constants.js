var HOST_PATH = 'http://' + window.location.host + '/';

var MODULE_CLINCTRIAL = 'http://' + window.location.host + '/clinictrial';
	
var MODULE_INVESTIGATOR = 'http://' + window.location.host + '/investigator';

var MODULE_LOGIN = 'http://' + window.location.host + '/login';

var MODULE_SUBJECT = 'http://' + window.location.host + '/subject';

var PUBLIC_PATH = 'http://' + window.location.host + '/public';

var IMAGE_PATH = PUBLIC_PATH + '/images';

var UPLOAD_PATH = 'http://' + window.location.host + '/uploads';