$(document).ready(function(){
	
	validateForm();
	$('form#formloginchangepassword').submit(function(){
		validateForm();
	});
	
	$('select#user_type').keydown(enableDisableAuth);
	$('select#user_type').change(enableDisableAuth);
	
	$('select#user_type').change(function(){
		var role = $('select#user_type').val();
		if(role == 2){
			$('input#Authenticate').click(getStudtByInvestigator);
		}
		if(role == 4){
			$('input#Authenticate').click(getStudyBySubject);
		}
	});
	
	$('select#user_type').keydown(function(){
		var role = $('select#user_type').val();
		if(role == 2){
			$('input#Authenticate').click(getStudtByInvestigator);
		}
		if(role == 4){
			$('input#Authenticate').click(getStudyBySubject);
		}
	});
	
	$('input#Authenticate').removeClass('main-btn');
	$('select#investigator_study').attr('disabled','disabled');
	validateLoginForm();
	$('form#login').submit(function(){
		validateLoginForm();
		if($("form#login").valid()){
			return true;
		}
		return false;  
	});
	
	validateResetPasswordForm();
	$('form#resetPassword').submit(function(){
		validateResetPasswordForm();
		if($("form#resetPassword").valid()){
			return true;
		}
		return false;
		  
	});
	
	validateNewInvestigatorForm();
	
	validateForgotPasswordForm();
	$('form#forgotpassword').submit(function(){
		validateForgotPasswordForm();
		if($("form#forgotpassword").valid()){
			return true;
		}
			return false;
	});
	
	$('form#newInvestigator').submit(function(){
		validateNewInvestigatorForm();
		if($("form#newInvestigator").valid()){
			return true;
		}
		return false;
	});
	errormsgHide();
	
	 $.validator.addMethod("email", function(value, element) {
		   return this.optional(element) || /^[a-zA-Z0-9._-]+@(?:[A-Z0-9-]+\.)+[a-zA-Z]{2,3}$/i.test(value);
		   // return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/i.test(value);
	    }, "Please enter the valid email");
	 
	 $.validator.addMethod("validpassword", function(value, element) {
		    return this.optional(element) ||
		    /^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[\W_]).*$/.test(value);
		}, "The password must contain a minimum of one lower case character," +
		           " one upper case character, one digit and one special character..");
	 
});
/**
 * getSubjectByInvestigator
 * 
 * get subject by investigator
 * 
 * @author kraj
 * @version 1.0
 */
function getStudtByInvestigator(){
	
	$.ajax({
		url : MODULE_LOGIN + "/index/getstudy",
		method : "post",
		data : {
			'username' : $('#username').val(),
			'password' : $('#password').val()
		},
		dataType : "json",
		type : "post",
		success : function(data) {
			var op = '';
			$('select#investigator_study option').remove();
			if (data.length > 0) {

				op = '<option>-'+__('Select Study')+'-</option>';
				for(var i in data){
					
						op+= '<option value='+data[i].studyId+'>' + data[i].title + "</option>";
				}		
				
			} else {
				
				op += '<option>-'+__('Record not found')+'-</option>';
			}
		
			$('select#investigator_study').append(op);
		}
	});		
}

/**
 * getStudyBySubject
 * 
 * get study by subject
 * 
 * @author skumar5
 * @version 1.0
 */
function getStudyBySubject(){
	
	$.ajax({
		url : MODULE_LOGIN + "/index/getsubjectstudy",
		method : "post",
		data : {
			'username' : $('#username').val(),
			'password' : $('#password').val()
		},
		dataType : "json",
		type : "post",
		success : function(data) {
			var op = '';
			$('select#investigator_study option').remove();
			if (data.length > 0) {

				op = '<option>-'+__('Select Study')+'-</option>';
				for(var i in data){
					
						op+= '<option value='+data[i].id+'>' + data[i].title + "</option>";
				}		
				
			} else {
				
				op += '<option>-'+__('Record not found')+'-</option>';
			}
		
			$('select#investigator_study').append(op);
		}
	});		
}

/**
 * enableDisableAuth
 * 
 * if investigator then enable select box for study and authenticate button
 * 
 * @author kraj
 * @version 1.0
 */
function enableDisableAuth(){

	var role = $(this).val();
	
	if(role==2 || role==4){	
		
		$('input#Authenticate').removeAttr('disabled');
		$('input#Authenticate').removeClass('authenticate-btn');
		$('input#Authenticate').addClass('main-btnew');
		$('select#investigator_study').removeAttr('disabled');
		
	}
	else{
		
		$('input#Authenticate').attr('disabled','disabled');
		$('input#Authenticate').removeClass('main-btnew');
		$('input#Authenticate').addClass('authenticate-btn');
		$('select#investigator_study').attr('disabled','disabled');
	}
}

/**
 * validateLoginForm
 * 
 * validations for login page
 * 
 * @author skumar5
 * @version 1.0
 */

function validateLoginForm()
{
	$("form#login").validate({
		rules: {
			username: {
	              required: true
	          },
	          password: {
				  required: true,
				  maxlength : 20
	          },
	          user_type: {
	              required: true
	          } 
	      },
	      messages: {
	    	  username: {
	              required: __("Please enter the username")
	          },
	          password: {
				  required: __("Please enter the password"),
				  maxlength : __("Maximum 20 characters are allowed")
	          },
	          user_type: {
	              required: __("Please select the role")
	          }
	      },
	      errorPlacement: function(error,element) {
	    	  $('select#user_type').removeClass('mb20');
	    	  $(element).after(error);
	    }
	  });
}

/**
 * validateResetPasswordForm
 * 
 * validations for Reset password page
 * 
 * @author skumar5
 * @version 1.0
 */

function validateResetPasswordForm()
{
	//$('div.success,div.error').hide();
	$("form#resetPassword").validate({
		rules: {
			password: {
	              required: true,
	              minlength : 8
	          },
	          re_password: {
				  required: true,
				  minlength : 8,
				  equalTo : "#password"
	          }
	      },
	      messages: {
	          password: {
				  required: __("Please enter the password"),
				  minlength : __("minimum 8 characters are required")
	          },
	          re_password: {
	              required: __("Please enter the password once again"),
				  minlength : __("minimum 8 characters are required"),
				  equalTo : __("password and Re-password should be same")
	          }
	      },
	      errorPlacement: function(error,element) {
	    	  $(element).after(error);
	    }
	      
	  });
}

/**
 * validateForgotPasswordForm
 * 
 * validations for forgot password page
 * 
 * @author skumar5
 * @version 1.0
 */

function validateForgotPasswordForm()
{
	$("form#forgotpassword").validate({
		rules: {
			email: {
	              required: true,
	              email : true
	          }
	      },
	      messages: {
	    	  email: {
				  required: __("Please enter the Email address"),
				  email : __("Please enter the valid Email address")
	          }
	      },
	      errorPlacement: function(error,element) {
	    	  $(element).after(error);
	      }
	  });
	
}

function errormsgHide(){
	$("input.login-btnew").click(function(){
		if($("span.errorserver").html() != ''){
			$('div.success,div.error').hide();
		}
	});
}

function validateNewInvestigatorForm()
{
	$("form#newInvestigator").validate({
		rules: {
			username: {
	              required: true,
	              remote : MODULE_LOGIN + '/index/unique-user/'
	          },
	          password: {
				  required: true,
				  minlength : 8,
				  validpassword : true
	          },
	          re_password: {
				  required: true,
				  minlength : 8,
				  equalTo : "#password"
	          }
	      },
	      messages: {
	          username: {
				  required: __("Please enter the username"),
				  remote : __("username already exists!!")
				  
	          },
	          password: {
	              required: __("Please enter the password"),
				  minlength : __("minimum 8 characters are required"),
				  validpassword : __("The password must contain a minimum of one lower case character,one upper case character, one digit and one special character")
	          },
	          re_password: {
	              required: __("Please enter the password"),
				  minlength : __("minimum 8 characters are required"),
				  equalTo : __("Password and confirm password must be same")
	          }
	      }
	  });
}


function validateForm() {
	
	validateChangePassword = $("form#formloginchangepassword").validate({
		rules: {
			oldpassword: {
	              required: true,
	              maxlength : 20,
	              minlength : 8
	          },
	          newpassword: {
				  required: true,
				  maxlength : 20,
				  minlength : 8,
				  validpassword : true
	          },
	          confirmpassword: {
	              required: true,
	              maxlength : 20,
	              minlength : 8,
	              equalTo : "#newpassword"
	          } 
	      },
	      messages: {
	    	  oldpassword: {
	    		  required: __("Please enter the old password"),
				  maxlength : __("Maximum 20 characters are allowed"),
				  minlength : __("Please enter minimum 8 characters ")
	          },
	          newpassword: {
				  required: __("Please enter the new password"),
				  maxlength : __("Maximum 20 characters are allowed"),
				  minlength : __("Please enter minimum 8 characters "),
				  validpassword : __("The password must contain a minimum of one lower case character,") +
				  					__(" one upper case character, one digit and one special character.")
	          },
	          confirmpassword: {
	        	  required: __("Please enter the confirm password"),
				  maxlength : __("Maximum 20 characters are allowed"),
				  minlength : __("Please enter minimum 8 characters "),
				  equalTo : __("Password and Confirm password must be same")
	          }
	      },
	      errorPlacement: function(error,element) {
	    	 
	    	  $(element).next('.clr').html(error);
	      }
	  });
}

function goBack(){
	window.location.href = MODULE_LOGIN;
}