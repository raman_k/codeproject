$(document).ready(function(){
	//get investigator record from database
	//getInvestigatorData();
	$("#investigator_list").css("width","100%");
	$( "#dialogDelete" ).dialog({
		autoOpen: false,
		width: 400,
		 show: {
			 effect: "fadeIn",
			 duration: 500
			 },
			 hide: {
			 effect: "fadeOut",
			 duration: 500
			 },
		buttons: [
			{
				text: "Ok",
				click: function() {
					$( this ).dialog( "close" );
					var cId = $( "#dialogDelete" ).attr('rel');
					deleteRecord(cId);
				}
			},
			{
				text: "Cancel",
				click: function() {
					$( this ).dialog( "close" );
					return false;
				}
			}
		]
	});
});

/**
 * moveToTrash
 * 
 * Call to edit function when user click on any row of the shop list
 * 
 * @param integer id
 * @author kraj
 * @version 1.0
 */
function moveToTrash(id){
	$( "#dialogDelete" ).dialog( "open" );
	$( "#dialogDelete" ).attr('rel',id);
 }
/**
 * deleteRecord
 * 
 * function use for deleted Article from list
 * and from database
 * 
 * @param id
 * @author kraj
 */
function deleteRecord(id) {

	$.ajax({
		url : MODULE_CLINCTRIAL+"/manageinvestigator/delete-investigator/id/" + id,
		method : "post",
		dataType : "json",
		type : "post",
		success : function(data) {
			
			if (data != null) {
				
				window.location.href = "manageinvestigator";
				
			} else {
				
				window.location.href = "manageinvestigator";
			}
		}
	});	
}
//var investigatorList = $('table#investigator_list').dataTable();
/**
 * getStudiesData
 * 
 * get record of the studies
 * 
 * @author kraj
 * @version 1.0
 */
