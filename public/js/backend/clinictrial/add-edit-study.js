$(document).ready(function(){
	
//code for add/edit study record
 	
	$("#study_startdate").datepicker({
		minDate : 0,
		dateFormat : 'yy-mm-dd',
		onSelect : function(dateStr) {
			$("#study_enddate").datepicker("destroy");
			$("#study_enddate").val(' ');
			var newDate = $(this).datepicker('getDate');
			if (newDate) { // Not null
				newDate.setDate(newDate.getDate() + 1);
			}
			$("#study_enddate").datepicker({
				minDate : newDate,
				dateFormat : 'yy-mm-dd',
                                onSelect: function(dateStr) {
                                 $(this).parent().next('.error').css('display','none');
                                }
			});
			$("#study_enddate").datepicker({
				setDate : newDate,
				dateFormat : 'yy-mm-dd'
                                
			});
                        $(this).parent().next('.error').css('display','none');
		}
	});

	$("#btn_startdate").click(function() {
		$("#study_startdate").datepicker('show');
		$("#study_startdate").datepicker({
			minDate : 0,
			dateFormat : 'yy-mm-dd',
			onSelect : function(dateStr) {
				$("#study_enddate").datepicker("destroy");
				$("#study_enddate").val(' ');
				var newDate = $(this).datepicker('getDate');
				if (newDate) { // Not null
					newDate.setDate(newDate.getDate() + 1);
				}
				$("#study_enddate").datepicker({
					minDate : newDate,
					dateFormat : 'yy-mm-dd',
                                        onSelect: function(dateStr) {
                                          $(this).parent().next('.error').css('display','none');
                                        }
				});
				$("#study_enddate").datepicker({
					setDate : newDate,
					dateFormat : 'yy-mm-dd'                                        
				});
                                $(this).parent().next('.error').css('display','none');
			}
		});
	});

	$("#btn_enddate").click(function() {
		$("#study_enddate").datepicker('show');
	});
	var single_submit = true;
 	validateAddStudyForm();
	$('form#add_study,form#edit_study').submit(function(){
		validateAddStudyForm();
		if($("form#add_study,form#edit_study").valid() && single_submit == true){
			single_submit = false;
			return true;
			$('#add_study input[type=submit], #edit_study input[type=submit]').attr("disabled", "disabled");
		}
		return false;
		  
	});
 	
 	jQuery.validator.addMethod("alpha", function(value, element) {
 		return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
 		},"Only alphabets are allowed."); 	
 	
 	jQuery.validator.addMethod("notEqual", function(value, element, param) {
 		return this.optional(element) || value != 0;
 		}, "Please specify a different (non-default) value");
});

/*code for validations add/edit study record 
 * author : skumar5
 */
var validateForm = null;
function validateAddStudyForm()
{
	validateForm = $("form#add_study,form#edit_study").validate({
    rules: {
    	study_id:{
			required: true,
			maxlength : 20,
			remote : {
                url:MODULE_CLINCTRIAL + "/managestudy/check-study-id",
                type: "post"
            }
		},
		study_title: {
            required: true,
            maxlength : 1024
        },
        study_description: {
            required: true,
            maxlength : 500
        },
        study_companies: {
        	required: true
        },
        study_startdate: {
        	required: true
        },
        study_status: {
            required: true
        },
        study_enrollment: {
        	required: true,
        	digits : true,
        	maxlength : 20,
        	notEqual : true
        	
        },
        study_enddate: {
        	required: true
        }
    },
	
    messages: {
    	study_id:{
			required: "Please enter the Study id",
			maxlength : "Only 20 characters are allowed",
			remote: "Please enter different study id"
		},
		study_title: {
            required: "Please enter the title",
            alpha : "Only alphabets are allowed",
            maxlength : "Only 1024 characters are allowed"
        },
        study_description: {
            required: "Please enter the description",
            maxlength : "Only 500 characters are allowed"
        },
        study_companies: {
        	required: "Please select company"
        },
        study_startdate: {
        	required: "Please select start date"
        },
        study_status: {
            required: "Please select status"
        },
        study_enrollment: {
        	required: "Please enter the study enrollment",
        	digits : "Only integer values are allowed",
        	maxlength : "Only 20 characters are allowed",
        	notEqual : "Expected enrollment cannot be zero"
        },
        study_enddate: {
        	required: "Please select end date"
        }
    },
	errorPlacement: function(error,element) {
    
		if ($(element).attr("type") == "text")
		{
			$(element).parent('div.input-box').after(error);
		} 
		else if($(element).attr("id") == "study_companies")
		{
			$(element).parent('div.input-box').after(error);
		}  
		else if($(element).attr("id") == "study_status")
		{
			$(element).parent('div.input-box').after(error);
		} 
		else
		{
			$(element).after(error);
		}
}
});
}