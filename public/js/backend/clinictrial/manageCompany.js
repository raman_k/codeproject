$(document).ready(function() {
	//getCompaniesData();
	
	
	$("#manage-company-data").css("width","100%");
	$("#dialogDelete").dialog({
		autoOpen : false,
		width : 400,
		show : {
			effect : "fadeIn",
			duration : 500
		},
		hide : {
			effect : "fadeOut",
			duration : 500
		},
		buttons : [ {
			text : "Ok",
			click : function() {
				$(this).dialog("close");
				var cId = $("#dialogDelete").attr('rel');
				deleteRecord(cId);
			}
		}, {
			text : "Cancel",
			click : function() {
				$(this).dialog("close");
				return false;
			}
		} ]
	});
	

});

function openDialogBox(id) { 
	$("#dialogDelete").dialog("open");
	$("#dialogDelete").attr('rel', id);
}

/**
 * moveToTrash
 * 
 * function use to open delete dialog box
 * 
 * @param id
 * @author Raman
 * @version 1.0
 */


/**
 * deleteRecord
 * 
 * function use to delete company record from database
 * 
 * @param id
 * @author Raman
 * @version 1.0
 */
function deleteRecord(id) {

	$.ajax({
		url : MODULE_CLINCTRIAL + "/managecompany/delete-company/id/" + id,
		method : "post",
		dataType : "json",
		type : "post",
		success : function(data) {

			if (data != null) {

				window.location.href = "managecompany";

			} else {

				window.location.href = "managecompany";
			}
		}
	});
}
/*var companiesList = $('table#manage-company-data').dataTable();
/**
 * getCompaniesData
 * 
 * display company data 
 * 
 * @author Raman
 * @version 1.0
 */
	/*function getCompaniesData(){
		companiesList = $("#manage-company-data")
			.dataTable(
					{
						"bLengthChange" : false,
						"bInfo" : true,
						"bFilter" : false,
						"bDestroy" : true,
						"bProcessing" : false,
						"bServerSide" : false,
						"iDisplayLength" : 100,
						"bDeferRender" : true,
						"aaSorting": [[ 0, "desc" ]],
						"sPaginationType" : "full_numbers",
						"sAjaxSource" : MODULE_CLINCTRIAL + "/managecompany/get-company-list/flag/0",
						"aoColumns" : [
								{
									"fnRender" : function(obj) {
								        var id = null;
										return id = obj.aData.id;
								
									},
									"bVisible":  false ,
									"bSortable" : true,
									"sType": 'numeric'
									
								},
								{
									"fnRender" : function(obj) {
										var projectNo;
                                        if(obj.aData.projectNumber == null){
                                        	projectNo =	"";
                                        }else {
                                        	projectNo = obj.aData.projectNumber;
                                        }
										return "<p>"+projectNo+"</p>";
									},
									"bSearchable" : false,
									"bSortable" : false
								},
								{
									"fnRender" : function(obj) {
										var comName;
										if(obj.aData.name == null){
											comName = "";	
										}else {
											comName = obj.aData.name;
										}
										return "<p>"+comName+"</p>";
									},
									"bSearchable" : false,
									"bSortable" : false
								},
								{
									"fnRender" : function(obj) {
										var startDate;
										if(obj.aData.startDate == null){
											startDate = "";	
										}else {
											startDate = obj.aData.startDate;
										}
										return "<p>"+startDate+"</p>";
									},
									"bSearchable" : false,
									"bSortable" : false
								},
								{
									"fnRender" : function(obj) {
										var dealValue;
										if(obj.aData.dealValue == null){
											dealValue = "";	
										}else {
											dealValue = obj.aData.dealValue;	
										}
										return "<p>"+dealValue+"</p>";

									},
									"bSearchable" : false,
									"bSortable" : false

								},
								{
									"fnRender" : function(obj) {
										var status;
										if (obj.aData.projectStatus == 1) {
											status = 'Inactive';
										} else {
											status = 'Active';
										}
										return "<p>"+status+"</p>";

									},
									"bSearchable" : false,
									"bSortable" : false

								},
								{
									"fnRender" : function(obj) {

										return "<div class='actions'> <a href='"
												+ IMAGE_PATH
												+ "/backend/preview-icon.png' alt='' /></a> <a title='View Record' href='"
												+ MODULE_CLINCTRIAL
												+ "/managecompany/view-company/id/"
												+ obj.aData.id
												+ "'><img src='"
												+ IMAGE_PATH
												+ "/backend/preview-icon.png' alt='' /></a> <a title='Edit Record' href='"
												+ MODULE_CLINCTRIAL
												+ "/managecompany/edit-company/id/"
												+ obj.aData.id
												+ "'><img src='"
												+ IMAGE_PATH
												+ "/backend/edit-icon.png' alt='' /></a> <a href='javascript:;' title='Delete Record' onclick='openDialogBox("
												+ obj.aData.id
												+ ")'><img src='"
												+ IMAGE_PATH
												+ "/backend/delete-icon.png' alt='' /></a> </div>";

									},
									"bSearchable" : false,
									"bSortable" : false

								} ],
						"fnPreDrawCallback" : function(oSettings) {
							$('#articleList').css('opacity', 0.5);
						},
						"fnDrawCallback" : function(obj) {

						},

						"fnInitComplete" : function(obj) {
							// $("form#createShop").each(function() {
							// this.reset(); });
							$('td.dataTables_empty').html('No record found !');
							$('td.dataTables_empty').unbind('click');
							// removeOverLay();
						},
						"fnServerData" : function(sSource, aoData, fnCallback) {
							$.ajax({
								"dataType" : 'json',
								"type" : "POST",
								"url" : sSource,
								"data" : aoData,
								"success" : fnCallback
							});

						}
					});
}*/