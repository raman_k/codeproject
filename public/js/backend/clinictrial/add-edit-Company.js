$(document).ready(function() {
		
					var single_submit = true;
					$("#act_start_date").datepicker({
						minDate : 0,
						dateFormat : 'yy-mm-dd',
						onSelect : function(dateStr) {
							$("#act_end_date").datepicker("destroy");
							$("#act_end_date").val(' ');
							var newDate = $(this).datepicker('getDate');
							if (newDate) { // Not null
								newDate.setDate(newDate.getDate() + 1);
							}
							$("#act_end_date").datepicker({
								minDate : newDate,
								dateFormat : 'yy-mm-dd',
                                onSelect: function(dateStr) {
                                  $(this).parent().next('.error').css('display','none');
                                }                                                                
							});
							$("#act_end_date").datepicker({
								setDate : newDate,
								dateFormat : 'yy-mm-dd'
							});
                            $(this).parent().next('.error').css('display','none');
						}
					});

					$("#btn_start_date").click(function() {
						$("#act_start_date").datepicker('show');
						$("#act_start_date").datepicker({
							minDate : 0,
							dateFormat : 'yy-mm-dd',
							onSelect : function(dateStr) {
								$("#act_end_date").datepicker("destroy");
								$("#act_end_date").val('');
								var newDate = $(this).datepicker('getDate');
								if (newDate) { // Not null
									newDate.setDate(newDate.getDate() + 1);
								}
								$("#act_end_date").datepicker({
									minDate : newDate,
									dateFormat : 'yy-mm-dd',
                                    onSelect: function(dateStr) {
                                    $(this).parent().next('.error').css('display','none');
                                    }

								});
								$("#act_end_date").datepicker({
									setDate : newDate,
									dateFormat : 'yy-mm-dd'
								});
                                   $(this).parent().next('.error').css('display','none');
							}
						});
					});
					
					$("#btn_end_date").click(function() {
						$("#act_end_date").datepicker('show');
					});
					
					validateCompanyForm();
					// validate add/edit company form on submit
					$('form#add_company,form#edit_company').submit(function(){
						validateCompanyForm();
						if ($("form#add_company,form#edit_company").valid()  && !(invalidForm) ) {
							single_submit = false;
							$('#add_company input[type=submit], #edit_company input[type=submit]').attr("disabled", "disabled");
							return true;
							
						}
						return false;

					});
					
					// change add/edit's state list on the basis of selected country
					$('#comp_country,#comp_owner_country').change(function(){
							var compSelectedId = $(this).val();
							var comSelectedName = $(this).attr('name');
							$.ajax({
								type : "GET",
								url : MODULE_CLINCTRIAL+ "/managecompany/get-states/id/"+ compSelectedId,
								success : function(res) {
									if (comSelectedName == 'comp_country') {
										if (res == "") {
											$('#comp_state').html("<option value=''>N/A");
										} else {
											$('#comp_state').html("<option value=''>Select State</option>");
											$('#comp_state').append(res);
										}
									} else {
										if (res == "") {
											$('#comp_owner_state').html("<option value=''>N/A");
										} else {
											$('#comp_owner_state').html("<option value=''>Select State</option>");
											$('#comp_owner_state').append(res);
										}
									}
								}
							});
						});
					// change add/edit's city list on the basis of selected state
					$('#comp_state,#comp_owner_state').change(function() {
							var stateSelectedId = $(this).val();
							var stateSelectedName = $(this).attr('name');
							$.ajax({
									type : "GET",
									url : MODULE_CLINCTRIAL+ "/managecompany/get-cities/id/"+ stateSelectedId,
									success : function(res) {
										if (stateSelectedName == 'comp_state') {
											if (res == "") {
												$('#comp_city').html("<option value=''>N/A");
											} else {
												$('#comp_city').html("<option value=''>Select City</option>");
												$('#comp_city').append(res);
											}
										} else {
											if (res == "") {
												$('#comp_owner_city').html("<option value=''>N/A");
											} else {
												$('#comp_owner_city').html("<option value=''>Select City</option>");
												$('#comp_owner_city').append(res);
											}
										}
									}
								});
						});
					
					 $.validator.addMethod("alphanumeric", function(value, element) {
					        return this.optional(element) || /^[a-z0-9]+$/i.test(value);
					    }, "field must contain only letters and numbers");
					 
					 $.validator.addMethod("phonenumber", function(value, element) {
					        return this.optional(element) || /^[0-9,\+-]+$/i.test(value);
					    }, "please enter the valid phone number");
					 
					 $.validator.addMethod("email", function(value, element) {
						 	return this.optional(element) || /^[a-zA-Z0-9._-]+@(?:[A-Z0-9-]+\.)+[a-zA-Z]{2,3}$/i.test(value);
					        //return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/i.test(value);
					    }, "Please enter the valid email");
					 
					 jQuery.validator.addMethod("notEqual", function(value, element, param) {
					 		return this.optional(element) || value != 0;
					 		}, "Please specify a different (non-default) value");
					 
					 $.validator.addMethod("lessthan", function(value, element) {
					        return parseInt($('#amount').val()) < parseInt($('#deal').val());
					    }, "One has to be greater than two"); 
					 		 
				});

/**
 * validateCompanyForm
 * 
 * validate add/edit company form
 * 
 * @author Raman
 * @version 1.0
 */
function validateCompanyForm() {
	
	$("form#add_company,form#edit_company").validate({
		rules : {
			comp_name : {
				required : true,
				maxlength : 40
			},
			comp_address1 : {
				required : true,
				maxlength : 60
			},
			comp_address2 : {
				maxlength : 60
			},
			comp_country : {
				required : true
			},	
			logoFile : {
				required : true
			},
			comp_zip : {
				required : true,
				alphanumeric : true,
				maxlength : 8,
				minlength : 6
			},
			comp_state : {
				required : true
			},
			comp_city : {
				required : true
			},
			comp_telephone : {
				required : true,
				maxlength : 16,
				minlength : 10,
				phonenumber : true
			},
			comp_email : {
				email : true,
				maxlength : 60
			},
			comp_owner_name : {
				required : true,
				maxlength : 40
			},
			comp_owner_address1 : {
				required : true,
				maxlength : 60
			},
            comp_owner_address2 : {
				maxlength : 60
			},
			comp_owner_country : {
				required : true
			},
			comp_owner_zip : {
				required : true,
				alphanumeric : true,
				maxlength : 8,
				minlength : 6
			},
			comp_owner_state : {
				required : true
			},
			comp_owner_city : {
				required : true
			},
			comp_owner_telephone : {
				required : true,
				maxlength : 16,
				minlength : 10,
				phonenumber : true
			},
			comp_owner_email : {
				email : true,
				maxlength : 60
			},
			comp_act_project_no : {
				required : true,
				maxlength : 20,
				digits : true,
				remote : {
                    url:MODULE_CLINCTRIAL + "/managecompany/check-project-no",
                    type: "post"
                }
			},
			comp_act_start_date : {
				required : true			
			},
			comp_act_status : {
				required : true
			},
			comp_act_end_date : {
				required : true
			},
			comp_finance_deal_value : {
				number : true,
				maxlength : 20,
				notEqual : true
			},
			comp_finance_amt_paid : {
				number : true,
				maxlength : 20
			},
			comp_finance_contact_info : {
				required : true,
				maxlength : 16,
				minlength : 10,
				phonenumber :true
				
			},
			comp_finance_email_id : {
				email : true,
				maxlength : 60
			},
			comp_finance_deal_value : {
				required : true
			}

		},
		messages : {
			comp_name : {
				required : "Please enter company name",
				maxlength : "Maximum 40 characters are allowed"
			},
			comp_address1 : {
				required : "Please enter company address"
			},
            comp_address2 : {
				required : "Please enter company address",
				maxlength : "Maximum 60 characters are allowed"
			},
			comp_country : {
				required : "Please select country"
			},
			logoFile : {
				required : "Please choose any image"
			},
			comp_zip : {
				required : "Please enter zip code",
				alphanumeric : "Please enter digits and alphabets only",
				maxlength : "Maximum 8 characters are allowed",
				minlength : "minimum 6 characters are required"
			},
			comp_state : {
				required : "Please select state"
			},
			comp_city : {
				required : "Please select city"
			},
			comp_telephone : {
				required : "Please enter telephone no",
				maxlength : "Maximum 16 characters are allowed",
				minlength : "Minimum 10 characters are required",
				phonenumber :"Please Enter the valid phone number"
			},
			comp_email : {
				email : "Please enter valid email address",
				maxlength : "only 60 characters are allowed"
			},
			comp_owner_name : {
				required : "Please enter owner name",
				maxlength : "Maximum 40 characters are allowed"
			},
			comp_owner_address1 : {
				required : "Please enter owner address",
				maxlength : "Maximum 60 characters are allowed"
			},
			comp_owner_address2 : {
				required : "Please enter owner address"
			},
			comp_owner_country : {
				required : "Please select country"
			},
			comp_owner_zip : {
				required : "Please enter zip code",
				alphanumeric : "Please enter digits and alphabets only",
				maxlength : "Maximum 8 characters are allowed",
				minlength : "minimum 6 characters are required"
			},
			comp_owner_state : {
				required : "Please select state"
			},
			comp_owner_city : {
				required : "Please select city"
			},
			comp_owner_telephone : {
				required : "Please enter telephone no",
				maxlength : "Maximum 16 characters are allowed",
				minlength : "Minimum 10 characters are required",
				phonenumber :"Please Enter the valid phone number"
			},
			comp_owner_email : {
				email : "Please enter valid email address",
				maxlength : "only 60 characters are allowed"
			},
			comp_act_project_no : {
				required : "Please enter project number",
				maxlength : "Maximum 40 characters are allowed",
				digits : "Please enter valid project number",
				remote: "Please enter different project no"
			},
			comp_act_start_date : {
				required : "Please select start date"
			},
			comp_act_status : {
				required : "Please select status"
			},
			comp_act_end_date : {
				required : "Please select end date"
			},
			comp_finance_deal_value : {
				number : "Please enter valid deal value",
				maxlength : "only 20 characters are allowed",
				notEqual : "Deal value cannot be zero"
			},
			comp_finance_amt_paid : {
				number : "Please enter valid Paid amount",
				maxlength : "only 20 characters are allowed"
			},
			comp_finance_contact_info : {
				required : "Please enter telephone no",
				maxlength : "Maximum 16 characters are allowed",
				minlength : "minimum 10 characters are required",
				phonenumber :"Please Enter the valid phone number"
			},
			comp_finance_email_id : {
				email : "Please enter valid email address",
				maxlength : "only 60 characters are allowed"
			},
			comp_finance_deal_value : {
				required : "Please enter deal value",
			}
		},
		errorPlacement : function(error, element) {
			$(element).parent('div').after(error);
		}
	});
}

var invalidForm = false ;
var errorBy = "" ;

/**
 * checkFileType
 * 
 * Checks the type of the file 
 * 
 * @author Raman
 * @version 1.0
 */

function checkFileType(e,max)
{
	 var el = e.target  ? e.target :  e.srcElement ;
	 
	 var regex = /png|jpg|jpeg|PNG|JPG|JPEG/ ;
	 
	 if( regex.test(el.value) )
	 {
		 invalidForm = false ;
		 $("#logo_file_message").hide();
		 
	 } else {
		 invalidForm = true ;
		 $("#logo_file_message").show();
	}
}


