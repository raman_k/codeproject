$(document).ready(function(){
	var single_submit = true;
	validateAddInvestigatorForm();
		$('form#add_investigator,form#edit_investigator').submit(function(){
			checkStudyInputSelected();
			validateAddInvestigatorForm();
			if($("form#add_investigator,form#edit_investigator").valid()&& studyexist && single_submit == true){
				single_submit = false;
				return true;
				$('#add_investigator input[type=submit], #edit_investigator input[type=submit]').attr("disabled", "disabled");
			}
			return false;
			});
	
 //validateNewInvestigatorForm();	
//	jQuery.validator.addMethod("alpha", function(value, element) {
// 		return this.optional(element) || value == value.match(/^[a-zA-Z]+$/);
// 		},"Only alphabets are allowed.");
//	
	 $.validator.addMethod("email", function(value, element) {
		 	return this.optional(element) || /^[a-zA-Z0-9._-]+@(?:[A-Z0-9-]+\.)+[a-zA-Z]{2,3}$/i.test(value);
	    }, "Please enter the valid email");
});

/*code for validations add/edit investigator record 
 * author : skumar5
 */

function validateAddInvestigatorForm()
{
$("form#add_investigator,form#edit_investigator").validate({
    rules: {
    	investigator_firstname:{
			required: true,
			maxlength : 40
		},
		investigator_email: {
            required: true,
            email : true,
            remote : MODULE_CLINCTRIAL + '/manageinvestigator/checkduplicateemail/',
            maxlength : 60
        },
        investigator_status: {
            required: true
        },
        investigator_lastname: {
        	required: true,
        	maxlength : 40
        },
        investigator_study: {
        	required: true
        }
    },
	
    messages: {
    	investigator_firstname:{
			required: "Please enter the first name",
			maxlength : "Only 40 characters are allowed"
		},
		investigator_email: {
            required: "Please enter the email address",
            email : "Please enter the valid email address",
            maxlength : "Only 60 characters are allowed",
            remote : "Email already exists!!"
        },
        investigator_status: {
            required: "Please enter the status",
        },
        investigator_lastname: {
        	required: "Please enter the last name",
        	maxlength : "Only 40 characters are allowed"
        },
        investigator_study: {
        	required: "Study is a required field"
        }
    },
	errorPlacement: function(error,element) {
    
		if ($(element).attr("type") == "text")
		{
			$(element).parent('div.input-box').after(error);
		} 
		else if($(element).attr("id") == "investigator_status")
		{
			$(element).parent('div.input-box').after(error);
		} 
		else if($(element).attr("id") == "investigator_study")
		{
			$(element).parent('div.input-box').after(error);
		} 
		else
		{
			$(element).after(error);
		}
}
});
}


/*
 * addStudyToList
 * add studies to form
 * 
 * @author Raman
 * @version 1.0
 */
function addStudyToList(){
	
	studyValue = $("#investigator_study").val();
	addField = true;
	if(studyValue!=null){
		
		$("#studyList input").each(function() {
			
			if($(this).attr("value") == studyValue){
				addField = false;
			}
		});
		if(addField == false){
			$("#dupStudy").show();
			$("#dupStudy").text("This Study is added already!!");
		} else {
			
			$("#dupStudy").hide();
			var studyInput = '';
			cross = IMG_PATH + '/common/DeleteRed.png';
			
			labelVal = $('select#investigator_study option:selected').text();
			studyInput += '<div style="float:left;" id="study_' + studyValue + '">';
			studyInput +=	'<label>' + labelVal + '&nbsp;</label>';
			studyInput +=	'<input class="studyInput" id="studyIn_' + studyValue + '" name="investigator_study[]" type="hidden" value="' + studyValue + '">';
			studyInput +=	'<a onclick="deleteStudyFromList(' + studyValue + ');"  href="javascript:void(0);"><img src="' + cross + '" width="15" height="15"></a>&nbsp;';
			studyInput += '</div>';
			$("#studyList").append(studyInput);
			$("#studyError").hide();
		}
	}
}

/*
 * deleteStudyFromList
 * delete studies from form
 * 
 * @author Raman
 * @version 1.0
 */
function deleteStudyFromList(id){
		
	$("#study_" + id).remove();
	$("#dupStudy").hide();
	
}
studyexist = false;
function checkStudyInputSelected(){
	if($('input.studyInput').val()){
		studyexist = true;
		$("#studyError").hide();
	} else{
		$("#studyError").show();
		$("#studyError").text("Please Add atleast one study!!");
	}
}

/**
 * checkDuplicateEmail
 * 
 * Check duplicate email from user
 * 
 * @author Raman
 * @version 1.0
 */
function checkDuplicateEmail(email){
	
	  $.ajax({
			url : MODULE_CLINCTRIAL +"/manageinvestigator/checkduplicateemail/emailid/" + email,
			method : "post",
			dataType : "json",
			type : "post",
			success : function(data) { 
				if(data ===  null){
					alert('No Subject enrolled yet!!'); 
					return false;
				}else { 
                    $('.view-subject-right').html(data);
                    return false;
				}
              
			} 
	  });
 }