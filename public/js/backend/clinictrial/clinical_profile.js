$(document).ready(function(){
	
	$('.message-div').hide();
	$( "#changepassword" ).dialog({
		autoOpen: false,
		width: 665,
		height:450,
		show: {
			 effect: "fadeIn",
			 duration: 500
			 },
			 hide: {
			 effect: "fadeOut",
			 duration: 500
			 }
		
	});
	//validate changepassword form
	validateForm();
	$('form#formchangepassword').submit(function(){
		validateForm();
		if($("form#formchangepassword").valid()){
			changePassword();
		}
		return false;
		  
	});
	
	$.validator.addMethod("validpassword", function(value, element) {
	    return this.optional(element) ||
	    /^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[\W_]).*$/.test(value);
	}, "The password must contain a minimum of one lower case character," +
	   " one upper case character, one digit and one special character..");
	
});
function changePassword(){
	var data = $("form#formchangepassword").serialize();
	$.ajax({
		url : MODULE_CLINCTRIAL+"/profile/update-password/",
		method : "post",
		dataType : "json",
		data:data,
		type : "post",
		success : function(data) {
			$('.message-div').show();
			if(data==0){
				
				$('.message-div').html('<div class="error">Your old password does not match with our record.</div>');
				
			}else{
				
				$('.message-div').html('<div class="success">Password has been changed successfully.</div>');
				$('.message-div .success').css("color","white");
			}
		}
	});	
	
}
function hidePopUp(){
	$( "#changepassword" ).dialog( "close" );
 }
/**
 * showPopUp
 * 
 * show popup when click on chanage password button
 * 
 * 
 * @author kraj
 * @version 1.0
 */
function showPopUp(){	
	$('div.message-div').hide();
	$('#formchangepassword').each(function(){ 
	    this.reset();
	});
	$( "#changepassword" ).dialog( "open" );
 }
/**
 * validateForm
 * 
 * validate form of changapassword by admin
 * 
 * 
 * @author kraj
 * @version 1.0
 */
var validateChangePassword = null;
function validateForm() {
	
	validateChangePassword = $("form#formchangepassword").validate({
		rules: {
			oldpassword: {
	              required: true,
	              maxlength : 20,
	              minlength : 8
	          },
	          newpassword: {
				  required: true,
				  maxlength : 20,
				  minlength : 8,
				  validpassword : true
	          },
	          confirmpassword: {
	              required: true,
	              maxlength : 20,
	              minlength : 8,
	              equalTo : "#newpassword"
	          } 
	      },
	      messages: {
	    	  oldpassword: {
	    		  required: "Please enter the old password",
				  maxlength : "Maximum 20 characters are allowed",
				  minlength : "Please enter minimum 8 characters "
	          },
	          newpassword: {
				  required: "Please enter the new password",
				  maxlength : "Maximum 20 characters are allowed",
				  minlength : "Please enter minimum 8 characters ",
				  validpassword : "The password must contain a minimum of one lower case character," +
				  					" one upper case character, one digit and one special character."
	          },
	          confirmpassword: {
	        	  required: "Please enter the confirm password",
				  maxlength : "Maximum 20 characters are allowed",
				  minlength : "Please enter minimum 8 characters ",
				  equalTo : ('Password and Confirm password must be same')
	          }
	      },
	      errorPlacement: function(error,element) {
		       
			$(element).parent('.input-box').after(error);
				
	      }
	  });
}