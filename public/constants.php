<?php
# define HTTP path
define("HTTP_PATH", trim('http://' . $_SERVER['HTTP_HOST'] . '/'));

# define root path
defined('ROOT_PATH')
		|| define("ROOT_PATH", dirname($_SERVER['SCRIPT_FILENAME']) . '/' );

# PUBLIC PATH
defined('PUBLIC_PATH')
|| define('PUBLIC_PATH','http://' . $_SERVER['HTTP_HOST']
		. dirname($_SERVER['SCRIPT_NAME']) .'/');
	
# define upload path
defined('UPLOAD_PATH')
|| define('UPLOAD_PATH',  'uploads/');

# define imag path
defined('IMG_PATH')
|| define('IMG_PATH', PUBLIC_PATH . "images/"  );

# define imag path
defined('JS_PATH')
|| define('JS_PATH', PUBLIC_PATH . "js/"  );

# define imag path
defined('CSS_PATH')
|| define('CSS_PATH', PUBLIC_PATH . "css/"  );


# define clinicaltrial module path
define("MODULE_CLINICTRIAL", trim('http://' . $_SERVER['HTTP_HOST'] . '/clinictrial'));

# define investigator module path
define("MODULE_INVESTIGATOR", trim('http://' . $_SERVER['HTTP_HOST'] . '/investigator'));

# define subject module path
define("MODULE_SUBJECT", trim('http://' . $_SERVER['HTTP_HOST'] . '/subject'));

# define login module path
define("MODULE_LOGIN", trim('http://' . $_SERVER['HTTP_HOST'] . '/login'));

# define investigator type 
define("PRI_INV_TYPE", 'PRIMARY');
define("SEC_INV_TYPE", 'SECONDARY');

?>